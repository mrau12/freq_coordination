RFS 
DA 10 - W71 A (P) 
10 ; Parabolic antenna single polarisation 
NONE 
NONE 
20030601  
HQM 010704a 
7125 - 8500 MHZ 
45.3 dBi 
0.9 Deg 
HH 47
-180.0  -70.00 
-100.0  -70.00 
 -85.0  -55.00 
 -70.0  -48.00 
 -40.0  -46.00 
 -25.0  -35.00 
 -15.0  -35.00 
  -2.5  -24.00 
  -1.5  -21.40 
  -1.4  -18.70 
  -1.3  -16.10 
  -1.2  -13.70 
  -1.1  -11.50 
  -1.0   -9.50 
  -0.9   -7.70 
  -0.8   -6.10 
  -0.7   -4.70 
  -0.6   -3.40 
  -0.5   -2.40 
  -0.4   -1.50 
  -0.3   -0.90 
  -0.2   -0.40 
  -0.1   -0.10 
   0.0    0.00 
   0.1   -0.10 
   0.2   -0.40 
   0.3   -0.90 
   0.4   -1.50 
   0.5   -2.40 
   0.6   -3.40 
   0.7   -4.70 
   0.8   -6.10 
   0.9   -7.70 
   1.0   -9.50 
   1.1  -11.50 
   1.2  -13.70 
   1.3  -16.10 
   1.4  -18.70 
   1.5  -21.40 
   2.5  -24.00 
  15.0  -35.00 
  25.0  -35.00 
  40.0  -46.00 
  70.0  -48.00 
  85.0  -55.00 
 100.0  -70.00 
 180.0  -70.00 
HV 19
-180.0  -70.00 
-100.0  -70.00 
 -80.0  -58.00 
 -30.0  -53.00 
 -15.0  -53.00 
 -10.0  -50.00 
  -6.5  -38.00 
  -4.0  -38.00 
  -1.5  -30.00 
   0.0  -30.00 
   1.5  -30.00 
   4.0  -38.00 
   6.5  -38.00 
  10.0  -50.00 
  15.0  -53.00 
  30.0  -53.00 
  80.0  -58.00 
 100.0  -70.00 
 180.0  -70.00 
VV 47
-180.0  -70.00 
-100.0  -70.00 
 -85.0  -55.00 
 -70.0  -48.00 
 -40.0  -46.00 
 -25.0  -35.00 
 -15.0  -35.00 
  -2.5  -24.00 
  -1.5  -21.40 
  -1.4  -18.70 
  -1.3  -16.10 
  -1.2  -13.70 
  -1.1  -11.50 
  -1.0   -9.50 
  -0.9   -7.70 
  -0.8   -6.10 
  -0.7   -4.70 
  -0.6   -3.40 
  -0.5   -2.40 
  -0.4   -1.50 
  -0.3   -0.90 
  -0.2   -0.40 
  -0.1   -0.10 
   0.0    0.00 
   0.1   -0.10 
   0.2   -0.40 
   0.3   -0.90 
   0.4   -1.50 
   0.5   -2.40 
   0.6   -3.40 
   0.7   -4.70 
   0.8   -6.10 
   0.9   -7.70 
   1.0   -9.50 
   1.1  -11.50 
   1.2  -13.70 
   1.3  -16.10 
   1.4  -18.70 
   1.5  -21.40 
   2.5  -24.00 
  15.0  -35.00 
  25.0  -35.00 
  40.0  -46.00 
  70.0  -48.00 
  85.0  -55.00 
 100.0  -70.00 
 180.0  -70.00 
VH 19
-180.0  -70.00 
-100.0  -70.00 
 -80.0  -58.00 
 -30.0  -53.00 
 -15.0  -53.00 
 -10.0  -50.00 
  -6.5  -38.00 
  -4.0  -38.00 
  -1.5  -30.00 
   0.0  -30.00 
   1.5  -30.00 
   4.0  -38.00 
   6.5  -38.00 
  10.0  -50.00 
  15.0  -53.00 
  30.0  -53.00 
  80.0  -58.00 
 100.0  -70.00 
 180.0  -70.00 
