RFS kabelmetal
SU 4-190
NONE
NONE
NONE
16.07.99
HTT 951003b
17700 - 19700
44.6 dBi
0.9 DEG
HH      53
 -180.0   -67.0
  -90.0   -67.0
  -65.0   -49.0
  -50.0   -44.0
  -30.0   -42.0
  -15.0   -36.0
   -9.5   -36.0
   -7.0   -29.0
   -4.0   -29.0
   -3.5   -24.0
   -1.6   -24.0
   -1.5   -21.1
   -1.4   -18.4
   -1.3   -15.9
   -1.2   -13.5
   -1.1   -11.4
   -1.0    -9.4
   -0.9    -7.6
   -0.8    -6.0
   -0.7    -4.6
   -0.6    -3.4
   -0.5    -2.3
   -0.4    -1.5
   -0.3    -0.9
   -0.2    -0.4
   -0.1    -0.1
    0.0    -0.0
    0.1    -0.1
    0.2    -0.4
    0.3    -0.9
    0.4    -1.5
    0.5    -2.3
    0.6    -3.4
    0.7    -4.6
    0.8    -6.0
    0.9    -7.6
    1.0    -9.4
    1.1   -11.4
    1.2   -13.5
    1.3   -15.9
    1.4   -18.4
    1.5   -21.1
    1.6   -24.0
    3.5   -24.0
    4.0   -29.0
    7.0   -29.0
    9.5   -36.0
   15.0   -36.0
   30.0   -42.0
   50.0   -44.0
   65.0   -49.0
   90.0   -67.0
  180.0   -67.0
HV      19
 -180.0   -70.0
  -80.0   -70.0
  -40.0   -56.0
  -30.0   -56.0
  -15.0   -49.0
   -8.5   -49.0
   -7.5   -47.0
   -4.5   -47.0
   -1.0   -32.0
    0.0   -32.0
    1.0   -32.0
    4.5   -47.0
    7.5   -47.0
    8.5   -49.0
   15.0   -49.0
   30.0   -56.0
   40.0   -56.0
   80.0   -70.0
  180.0   -70.0
VV      53
 -180.0   -67.0
  -90.0   -67.0
  -65.0   -49.0
  -50.0   -44.0
  -30.0   -42.0
  -15.0   -36.0
   -9.5   -36.0
   -7.0   -29.0
   -4.0   -29.0
   -3.5   -24.0
   -1.6   -24.0
   -1.5   -21.1
   -1.4   -18.4
   -1.3   -15.9
   -1.2   -13.5
   -1.1   -11.4
   -1.0    -9.4
   -0.9    -7.6
   -0.8    -6.0
   -0.7    -4.6
   -0.6    -3.4
   -0.5    -2.3
   -0.4    -1.5
   -0.3    -0.9
   -0.2    -0.4
   -0.1    -0.1
    0.0    -0.0
    0.1    -0.1
    0.2    -0.4
    0.3    -0.9
    0.4    -1.5
    0.5    -2.3
    0.6    -3.4
    0.7    -4.6
    0.8    -6.0
    0.9    -7.6
    1.0    -9.4
    1.1   -11.4
    1.2   -13.5
    1.3   -15.9
    1.4   -18.4
    1.5   -21.1
    1.6   -24.0
    3.5   -24.0
    4.0   -29.0
    7.0   -29.0
    9.5   -36.0
   15.0   -36.0
   30.0   -42.0
   50.0   -44.0
   65.0   -49.0
   90.0   -67.0
  180.0   -67.0
VH      19
 -180.0   -70.0
  -80.0   -70.0
  -40.0   -56.0
  -30.0   -56.0
  -15.0   -49.0
   -8.5   -49.0
   -7.5   -47.0
   -4.5   -47.0
   -1.0   -32.0
    0.0   -32.0
    1.0   -32.0
    4.5   -47.0
    7.5   -47.0
    8.5   -49.0
   15.0   -49.0
   30.0   -56.0
   40.0   -56.0
   80.0   -70.0
  180.0   -70.0