Andrew Corp
PL15-77GD                     
                              
NONE            
NONE            
 4-25-79         
2763             
7750-8400    MHZ   
48.50 dBi       
0.6 DEGREES     
HH      27
-180.0  -57.0
-170.0  -57.0
-170.0  -68.0
-120.0  -68.0
-100.0  -53.0
 -60.0  -52.0
 -35.0  -52.0
 -20.0  -50.0
 -12.5  -50.0
  -8.0  -47.0
  -2.9  -36.0
  -0.5  -19.8
  -0.5  -15.0
   0.0    0.0
   0.5  -15.0
   0.5  -19.8
   2.9  -36.0
   8.0  -47.0
  12.5  -50.0
  20.0  -50.0
  35.0  -52.0
  60.0  -52.0
 100.0  -53.0
 120.0  -68.0
 170.0  -68.0
 170.0  -57.0
 180.0  -57.0
HV      17
-180.0  -70.0
 -40.0  -70.0
 -15.0  -62.0
 -10.0  -60.0
  -6.1  -60.0
  -5.0  -51.0
  -2.1  -45.0
  -1.0  -30.0
   0.0  -30.0
   1.0  -30.0
   2.1  -45.0
   5.0  -51.0
   6.1  -60.0
  10.0  -60.0
  15.0  -62.0
  40.0  -70.0
 180.0  -70.0
VV      29
-180.0  -57.0
-170.0  -57.0
-170.0  -68.0
-120.0  -68.0
-100.0  -54.0
 -60.0  -54.0
 -40.0  -51.0
 -20.0  -50.0
 -15.0  -42.0
 -10.0  -40.0
  -6.8  -37.0
  -3.0  -35.0
  -0.5  -19.8
  -0.5  -15.0
   0.0    0.0
   0.5  -15.0
   0.5  -19.8
   3.0  -35.0
   6.8  -37.0
  10.0  -40.0
  15.0  -42.0
  20.0  -50.0
  40.0  -51.0
  60.0  -54.0
 100.0  -54.0
 120.0  -68.0
 170.0  -68.0
 170.0  -57.0
 180.0  -57.0
VH      23
-180.0  -70.0
 -66.0  -70.0
 -60.0  -67.0
 -20.0  -67.0
 -15.0  -62.0
 -10.5  -54.9
  -8.6  -54.0
  -7.0  -47.0
  -3.1  -47.0
  -2.1  -45.0
  -1.0  -30.0
   0.0  -30.0
   1.0  -30.0
   2.1  -45.0
   3.1  -47.0
   7.0  -47.0
   8.6  -54.0
  10.5  -54.9
  15.0  -62.0
  20.0  -67.0
  60.0  -67.0
  66.0  -70.0
 180.0  -70.0
