REVNUM:,NSMA WG16.99.050
REVDAT:,19990520
ANTMAN:,ANDREW CORPORATION
MODNUM:,VHLP2-80-HW2
PATNUM:,7288
FEDORN:,NA
DESCR1:,
DESCR2:,
DESCR3:,
DESCR4:,
DESCR5:,
DTDATA:,20130204
LOWFRQ:, 71000 
HGHFRQ:, 86000 
GUNITS:,DBI/DBR
LWGAIN:,50.0
MDGAIN:,50.5
HGGAIN:,51.0
AZWIDT:,0.5
ELWIDT:,0.5
ATVSWR:,1.5
FRTOBA:,68
ELTILT:,0
PATTYP:,ENVELOPE
NOFREQ:,NA
PATFRE:,NA
NUMCUT:,4
PATCUT:,AZ
POLARI:,H/H
NUPOIN:, 43 
-180.00,-68.00,
-85.00,-68.00,
-70.00,-55.00,
-50.00,-52.00,
-30.00,-52.00,
-20.00,-50.00,
-10.00,-42.00,
-5.01,-35.00,
-5.00,-30.00,
-1.21,-23.00,
-1.20,-12.00,
-0.50,-12.00,
-0.45,-10.84,
-0.40,-8.29,
-0.35,-6.24,
-0.30,-4.43,
-0.25,-2.92,
-0.20,-1.43,
-0.15,-0.67,
-0.10,-0.22,
-0.05,-0.01,
0.00,0.00,
0.05,-0.01,
0.10,-0.22,
0.15,-0.67,
0.20,-1.43,
0.25,-2.92,
0.30,-4.43,
0.35,-6.24,
0.40,-8.29,
0.45,-10.84,
0.50,-12.00,
1.20,-12.00,
1.21,-23.00,
5.00,-30.00,
5.01,-35.00,
10.00,-42.00,
20.00,-50.00,
30.00,-52.00,
50.00,-52.00,
70.00,-55.00,
85.00,-68.00,
180.00,-68.00,
PATCUT:,AZ
POLARI:,H/V
NUPOIN:, 15 
-180.00,-69.00,
-70.00,-69.00,
-20.00,-55.00,
-10.01,-50.00,
-10.00,-48.00,
-5.01,-48.00,
-5.00,-25.00,
0.00,-25.00,
5.00,-25.00,
5.01,-48.00,
10.00,-48.00,
10.01,-50.00,
20.00,-55.00,
70.00,-69.00,
180.00,-69.00,
PATCUT:,AZ
POLARI:,V/V
NUPOIN:, 43 
-180.00,-68.00,
-85.00,-68.00,
-65.00,-55.00,
-50.00,-52.00,
-30.00,-52.00,
-20.00,-50.00,
-10.00,-42.00,
-5.01,-35.00,
-5.00,-30.00,
-1.21,-23.00,
-1.20,-12.00,
-0.50,-12.00,
-0.45,-10.84,
-0.40,-8.29,
-0.35,-6.24,
-0.30,-4.43,
-0.25,-2.92,
-0.20,-1.43,
-0.15,-0.67,
-0.10,-0.22,
-0.05,-0.01,
0.00,0.00,
0.05,-0.01,
0.10,-0.22,
0.15,-0.67,
0.20,-1.43,
0.25,-2.92,
0.30,-4.43,
0.35,-6.24,
0.40,-8.29,
0.45,-10.84,
0.50,-12.00,
1.20,-12.00,
1.21,-23.00,
5.00,-30.00,
5.01,-35.00,
10.00,-42.00,
20.00,-50.00,
30.00,-52.00,
50.00,-52.00,
65.00,-55.00,
85.00,-68.00,
180.00,-68.00,
PATCUT:,AZ
POLARI:,V/H
NUPOIN:, 15 
-180.00,-69.00,
-70.00,-69.00,
-20.00,-55.00,
-10.01,-50.00,
-10.00,-48.00,
-5.01,-48.00,
-5.00,-25.00,
0.00,-25.00,
5.00,-25.00,
5.01,-48.00,
10.00,-48.00,
10.01,-50.00,
20.00,-55.00,
70.00,-69.00,
180.00,-69.00,
ENDFIL:,EOF
