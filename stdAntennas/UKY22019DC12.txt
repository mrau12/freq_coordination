REVNUM:,NSMA WG16.99.050
REVDAT:,19990520
ANTMAN:,ERICSSON
MODNUM:,ML11 2.4m HP 2p
PATNUM:,2422019dc12a
FEDORN:,NA
DESCR1:,UKY 220 19/DC12
DESCR2:,
DESCR3:,
DESCR4:,
DESCR5:,
DTDATA:,20061031
LOWFRQ:,10500
HGHFRQ:,11700
GUNITS:,DBI/DBR
LWGAIN:,45.4
MDGAIN:,45.8
HGGAIN:,46.3
AZWIDT:,0.84
ELWIDT:,0.84
ATVSWR:,1.22
FRTOBA:,71
ELTILT:,0
ANTWID:,2.4
PATTYP:,ENVELOPE
NOFREQ:,NA
PATFRE:,NA
NUMCUT:,4
PATCUT:,AZ
POLARI:,H/H
NUPOIN:,25
-180,-70.8,
-100,-70.8,
-60,-51.8,
-50,-47.8,
-20,-43.8,
-5,-25.8,
-3,-25.8,
-2,-20,
-1,-12,
-0.4,-3,
-0.25,-1,
-0.17,-0.5,
0,0,
0.17,-0.5,
0.25,-1,
0.4,-3,
1,-12,
2,-20,
3,-25.8,
5,-25.8,
20,-43.8,
50,-47.8,
60,-51.8,
100,-70.8,
180,-70.8,
PATCUT:,AZ
POLARI:,H/V
NUPOIN:,13
-180,-70.8,
-95,-70.8,
-60,-59.8,
-12,-51.8,
-5,-40.8,
-1,-32,
0,-32,
1,-32,
5,-40.8,
12,-51.8,
60,-59.8,
95,-70.8,
180,-70.8,
PATCUT:,AZ
POLARI:,V/V
NUPOIN:,25
-180,-70.8,
-100,-70.8,
-60,-51.8,
-50,-47.8,
-20,-43.8,
-5,-25.8,
-3,-25.8,
-2,-20,
-1,-12,
-0.4,-3,
-0.25,-1,
-0.17,-0.5,
0,0,
0.17,-0.5,
0.25,-1,
0.4,-3,
1,-12,
2,-20,
3,-25.8,
5,-25.8,
20,-43.8,
50,-47.8,
60,-51.8,
100,-70.8,
180,-70.8,
PATCUT:,AZ
POLARI:,V/H
NUPOIN:,13
-180,-70.8,
-95,-70.8,
-60,-59.8,
-12,-51.8,
-5,-40.8,
-1,-32,
0,-32,
1,-32,
5,-40.8,
12,-51.8,
60,-59.8,
95,-70.8,
180,-70.8,
ENDFIL:,EOF
