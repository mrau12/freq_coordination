Andrew Corp
HP2-220B                      
                              
AD0500          
NONE            
 6-25-92         
3821             
21200-23600  MHZ   
40.50 dBi       
1.5 DEGREES     
HH      83
-180.0  -65.0
 -90.0  -65.0
 -82.0  -62.0
 -78.2  -59.6
 -73.3  -56.5
 -66.8  -56.5
 -60.7  -52.4
 -57.9  -51.3
 -51.6  -46.3
 -46.8  -42.8
 -40.7  -42.4
 -39.8  -42.4
 -30.0  -42.0
 -28.1  -36.2
 -20.0  -36.2
 -17.4  -33.7
 -15.0  -33.1
 -10.9  -32.0
  -9.1  -31.2
  -8.3  -31.2
  -7.3  -30.4
  -6.6  -28.5
  -5.2  -25.1
  -3.2  -25.1
  -2.4  -16.9
  -1.5  -12.0
  -1.0   -6.0
  -1.0   -5.2
  -0.9   -4.4
  -0.8   -3.7
  -0.8   -3.1
  -0.7   -2.5
  -0.6   -2.0
  -0.6   -1.6
  -0.5   -1.2
  -0.4   -0.9
  -0.3   -0.6
  -0.3   -0.4
  -0.2   -0.2
  -0.1   -0.1
  -0.1    0.0
   0.0    0.0
   0.1    0.0
   0.1   -0.1
   0.2   -0.2
   0.3   -0.4
   0.3   -0.6
   0.4   -0.9
   0.5   -1.2
   0.6   -1.6
   0.6   -2.0
   0.7   -2.5
   0.8   -3.1
   0.8   -3.7
   0.9   -4.4
   1.0   -5.2
   1.0   -6.0
   1.5  -12.0
   2.4  -16.9
   3.2  -25.1
   5.2  -25.1
   6.6  -28.5
   7.3  -30.4
   8.3  -31.2
   9.1  -31.2
  10.9  -32.0
  15.0  -33.1
  17.4  -33.7
  20.0  -36.2
  28.1  -36.2
  30.0  -42.0
  39.8  -42.4
  40.7  -42.4
  46.8  -42.8
  51.6  -46.3
  57.9  -51.3
  60.7  -52.4
  66.8  -56.5
  73.3  -56.5
  78.2  -59.6
  82.0  -62.0
  90.0  -65.0
 180.0  -65.0
HV      41
-180.0  -75.0
 -80.0  -75.0
 -78.2  -72.0
 -70.0  -65.5
 -66.0  -64.8
 -62.7  -64.8
 -56.8  -63.0
 -51.0  -57.7
 -44.3  -57.7
 -35.5  -56.3
 -30.2  -50.7
 -27.9  -50.7
 -21.6  -48.1
 -16.3  -47.4
 -15.0  -47.4
 -14.1  -47.4
 -13.4  -44.5
  -2.1  -44.5
  -1.5  -41.2
  -1.4  -34.0
   0.0  -34.0
   1.4  -34.0
   1.5  -41.2
   2.1  -44.5
  13.4  -44.5
  14.1  -47.4
  15.0  -47.4
  16.3  -47.4
  21.6  -48.1
  27.9  -50.7
  30.2  -50.7
  35.5  -56.3
  44.3  -57.7
  51.0  -57.7
  56.8  -63.0
  62.7  -64.8
  66.0  -64.8
  70.0  -65.5
  78.2  -72.0
  80.0  -75.0
 180.0  -75.0
VV      71
-180.0  -65.0
 -75.0  -65.0
 -70.9  -59.4
 -60.8  -59.4
 -56.6  -52.9
 -51.6  -49.5
 -47.0  -47.3
 -40.1  -42.5
 -30.0  -42.5
 -26.3  -37.9
 -20.0  -36.1
 -17.0  -33.5
 -15.0  -33.1
  -9.1  -31.9
  -8.1  -31.3
  -6.2  -31.3
  -5.4  -25.7
  -3.3  -25.7
  -2.1  -18.0
  -1.4   -9.0
  -1.0   -4.8
  -0.9   -4.3
  -0.9   -3.7
  -0.8   -3.2
  -0.7   -2.8
  -0.7   -2.3
  -0.6   -1.9
  -0.5   -1.5
  -0.5   -1.2
  -0.4   -0.9
  -0.3   -0.6
  -0.3   -0.4
  -0.2   -0.2
  -0.1   -0.1
  -0.1    0.0
   0.0    0.0
   0.1    0.0
   0.1   -0.1
   0.2   -0.2
   0.3   -0.4
   0.3   -0.6
   0.4   -0.9
   0.5   -1.2
   0.5   -1.5
   0.6   -1.9
   0.7   -2.3
   0.7   -2.8
   0.8   -3.2
   0.9   -3.7
   0.9   -4.3
   1.0   -4.8
   1.4   -9.0
   2.1  -18.0
   3.3  -25.7
   5.4  -25.7
   6.2  -31.3
   8.1  -31.3
   9.1  -31.9
  15.0  -33.1
  17.0  -33.5
  20.0  -36.1
  26.3  -37.9
  30.0  -42.5
  40.1  -42.5
  47.0  -47.3
  51.6  -49.5
  56.6  -52.9
  60.8  -59.4
  70.9  -59.4
  75.0  -65.0
 180.0  -65.0
VH      37
-180.0  -75.0
 -80.0  -75.0
 -72.0  -72.0
 -67.0  -68.6
 -63.9  -67.4
 -58.4  -67.4
 -55.0  -64.6
 -48.9  -63.7
 -44.2  -63.1
 -39.6  -61.9
 -31.5  -58.0
 -25.6  -58.0
 -22.4  -50.3
 -15.0  -50.3
  -4.7  -50.3
  -3.0  -48.3
  -2.1  -39.1
  -1.5  -34.0
   0.0  -34.0
   1.5  -34.0
   2.1  -39.1
   3.0  -48.3
   4.7  -50.3
  15.0  -50.3
  22.4  -50.3
  25.6  -58.0
  31.5  -58.0
  39.6  -61.9
  44.2  -63.1
  48.9  -63.7
  55.0  -64.6
  58.4  -67.4
  63.9  -67.4
  67.0  -68.6
  72.0  -72.0
  80.0  -75.0
 180.0  -75.0
