Andrew Corp
UHX8-37H                      
                              
A42860          
A42861          
 7-27-82         
1433             
3700-4200    MHZ   
37.40 dBi       
2.4 DEGREES     
HH      60
-180.0  -66.0
-171.0  -66.0
-160.0  -72.0
-115.0  -72.0
-102.3  -70.3
 -99.0  -68.2
 -82.0  -63.0
 -77.0  -57.5
 -65.5  -54.0
 -61.2  -52.2
 -58.0  -47.8
 -40.4  -47.8
 -35.9  -47.6
 -30.8  -46.3
 -29.8  -43.4
 -22.4  -38.0
 -19.9  -37.3
 -15.0  -35.1
 -14.3  -34.8
 -13.2  -34.8
 -11.7  -30.1
  -9.7  -30.1
  -8.5  -25.3
  -7.3  -25.1
  -5.1  -25.1
  -4.0  -17.6
  -3.7  -17.5
  -2.2  -12.9
  -1.5   -5.9
  -1.0   -2.8
  -0.6   -0.8
   0.0    0.0
   0.5   -0.5
   1.2   -2.8
   1.5   -5.8
   2.0   -8.8
   3.7  -15.6
   4.2  -16.2
   5.0  -24.3
   6.7  -27.0
   8.3  -27.0
   9.0  -34.5
  12.4  -34.5
  13.6  -43.6
  15.0  -43.6
  17.1  -43.6
  21.9  -48.0
  27.9  -49.0
  32.5  -49.0
  36.1  -54.1
  42.8  -54.2
  55.0  -54.2
  56.0  -57.5
  62.7  -57.5
  78.0  -70.0
  94.0  -70.0
  96.0  -72.0
 160.0  -72.0
 171.0  -66.0
 180.0  -66.0
VV      62
-180.0  -66.0
-171.0  -66.0
-160.0  -72.0
 -90.0  -72.0
 -88.0  -70.0
 -75.3  -63.8
 -70.5  -63.8
 -68.9  -63.0
 -61.2  -60.7
 -56.9  -54.9
 -46.8  -54.9
 -41.0  -54.8
 -35.7  -49.5
 -30.9  -45.5
 -25.9  -42.9
 -24.8  -42.9
 -16.3  -37.2
 -15.0  -35.6
 -14.5  -35.0
 -12.4  -35.0
 -11.8  -32.9
  -9.1  -32.9
  -8.3  -28.4
  -7.7  -27.0
  -4.9  -27.0
  -3.3  -21.2
  -2.1   -8.9
  -1.6   -5.7
  -1.2   -2.8
  -0.6   -0.9
   0.0    0.0
   0.6   -0.8
   1.0   -1.8
   1.8   -5.8
   2.1   -8.1
   3.9  -21.4
   4.2  -21.9
   5.3  -27.3
   7.0  -29.0
   8.3  -29.0
   9.2  -35.0
  10.7  -35.1
  11.6  -35.6
  12.3  -35.6
  14.2  -42.2
  15.0  -42.5
  16.1  -43.0
  21.8  -46.9
  25.1  -47.7
  30.9  -49.6
  38.0  -53.4
  46.0  -58.0
  58.0  -58.0
  68.0  -59.0
  72.0  -66.0
  77.0  -67.5
  80.0  -69.0
  96.0  -69.0
 100.0  -70.0
 164.0  -70.0
 171.0  -66.0
 180.0  -66.0
HV      42
-180.0  -72.0
-171.0  -72.0
-169.0  -74.0
 -80.0  -74.0
 -68.0  -70.0
 -62.0  -65.0
 -60.0  -64.8
 -52.9  -64.8
 -42.6  -64.8
 -36.1  -64.1
 -27.3  -64.1
 -23.0  -58.5
 -17.3  -55.1
 -15.0  -52.1
 -14.0  -50.9
  -8.7  -50.9
  -6.0  -49.2
  -5.6  -49.1
  -4.3  -43.0
  -3.0  -33.0
   0.0  -33.0
   3.0  -33.0
   6.0  -54.0
   6.4  -59.6
   7.8  -59.6
   8.1  -61.6
   9.9  -62.2
  11.6  -62.2
  12.3  -64.0
  14.2  -64.1
  15.0  -64.1
  27.5  -64.1
  31.9  -65.6
  59.9  -65.6
  63.9  -68.6
  69.4  -68.6
  72.1  -69.3
  78.0  -73.8
  82.0  -74.0
 169.0  -74.0
 171.0  -72.0
 180.0  -72.0
VH      37
-180.0  -72.0
 -90.0  -72.0
 -85.8  -70.3
 -75.8  -69.5
 -72.1  -66.6
 -64.1  -66.6
 -55.9  -64.3
 -51.9  -64.3
 -46.2  -63.6
 -43.4  -63.6
 -37.0  -63.5
 -33.0  -63.5
 -26.0  -62.7
 -21.6  -56.8
 -15.9  -53.6
 -15.0  -53.0
 -12.7  -51.6
  -7.1  -51.6
  -6.8  -50.5
  -5.5  -50.5
  -3.0  -33.0
   0.0  -33.0
   3.0  -33.0
   5.7  -53.0
   7.3  -53.0
   9.0  -59.0
  12.0  -59.0
  14.5  -62.0
  15.0  -62.0
  25.0  -62.0
  27.0  -66.0
  49.3  -66.0
  52.1  -68.2
  56.0  -68.3
  66.0  -68.3
  80.0  -72.0
 180.0  -72.0
