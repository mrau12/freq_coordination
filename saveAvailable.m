classdef saveAvailable < handle
    %MODEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        filename;
        values;
        availableChannels;
        Table
    end
    
    methods
        function saveAv = saveAvailable(filename)
            if ~exist('Jobs','dir')
                mkdir('Jobs');
                saveAv.filename=[filename,'-',datestr(date),'.csv'];
            else
                saveAv.filename=[filename,'-',datestr(date),'.csv'];
            end
        end
        
        function propertiesSize = getPropertySize(availableChannels)
            propertiesSize = length(fieldnames(availableChannels));
        end
        
        function tableValues(saveAv,availableChannels)
            for index = 1:length(availableChannels); % not using theband or correction property
 %               C{1,index} = availableChannels(1,index).numChannel;
                C{2,index} = availableChannels(1,index).channelCenter;
                C{3,index} = availableChannels(1,index).lower3dD;
                C{4,index} = availableChannels(1,index).upper3dD;
                C{5,index} = availableChannels(1,index).channel_Inteferes_TxLo;
                C{6,index} = availableChannels(1,index).channel_Inteferes_TxHi;
                C{7,index} = availableChannels(1,index).coch_attenuation_TxLo;
                C{8,index} = availableChannels(1,index).coch_attenuation_TxHi;
                C{9,index} = availableChannels(1,index).adj_attenuation_TxLo;
                C{10,index} = availableChannels(1,index).adj_attenuation_TxHi;
                saveAv.values = C;
            end
        end
        
        function objToTable (saveAv)
            % ava_names = {'Channel','Site_Sense','Nearest_CoInterferer','Nearest_AdjInterferer','Frequency','Avaliability'};
            headerNames = {'Num_Channel' 'Channel_Center' 'Lower_3dD' 'Upper_3dD'...
                'Channel_Inteferes_TxLo' 'Channel_Inteferes_TxHi' 'Coch_attenuation_TxLo'...
                'Coch_attenuation_TxHi' 'Adj_attenuation_TxLo' 'Adj_attenuation_TxHi'};
            T = cell2table(saveAv.values','VariableNames',headerNames);
            saveAv.Table = T;
        end
        
        function [] = writeDataAvaialable(saveAv)
            writetable(saveAv.Table,strcat('Jobs/',saveAv.filename));
            %This warning is displayed when your requested worksheet does
            %not exist and is created.
            warning('off','MATLAB:xlswrite:AddSheet');
        end
        
        %% Open saved file in Excel
        function openFolder(saveAv)
            winopen(strcat('Jobs/',saveAv.filename));
        end
    end
end



