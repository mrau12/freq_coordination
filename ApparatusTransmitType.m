classdef ApparatusTransmitType < handle
    %TRANSMITTYPEAPPARATUS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Mode1;
        Mode2;
    end
    
    methods
        
        function transApp = ApparatusTransmitType(TxRx)
            switch nargin
                case 0
                    transApp.Mode1 = [];
                    transApp.Mode2 = [];
                case 1
                    setModes(transApp,TxRx)
            end
        end
        
        function [] = setModes(transApp,TxRx)
            switch TxRx
                case 1, %Tx/Rx TxHi@A
                    transApp.Mode1 = 'T';
                    transApp.Mode2 = 'R';
                case 2, %Tx/Rx TxHi@B
                    transApp.Mode1 = 'R';
                    transApp.Mode2 = 'T';
                case 3, %Tx @ A
                    transApp.Mode1 = 'T';
                    transApp.Mode2 = '';
                case 4, %Tx @ B
                    transApp.Mode1 = '';
                    transApp.Mode2 = 'R';
            end            
        end
        
        function [] = setMode1(transApp, modeA)
            transApp.Mode1 = modeA;
        end
        
        function [] = setMode2(transApp, modeB)
            transApp.Mode2 = modeB;
        end       
    end
end

