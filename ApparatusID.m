classdef ApparatusID < handle
    %LOADAPPARATUS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties                
        ACCESS_ID;
        HEIGHT;              
        emission;
        date;
    end
    
    methods
        function IdApp = ApparatusID(access,height,emission,date)
            switch nargin
                case 0
                    IdApp.ACCESS_ID = [];
                    IdApp.HEIGHT = [];
                    IdApp.emission = [];
                    IdApp.date = [];
                case 4
                    setACCESS_ID(IdApp,access);
                    setHeight(IdApp,height);
                    setEmission(IdApp,emission);
                    setDate(IdApp,date);
                otherwise
                    disp('Error: incorrect parameters for AppID object')
            end
        end
        % How do I make SURE any inputed value is a string?
        function [] = setACCESS_ID(IdApp,value)
            IdApp.ACCESS_ID = char(value);
        end
        
        function [] = setHeight(IdApp,height)
            IdApp.HEIGHT = height;
        end
        
        function [] = setEmission(IdApp,emission)
            IdApp.emission = emission;
        end
        
        function [] = setDate(IdApp,date)
            IdApp.date = date;
        end
    end
end

