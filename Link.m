% Michael McClellan
% SAT PTY LTD
% Created: OCT 2015
% Modified: APR 2016 08:00
% Revision 0.20
% Class Summary goes here
%  MVC: Model
% Examples:
%
% Provide sample usage code here
%
% See also: List related files here

classdef Link < handle
    % write a description of the class here.
    properties (Constant)
       LIGHT_SPEED=3*10^8;
    end
    properties
        app1;
        app2;
        theBand;              
    end
    methods
        % CONSTRUCTOR
        function link = Link(app1,app2)
            link.app1=app1;
            link.app2=app2;           
        end         
        
        function [link_budget] = antennaBudget(link,AngleOne,AngleTwo)
            %% LOS angle of sites
%             AngleOne
%             AngleTwo

            find_antenna_pattern(link.app1);
            find_antenna_pattern(link.app2);
            
            if link.app1.theGain==link.app1.theAngle % why does it only check app1 gain?
                link_budget=0;
            end
            %% Format text, so usbale for calculations                        
            removeNulls(link.app1);
            uniqueSort(link.app1);
            removeNulls(link.app2);
            uniqueSort(link.app2);            
            findBoreGain(link.app1);
            findBoreGain(link.app2);
           
            %% Transmitter Gain (dB):
            % Perform interpolation to find the gain at a particular angle.
            ant1_gain=interpolation(link.app1,AngleOne);
            
            %% Receiver Gain (dB):
            % Perform interpolation to find the gain at a particular angle.
            ant2_gain=interpolation(link.app2,AngleTwo);  
            
            %% Free Space Loss
            site1_to_site2 = DistanceTxRx(link);
            site1_to_site2_fspl = calcFSPL(link,site1_to_site2); %(link.app1,site1_to_site2)
            
            %% Feed loss
            feed_loss=0;
            
            link_budget = getTxPower(link.app1)-feed_loss+ant1_gain+ant2_gain-site1_to_site2_fspl-feed_loss; % Basic link budget for antenna pointing directly at opposing atnenna
%             getTxPower(link.app1)
%             ant1_gain
%             ant2_gain
%             site1_to_site2
%             link.app1.BandsApparatus.frequency1
%             site1_to_site2_fspl
%             link_budget
        end   
        
        % Output: D2D (digital to digital modulation),A2D,D2A,A2A
        % Resources: http://www.acma.gov.au/webwr/_assets/main/lib100342/emission(rib8).pdf
        function linkMod = getLinkModulation(link)
            % mods are strings
            % Emission modulation designation for app1
            mod1 = getModulationFromEmission(link.app1);
            % Emission modulation designation for app2
            mod2 = getModulationFromEmission(link.app2);
            linkMod = strcat(mod1,'2',mod2);
        end
        
        function feederLossReceiver(link)
        end
        
        function feederLossTransmitter(link)
        end
        
        function fspl = calcFSPL(link,Distance)
           %fspl = 20*log10(((4*pi)/link.LIGHT_SPEED)*Distance*link.app1.BandsApparatus.frequency1)
           d = 20*log10(Distance);
           f = 20*log10(link.app1.BandsApparatus.frequency1);
           c = 20*log10(((4*pi)/link.LIGHT_SPEED));
           fspl = d+f+c;
        end  
        
        function [distance] = DistanceTxRx(link)
            distance = vdist(link.app1.LocationApparatus.latitude,link.app1.LocationApparatus.longitude,link.app2.LocationApparatus.latitude,link.app2.LocationApparatus.longitude);
        end       
        
        % new link's boresite angle
        function [delta,gamma] = calcDelta(link)
            delta = atan2((link.app1.LocationApparatus.longitude-link.app2.LocationApparatus.longitude),(link.app1.LocationApparatus.latitude-link.app2.LocationApparatus.latitude));
            gamma = delta+pi;
        end       
        
        % Output return all lats and longs as array
        function [latApp1,longApp1,latApp2,longApp2] = bothAppsLocations(link)             
            % App 1
            latApp1 = link.app1.LocationApparatus.latitude;
            longApp1 = link.app1.LocationApparatus.longitude;
            
            % App 2 
            latApp2 = link.app2.LocationApparatus.latitude;
            longApp2 = link.app2.LocationApparatus.longitude;           
        end
        
        %% Link Ends coordinates
        % Input: link
        % Outputs: lat, long of each end of link
        function [end1_link,end2_link] = LinkEnds(link)
           end1_Lat = link.app1.LocationApparatus.latitude;
           end1_Long = link.app1.LocationApparatus.longitude;          
           end1_link = [end1_Lat,end1_Long];          
           end2_Lat = link.app2.LocationApparatus.latitude;
           end2_Long = link.app2.LocationApparatus.longitude;           
           end2_link = [end2_Lat,end2_Long];           
        end  
        
        %% Coordinates of triangle
        % Input: Two Links, where you're finding angle between
        % Output: [coord1,coord2, coord3]  where coordinate 1 is the overlap of the two links
        % 1*******2
        %  *
        %    *
        %      *
        %        3        
       
        function [coordinate1,coordinate2,coordinate3] = coordinatesForAngle(link1,link2)
           [end1_linkOne,end2_linkOne] = LinkEnds(link1);
           [end1_linkTwo,end2_linkTwo] = LinkEnds(link2);           
           if isequal(end1_linkOne,end1_linkTwo)
               coordinate1 = end1_linkOne;
               coordinate2 = end2_linkOne;
               coordinate3 = end2_linkTwo;
           elseif isequal(end2_linkOne,end2_linkTwo)
               coordinate1 = end2_linkOne;
               coordinate2 = end1_linkOne;
               coordinate3 = end1_linkTwo;
           elseif isequal(end1_linkOne,end2_linkTwo)
               coordinate1 = end1_linkOne;
               coordinate2 = end2_linkOne;
               coordinate3 = end1_linkTwo;
           elseif isequal(end2_linkOne,end1_linkTwo)
               coordinate1 = end2_linkOne;
               coordinate2 = end1_linkOne;
               coordinate3 = end2_linkTwo;
           else
               disp('Links do not have common Apparatus aka Coordinates')
           end
        end
        
        %% Finds angle  between two links
        % Input: two links, link1 and interfer link2
        % Output: Angle in radians between three pairs of lat, long 
        function ang = angleBetweenLinks(link1,link2)
            [coordinate1,coordinate2,coordinate3] = coordinatesForAngle(link1,link2);
            % Coordinates 1
            lat1 = coordinate1(1,1);
            long1 = coordinate1(1,2);
            % Coordinates 2
            lat2 = coordinate2(1,1);
            long2 = coordinate2(1,2);
            % Coordinates 3
            lat3 = coordinate3(1,1);
            long3 = coordinate3(1,2);
            % Angle between norm vectors
            V1=[(lat1-lat2),(long1-long2)];
            V2=[(lat1-lat3),(long1-long3)];
            ang  = acos(dot(V1,V2)/(norm(V1)*norm(V2)));
            if ang > pi
                ang = 2*pi - ang;
            end
        end

    end
end
