classdef ApparatusSignal < handle % just for site C and D???
    % write a description of the class here.
    properties
        % define the properties of the class here, (like fields of a struct)        
        ANT_GAIN;
        ANT_MODEL;
        BANDWIDTH;
    end
    
    methods
        % constructor
        function sigApp = ApparatusSignal(gain,model,bandwidth)
            switch nargin
                case 0                    
                    sigApp.ANT_MODEL = [];
                    sigApp.ANT_GAIN = [];
                    sigApp.BANDWIDTH = [];
                                       
                case 2                   
                    setANT_GAIN(sigApp,gain);
                    setANT_MODEL(sigApp,model);
                    sigApp.BANDWIDTH = [];
                                        
                case 3                    
                    setANT_GAIN(sigApp,gain);
                    setANT_MODEL(sigApp,model); 
                    sigApp.BANDWIDTH = bandwidth;
                                       
                otherwise
                    disp('error incorrect parameters for AppSignal object')
            end
        end
        % METHODS   %%%%%
        % 1.the functions with switches not working FIXED: pass object
        % first i.e function(sigApp,value) NOT function(value, sigApp)
        % 2. add some checks to make sure im entering string when im suppose
        % to enter strings
        
        %working with strings
        
        function setANT_GAIN(sigApp,gain)
            sigApp.ANT_GAIN = gain;
        end        
       
        function setANT_MODEL(sigApp,model)
            sigApp.ANT_MODEL = model;
        end
    end
end