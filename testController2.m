clear;
clc;

Logon = LogonODBC('googleSQLAsia','root','matlab123');% Default settings, username,pass, etc for google SQL server

%h = waitbar(0,'Please wait...');

%outputApp3 = Commandline();

%-----------------> Input variables
radius = 15; % km's
bandIndex = 1;%15; %remember this is a index so index = 11 doesnt mean 11G2.4.dat 
latitude1 = -33.791132;
longitude1 = 151.087255;
latitude2 = -33.772771;
longitude2 = 151.081349;

TxPower = 1;
ModelID = 'DEFAULT';  % folders in antenna folder: DEFAULT,89,99,NOTIONAL
TxRx = 1; %Tx/Rx TxHi@A

gain = 1;

polarity = 'V';
sizeAnt = 2.4;

correction = 0; % rain in mm

access = '0000';
height = 5;
emission = 'XXXX';
date = datetime('today');

%----------------->SETUP INPUT APPARATUS: App3 & App4
bands = BandsSQL(bandIndex,Logon);

%-----------------> location object
loc_3 = ApparatusLocation(latitude1,longitude1);
loc_4 = ApparatusLocation(latitude2,longitude2);

%-----------------> creates two location Apparatus object and Calc Azimuth
setup1 = SetupLocation(loc_3,loc_4);

%-----------------> signal Apparatus object 
signal3_4 = ApparatusSignal(gain,ModelID);

%-----------------> transmit Apparatus object 
transmit3_4 = ApparatusTransmitType(TxRx);

%-----------------> Bands Apparatus object
bandsApp3_4 = ApparatusBands(bands);

%-----------------> Coordinate object 
coordinate3_4 = ApparatusCoordinate(polarity,sizeAnt,TxPower,correction);

%-----------------> Setup 2
setup2 = SetupBandsCoordSigApp(bandsApp3_4,coordinate3_4,signal3_4);

%-----------------> ID Object
IdApp3_4 = ApparatusID(access,height,emission,date);

%-----------------> Input & merge sub-apparatus into main app object
AppC = Apparatus(getSigApp(setup2),getBandApp(setup2),getLoc3(setup1),transmit3_4,IdApp3_4,getCoordApp(setup2));
AppD = Apparatus(getSigApp(setup2),getBandApp(setup2),getLoc4(setup1),transmit3_4,IdApp3_4,getCoordApp(setup2));

%-----------------> DATABASE SETUP
data = Database1(radius,freqLow(bands),freqHigh(bands),AppC.LocationApparatus,AppD.LocationApparatus,Logon);

%Mid location for AppC and AppD
setMidCoord(data); 

% Make SQL query and return the relevant data
getRawData(data);

% Sort relevant data so easier to manipulate
sorted = sortRawData(data);

%------------------> Coordinate
coordinate = Coordinate(AppC,AppD,sorted,bands,correction);

%------------------> Link Calc
tic

dumpIndex=1;
for device=2:size(coordinate.sortedData,1);
    %waitbar(device/size(coordinate.sortedData,1));
    if isequal(coordinate.sortedData{device-1,1},coordinate.sortedData{device,1})
        loadApp1App2(coordinate,device,bands);
        dataDump(dumpIndex) = DataDump();
        dataDump(dumpIndex).setData(coordinate,bands,correction);
        dumpIndex=dumpIndex+1;
        dumpIndex
    end
end
toc

%------------------> Save 
save = SaveJob('adj_server_listing');
tableValues(save,dataDump);
objDumpToTable(save);
writeDataDump(save);

%close(h) 