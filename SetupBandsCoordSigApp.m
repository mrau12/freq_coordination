classdef SetupBandsCoordSigApp < handle
    %USED for input Apparatus App3(siteC) and App4 (siteD)
    %Sets up three Apparatus:  Bands,Coordinate,Signal
    %Uses those Apparatus to calcate boregain and then set it
    
    properties
        bandsApp3_4;
        coordinate3_4;
        signal3_4;
    end
    
    methods
        function setup2 = SetupBandsCoordSigApp(bandsApp3_4,coordinate3_4,signal3_4)
            
            setup2.bandsApp3_4 = bandsApp3_4;
            setup2.coordinate3_4 = coordinate3_4;
            setup2.signal3_4 = signal3_4;            
            
            %
            %               Calc boreGain
            setBoreGain(setup2);
            
            %
            %               signal3_4.gain = boreGain
            setBoreGainToGain(setup2);
        end                
        
        %Must first set: freq1 & antennaSize
        function setBoreGain(setup2)
            
            if(~isnan(setup2.bandsApp3_4.frequency1) && ~isnan(setup2.coordinate3_4.ANT_SIZE))
                setup2.bandsApp3_4.boreGain = 10*log10(0.6*(setup2.bandsApp3_4.frequency1*pi*setup2.coordinate3_4.ANT_SIZE/(3*10^8))^2);
                %disp('Bore-gain is set')
            else
                warning('frequency1 & ant_size must be set, or Datab is not the correct argument before bore gain calculated')
                return; % Safe?
            end
            
        end
        
        function setBoreGainToGain(setup2)
            setANT_GAIN(setup2.signal3_4,setup2.bandsApp3_4.boreGain);            
        end
        
        function bandApp = getBandApp(setup2)
            bandApp = setup2.bandsApp3_4;
        end
        
        function coApp = getCoordApp(setup2)
            coApp = setup2.coordinate3_4;
        end
        
        function sigApp = getSigApp(setup2)
            sigApp = setup2.signal3_4;
        end
    end    
end

