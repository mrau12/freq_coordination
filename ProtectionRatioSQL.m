classdef ProtectionRatioSQL < handle
    % ProtectionRatioSQL returns Protection ratio
    % Detailed explanation goes here
    
    properties        
        tableProRatio;
        Logon;
        band;
    end
    
    methods
        % constructor
        function proRatio = ProtectionRatioSQL(Logon,whichband)           
            proRatio.Logon = Logon;
             proRatio.band = whichband;           
        end         
        
        % Input: band e.g. 8GHz
        % Output: Modulation,Offset,Bandwidth1,Bandwidth2,Protection Ratio
        % in table form 
        function [] = setProRatioTable(proRatio)
            try
            conn = database(proRatio.Logon.dbsource,proRatio.Logon.username,proRatio.Logon.password); %matlab>2014
            if ~isempty(conn.Message)
                fprintf(2,'%s\n', conn.Message);
            else
                fprintf(1, 'Querying database for Protection Ratio table: Band,Modulation,Frequency Offset,ChannelSize1,ChannelSize2,Protection Ratio,special et cetera... \n');
            end
            SQL_command = ['SELECT * FROM  acma_new.protectionRatio WHERE band=',num2str(proRatio.band)];
            curs = exec(conn,SQL_command); %defines a command
            SLQreturn=fetch(curs); %executes command            
            close(conn)
            catch
                close(conn);
                display('ERROR: Protection Ration SQL connection...SQL query maybe taking to long')
            end
            protectionRatio = SLQreturn.Data;
            names = {'band','modulation','frequencyOffset','channelSize1','channelSize2','protectionRatio','Special'};
            proRatio.tableProRatio = cell2table(protectionRatio,'VariableNames',names);
        end
             
    end
end
