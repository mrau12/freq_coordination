classdef Distance < handle
    %DISTANCE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        sorted;
        AppC;
        AppD;
        C;
        distanceTable;
    end
    
    methods
        function distance = Distance(sorted,AppC,AppD)
            distance.sorted = sorted;
            distance.AppC = AppC;
            distance.AppD = AppD;
            distance.C = [];
            distance.distanceTable = [];
        end
        
        function dist_from_AppC = getDistFromAppC(distance,num)
            dist_from_AppC = vdist(distance.sorted{num,10},distance.sorted{num,11},distance.AppC.LocationApparatus.latitude,distance.AppC.LocationApparatus.longitude);
        end
        
        function dist_from_AppD = getDistFromAppD(distance,num)
            dist_from_AppD = vdist(distance.sorted{num,10},distance.sorted{num,11},distance.AppD.LocationApparatus.latitude,distance.AppD.LocationApparatus.longitude);
        end
        
        function setIndividualTable(distance,index)
            distance.C{index,1} = distance.sorted{index,1}; %Licence
            distance.C{index,2} = distance.sorted{index,9}; %Site id 
            distance.C{index,3} = distance.sorted{index,10}; %Site LAT
            distance.C{index,4} = distance.sorted{index,11}; %Site LONG
            distance.C{index,5} = getDistFromAppC(distance,index);
            distance.C{index,6} = getDistFromAppD(distance,index);
        end
        
        
        function setAllTables(distance)
            for index = 1:length(distance.sorted)
                setIndividualTable(distance,index)
            end
        end
        
        %% Creates table distance between victims and input apps
        function setTable(distance)
            names = {'Licence_No','Site_ID','Site_LAT','Site_LONG' ,'dist_Site_2_AppC','dist_Site_2_AppD'};                    
            distance.distanceTable = cell2table(distance.C,'VariableNames',names);
        end
        
    end
    
end

