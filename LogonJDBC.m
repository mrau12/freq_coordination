classdef LogonJDBC < handle
    % Password & Username for database
    
    properties
        dbname;
        username;
        password;
        driver;
        dburl;
    end
    
    methods
        function Logon = LogonJDBC(username,password,dbname,driver,dburl)
            switch nargin
                case 5                    
                    Logon.username = username;
                    Logon.password = password;
                    Logon.dbname = dbname;
                    Logon.driver = driver; %http://au.mathworks.com/help/database/ug/mysql-jdbc-windows.html
                    Logon.dburl = dburl;
                    
                otherwise                    
                    Logon.username = username;
                    Logon.password = password;                    
                    Logon.dbname = 'acma_new';
                    Logon.driver = 'com.mysql.jdbc.Driver';
                    Logon.dburl = strcat('jdbc:mysql://104.199.148.36:3306/',Logon.dbname);  
                    disp('Default SQL connection settings: ')
                    disp(['Database name: ',Logon.dbname])
                    disp(['User: ',Logon.username])
                    disp('Pass: ******* ')
                    disp(['MySQL driver: ',Logon.driver])
                    disp(['IP & DBname: ',Logon.dburl])                   
            end            
        end
        
        function user = getUser(Logon)
            user = char(Logon.username);
        end
        
        function pass = getPass(Logon)
            pass = char(Logon.password);
        end        
        
    end
end
