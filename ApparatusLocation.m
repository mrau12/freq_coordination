classdef ApparatusLocation < handle
    %DISCRETEAPPARATUS Summary of this class goes here
    %  Sub object for Apparatus, sets where site is located and if
    
    properties
        latitude;
        longitude;
        ANT_AZ;  
        SITE_ID;
    end
    
    methods
        function locApp = ApparatusLocation(lat,long,Az,site)
            switch nargin 
                 case 0
                    locApp.latitude=[];
                    locApp.longitude=[];
                    locApp.ANT_AZ=[]; 
                    locApp.SITE_ID=[];               
                case 2
                    setLat(locApp,lat);
                    setLong(locApp,long);
                    locApp.ANT_AZ = []; 
                    locApp.SITE_ID = '4123';
                case 3
                    setLat(locApp,lat);
                    setLong(locApp,long);
                    locApp.ANT_AZ = []; 
                    setSITE_ID(locApp,site);
                case 4
                    setLat(locApp,lat);
                    setLong(locApp,long);
                    setAz(locApp,Az);
                    setSITE_ID(locApp,site);
                otherwise
                    disp('error incorrect parameters for AppLocation object')
            end
        end
        function [] = setAz(locApp,Az)
            locApp.ANT_AZ = Az;
        end
        
        function [] = setLat(locApp,lat)
            locApp.latitude = lat;
        end
        
        function [] = setLong(locApp,long)
            locApp.longitude = long;
        end
        
        function [long] = getLong(locApp)
            long = locApp.longitude;
        end
        
        function [lat] = getLat(locApp)
            lat = locApp.latitude;
        end
        
        function [] = setSITE_ID(locApp,site)
            locApp.SITE_ID = site;
        end
        
        function [id] = getSiteId(locApp)
            id = locApp.SITE_ID;
        end
    end
end

