classdef saveDataDump < handle
    %MODEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        filename;
        dumpTable;
        values;
    end
    
    methods
        function save = saveDateDump()
            save.filename = [];
            save.dumpTable = [];
            save.values = [];
        end
        
        function addFileName(save,theFilename)
            if ~exist('Jobs','dir')
                mkdir('Jobs');
            end
            save.filename=[theFilename,'-',datestr(date),'.csv'];
        end
        
        function propertiesSize = getPropertySize(dataDump)
            propertiesSize = length(fieldnames(dataDump));
        end
        
        function tableValues(save,dataDump)
            s = length(dataDump);
            for index = 1:s; % not using theband or Correction property
                C{1,index} = dataDump(1,index).linkID;
                C{2,index} = dataDump(1,index).SiteAId;
                C{3,index} = dataDump(1,index).SiteBId;                
                C{4,index} = dataDump(1,index).antBudget1;
                C{5,index} = dataDump(1,index).antBudget2;
                C{6,index} = dataDump(1,index).antBudget3;
                C{7,index} = dataDump(1,index).antBudget4;
                C{8,index} = dataDump(1,index).wantedToUnwantedSignalB; % was coChannelIsolationB;
                C{9,index} = dataDump(1,index).CoCh_pro_B;
                C{10,index} = dataDump(1,index).Prot_Margin_B;
                C{11,index} = dataDump(1,index).wantedToUnwantedSignalD; % was coChannelIsolationD;
                C{12,index} = dataDump(1,index).CoCh_pro_D;
                C{13,index} = dataDump(1,index).Prot_Margin_D;
                C{14,index} = dataDump(1,index).A2B_pl;
                C{15,index} = dataDump(1,index).A2D_pl;
                C{16,index} = dataDump(1,index).C2B_pl;
                C{17,index} = dataDump(1,index).C2D_pl;
                C{18,index} = dataDump(1,index).assTxFreqApp1;
                C{19,index} = dataDump(1,index).CoChannelNum ;
            end
            save.values = C;
        end
        
        function objDumpToTable (save)
            headerNames = {'Link_A2B_ID' 'Site_A_ID' 'Site_B_ID' 'A2B_link_budget' 'A2D_link_budget' 'C2B_link_budget' 'C2D_link_budget'...
                'Wanted_2_Unwanted_Sig_B' 'CoCh_protection_B' 'Prot_Margin_B' 'Wanted_2_Unwanted_Sig_D' 'CoCh_pro_ratio_D'...
                'Prot_Margin_D' 'A2B_path_length' 'A2D_path_length' 'C2B_path_length' 'C2D_path_length' 'Ass_Tx_Freq_App1' 'Co_Channel_Number'};
            T = cell2table(save.values','VariableNames',headerNames);
            save.dumpTable = T;
        end
        
        function [] = writeDataDump(save)
            writetable(save.dumpTable,strcat('Jobs/',save.filename));
            %This warning is displayed when your requested worksheet does
            %not exist and is created.
            warning('off','MATLAB:xlswrite:AddSheet');
        end
        
        %% Open saved file in Excel
        function openFolder(save)
            winopen(strcat('Jobs/',save.filename));
        end
        
    end
end

