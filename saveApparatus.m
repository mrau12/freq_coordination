classdef saveApparatus < handle
    %SAVEJOBNAME Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        filename;
        table;
        SQLdata;
    end
    
    methods
        function saveJob = saveApparatus(data)
            saveJob.filename = [];
            saveJob.table = [];
            saveJob.SQLdata = data;
        end
        
        
        function createFileName(saveJob,theFilename)
            if ~exist('Jobs','dir')
                mkdir('Jobs');
            end
            saveJob.filename=[theFilename,'-',datestr(date),'.csv'];
        end
        
        %% Write data to table so is usable
        function objSQLToTable (saveJob)
            columns={'ACCESSID',...
                'FREQASS',...
                'MODEA',...
                'MODEB',...
                'TXPOWER',...
                'ANTAZ',...
                'ANTPOL',...
                'HEIGHT',...
                'SITEID',...
                'LATITUDE',...
                'LONGITUDE',...
                'ANTID',...
                'AUTHORISATIONDATE',...
                'EMISSION',...
                'ANTGAIN',...
                'ANTMODEL',...
                'ANTSIZE',...
                'BANDWIDTH'};
            
            T = cell2table(saveJob.SQLdata ,'VariableNames',columns);
            %size(columns)
            saveJob.table = T;
        end
        
        %% write to data to file
        function [] = writeSQL(saveJob)
            writetable(saveJob.table,strcat('Jobs/',saveJob.filename));
            %This warning is displayed when your requested worksheet does
            %not exist and is created.
            warning('off','MATLAB:xlswrite:AddSheet');
        end       
        
        %% Open saved file in Excel
        function openFolder(saveJob)
            winopen(strcat('Jobs/',saveJob.filename));
        end
    end    
end

