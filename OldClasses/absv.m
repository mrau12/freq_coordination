function [abss] = absv(Vector)
    abss=abs(Vector*[1 1i]');
end