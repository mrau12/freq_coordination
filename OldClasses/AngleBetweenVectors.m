%% Aka awesomeFunction
% Input: Lats and Long of three Apparatus 
% Output: Angle in degrees between 3 lat, long pairs
function ang = AngleBetweenVectors(lat1,long1,lat2,long2,lat3,long3)
V1=[(lat1-lat2),(long1-long2)];
V2=[(lat1-lat3),(long1-long3)];
ang  = acosd(dot(V1,V2)/(norm(V1)*norm(V2)));
end