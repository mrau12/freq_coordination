clear;
clc;
%-----------------> setup 1 variables

%outputApp3 = Commandline();

%-----------------> setup 2 variables
radius = 200; 
bandIndex = 15; %remember this is a index so index = 11 doesnt mean 11G2.4.dat 
latitude1 = -33.791132;
longitude1 = 151.087255;
latitude2 = -33.772771;
longitude2 = 151.081349;

TxPower = 1;
ModelID = 'DEFAULT';  %folders in antenna folder: DEFAULT,89,99,NOTIONAL
TxRx = 1; %Tx/Rx TxHi@A

%not sure what gain should be?
gain = 1;

polarity = 'V';
sizeAnt = 2.4;

correction = 0; % rain in mm

access = '0000';
height = 5;
emission = 'XXXX';
date = datetime('today');

%----------------->SETUP INPUT APPARATUS: App3 & App4
bands = BandsSQL(bandIndex);

%-----------------> location object
loc_3 = ApparatusLocation(latitude1,longitude1);
loc_4 = ApparatusLocation(latitude2,longitude2);

%-----------------> creates two location Apparatus object and Calc Azimuth
setup1 = SetupLocation(loc_3,loc_4);

%-----------------> signal Apparatus object 
signal3_4 = ApparatusSignal(gain,ModelID);

%-----------------> transmit Apparatus object 
transmit3_4 = ApparatusTransmitType(TxRx);

%-----------------> Bands Apparatus object
bandsApp3_4 = ApparatusBands(bands);

%-----------------> coordinate object 
coordinate3_4 = ApparatusCoordinate(polarity,sizeAnt,TxPower,correction);

%-----------------> boresight setup
setup2 = SetupBandsCoordSigApp(bandsApp3_4,coordinate3_4,signal3_4);

%-----------------> ID Object
IdApp3_4 = ApparatusID(access,height,emission,date);

%-----------------> Input & merge sub-apparatus into main app object
AppC = Apparatus(getSigApp(setup2),getBandApp(setup2),getLoc3(setup1),transmit3_4,IdApp3_4,getCoordApp(setup2));
AppD = Apparatus(getSigApp(setup2),getBandApp(setup2),getLoc4(setup1),transmit3_4,IdApp3_4,getCoordApp(setup2));

%-----------------> DATABASE SETUP
data = Database(radius, freqLow(bands), freqHigh(bands),getLoc3(setup1),getLoc4(setup1));
sorted = sortRawData(data);

%------------------> coordinate
coordinate = Coordinate(AppC,AppD,sorted,bands,correction);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%___DEVICE 113 Interpolation____%%%%%%%%%%%%%%%%%%%%%
AppShitty1 = Apparatus();
loadApparatus(AppShitty1, sorted, 114, bands); %AppShitty, sorted, 113, bands);
removeIllegChar(AppShitty1);
[antenna,offset] = lookupAntDatabase(AppShitty1);
% App.theAngle=0;
% App.theGain=0;
% trigger=0;
% for i=1:size(antenna{1},1)
%     if str2double(antenna{1}{i})==-180
%         trigger=1;
%         offset=i-1;
%     end
%     if trigger
%         App.theAngle(i-offset)=str2double(antenna{1}{i});
%         App.theGain(i-offset)=str2double(antenna{2}{i});
%         if isnan(App.theAngle(i-offset))
%             display(['skipped line. Antenna file ',App.SignalApparatus.ANT_MODEL,' need work.'])
%             App.theAngle(i-offset)=[];
%             App.theGain(i-offset)=[];
%             offset=offset-1;
%         end
%         if isequal(App.theAngle(i-offset),180)
%             App.theAngle=App.theAngle*2*pi/360;
%             break
%         end
%     end
% end
% 
% if AppShitty.theGain==AppShitty.theAngle % why does it only check app1 gain?
%     link_budget=0;
% end
% 
% find(AppShitty.theAngle==0); %NEEDED? %in the vector angle, return the index of 0's
% removeNulls(AppShitty);
% uniqueSort(AppShitty);
% 
% 
% findBoreGain(AppShitty);
% ant1_gain=interpolation(AppShitty);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




device = 114;
dumpIndex=1;
    if isequal(coordinate.sortedData{device-1,1},coordinate.sortedData{device,1})
        loadApp1App2(coordinate,device,bands);
        dataDump(dumpIndex) = DataDump();
        dataDump(dumpIndex).setData(coordinate,bands,correction);
        dumpIndex=dumpIndex+1;
        dumpIndex
    end

