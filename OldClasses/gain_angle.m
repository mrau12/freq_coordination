function [ gain,angle ] = gain_angle()
file_name=['Antennas/DEFAULT/','8','G','1.2','.adf'];
s=30; % first data point offset
antenna_data = fopen(file_name, 'r');% load antenna data
antenna = textscan(antenna_data, '%s%s', 'delimiter',',');% import mixed data types as strings (%s) 2 columbs
fclose(antenna_data);
gain = [-45.00;-45.00';-50.00;-50.00;-37.00;-37.00;-35.00;-35.00;-31.20;-29.00;-29.00;-21.00;-21.00;-14.00;-7.30;-2.90;-1.10;-0.40;0.00;-0.40;-1.10;-2.90;-7.30;-14.00;-21.00;-21.00;-29.00;-29.00;-31.20;-35.00;-35.00;-37.00;-37.00;-50.00;-50.00;-45.00;-45.00]
angle = [-180.00;-170.00;-170.00;-141.50;-110.00;-75.00;-60.00;-24.00;-15.00;-14.00;-9.90;-6.50;-3.00;-2.50;-2.00;-1.50;-1.00;-0.50;0.00;0.50;1.00;1.50;2.00;2.50;3.00;6.50;9.90;14.00;15.00;24.00;60.00;75.00;110.00;141.50;170.00;170.00;180.00]

angle=0;
gain=0;
trigger=0;

for i=1:size(antenna{1},1)
    if str2double(antenna{1}{i})==-180
        trigger=1;
        s=i-1;
    end
    if trigger
        angle(i-s)=str2double(antenna{1}{i});
        gain(i-s)=str2double(antenna{2}{i});
        if isnan(angle(i-s))
            
            angle(i-s)=[];
            gain(i-s)=[];
            s=s-1;
        end
        if isequal(angle(i-s),180)
            angle=angle*2*pi/360;
            break
        end
    end
end
end

