classdef Links
    %LINKS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        link1;
        link2;
        link3;
        link4;
    end
    
    methods
        function links = Links()
            switch n
                case 2
                    links.link3 = link3;
                    links.link4 = link4;
                case 4
                    links.link1 = link1;
                    links.link2 = link2;
                    links.link3 = link3;
                    links.link4 = link4;
            end
        end
        
         function [tf] = get(links)% before comparsion must load app1 into link1
            app1LinkID = links.link1.app1.IDApparatus.setACCESS_ID;
            tf = isequal(links.currentLinkID,app1LinkID);
         end
        
                 %OLD
%         function [A2B_link_budget,A2D_link_budget,C2B_link_budget,C2D_link_budget,A_2_B,A_2_D,C_2_B,C_2_D] = old_find_signal_level( )
%             %% OLD data types               NEW Apparatus data types
%             % columns=['ACCESS_ID,',...1 ------> app.ACCESS_ID
%             %         'FREQ_ASS,',...2 --------> app.frequency1
%             %         'MODE_,',...3 -----------> app.Mode1;
%             %         'MODE_,',...4 -----------> app.Mode2;
%             %         'TX_POWER,',...5  -------> app.TX_POWER;
%             %         'ANT_AZ,',...6
%             %         'ANT_POL,',...7 ---------> app.pol;
%             %         'HEIGHT,',...8 ----------> app.HEIGHT;
%             %         'assign.SITE_ID,',..9 ---> app.SITE_ID;
%             %         'site.LATITUDE,',...10---> app.latitude;
%             %         'site.LONGITUDE,',...11--> app.longitude;
%             %         'assign.ANT_ID,',...12 ----> app.ANT_ID;
%             %         'AUTHORISATION_DATE,',..13-> app.date;
%             %         'EMISSION,'...14---------> app.emission;
%             %         'antenna.ANT_GAIN,'...15-> app.ANT_GAIN;
%             %         'antenna.ANT_MODEL,',...16--> app.ANT_MODEL;
%             %         'antenna.ANT_SIZE']; 17--> app.ANT_SIZE;
%             
%             %[alpha,beta] = calcAlpha(signal.appA,signal.appB); %
%             %calcalting interf_angle used for plotting
%             %[delta,gamma] = calcDelta(signal.appC,signal.appD);
%             A2D_link_budget = antennaBudget(signal.appA,signal.appD);
%             A2B_link_budget = antennaBudget(signal.appA,signal.appB);
%             %A_2_D = DistanceTxRx(signal.appA,signal.appD);
%             %C2B_link_budget = link(signal.appC,signal.appB);
%             %C2D_link_budget = link(signal.appC,signal.appD);
%             C2B_link_budget = antennaBudget(signal.appC,signal.appB);
%             C2D_link_budget = antennaBudget(signal.appC,signal.appD);
%             %C_2_B = DistanceTxRx(signal.appC,signal.appB);
%             %A_2_B = DistanceTxRx(signal.appA,signal.appB);
%             %C_2_D = DistanceTxRx(signal.appC,signal.appD);
%         end
    end
    
end

