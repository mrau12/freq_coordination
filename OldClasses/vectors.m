clc
clear all
lat1=-31;
long1=150;
lat2=-31.5;
long2=150.5;
lat3=-31;
long3=150.5;
%unit vector


V1=[(lat1-lat2),(long1-long2)];
V2=[(lat1-lat3),(long1-long3)];

v1_1 = sqrt(sum(V1.^2))
v2_2 = sqrt(sum(V2.^2))
%atan2(sqrt(dot(cross(a, b), cross(a, b))), dot(a, b))
theta=acos((dot(V1,V2))/(absv(V2)*absv(V1)));
