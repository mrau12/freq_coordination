classdef GUILogon
    %GUILOGON Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        login;
        password;
    end
    
    methods
        function guilogon =  GUILogon()
            %-----------------> Logon infomation, also need ssl certs for google SQL
            % cloud server in the  same  folder as this file
            [guilogon.login, guilogon.password] = logindlg('Title','Login Title');            
        end
        
        
        function Logon = ODBCFunction(guilogon)
            % Default settings, username,pass, etc for google SQL server
             Logon = LogonODBC('googleSQLAsia',guilogon.login,guilogon.password);
        end
        
        function tf = isConnected(guilogon)
            conn = database('googleSQLAsia',guilogon.login,guilogon.password); %matlab>2014
            if ~isempty(conn.Message)
                fprintf(1, 'Unsuccessful, Retry or Escape\n');
                fprintf(2,'%s\n', conn.Message);
                tf = 0;
            else   
                 fprintf(1, 'Successful log on\n');
                 tf = 1;
            end
        end
    end    
end

