 function [] = matchIdLinks(coordinate,sortedData,Bands,correction)
            j=1;
            current_linkID=0;
            for i=1:size(sortedData,1) % assess each Interferer criteria
                %% Match - finds matching "link ID's"
                if isequal(current_linkID,sortedData{i,1}) % tests if current device matches data saved for siteA
                    j=j+1;
                    loadApparatus(coordinate.link2,sortedData,i);%load new Interferer Site B %extracts row at i                   
                    [antBudget1(j),antBudget2(j),antBudget3(j),antBudget4(j)] = antBudget(coordinate,j);                   
                    [coChannelIsolationB(j)] = coChanIsolation(antBudget1(j),antBudget3(j));
                    [coChannelIsolationD(j)] = coChanIsolation(antBudget4(j),antBudget2(j));
                    %% Protection Ratios
                    link2Dist(j) = coordinate.link2.DistanceTxRx(); %distance app1,app4
                    link3Dist(j) = coordinate.link3.DistanceTxRx(); %distance app3,app2             
                    [CoCh_pro_D(j),AdjCh_pro_D(j)] = CorrectionF(correction,link2Dist(j),Bands);
                    [CoCh_pro_B(j),AdjCh_pro_B(j)] = CorrectionF(correction,link3Dist(j),Bands);
                    
                    %% Protection Margin - if negitive we can't use that channel
                    Prot_Margin_B(j)=coChannelIsolationB(j)+CoCh_pro_B(j);
                    Prot_Margin_D(j)=coChannelIsolationD(j)+CoCh_pro_D(j);
                    %% find avaliable channels
                    new_site(j)=coordinate.link1.app1.frequency1; % frequency/channel % site A Frequency/Chan
                else
                    current_linkID=sortedData{i,1};% save link ID to compare to find matching pair App.IDApparatus.setACCESS_ID
                    loadApparatus(coordinate.link1,sortedData,i); % load next device to find its matching end
                end
            end
        end
%-----------------------------------------------------------------------------------------------------------------
% Michael McClellan
% SAT PTY LTD
% Created: OCT 2015
% Modified: APR 2016 08:00:00
% Revision 0.20
% Class Summary goes here
%  MVC: Model
% Examples:
%
% Provide sample usage code here
%
% See also: List related files here
classdef Coordinate
    % write a description of the class here.
    properties
        link1;
        link2;
        link3;
        link4;
        currentLinkID;
    end
    
    methods
        % CONSTRUCTOR
        function coordinate = Coordinate(link1,link2,link3,link4)
            coordinate.link1 = link1;
            coordinate.link2 = link2;
            coordinate.link3 = link3;
            coordinate.link4 = link4;
            coordinate.currentLinkID = 0;
        end
        % METHODS
        
        
        function [] = experimentMatch(sortedData,Bands,correction)
            j=1;
            for i=1:size(sortedData,1) %all rows in sorted Data
                if not(compareLinkID(coordinate)) %currentID != siteA_ID
                    coordinate.currentLinkID = sortedData{i,1}; % currentID = siteA_ID
                    loadAppIntoLinks(coordinate.link1.app1,sortedData,i); %loads siteA into link1 and link2 = currentRow (sortedData)
                else
                    loadApparatus(coordinate.link1.app2,sortedData,i); % loads siteB into link1 and link3 = currentRow (sortedData)
                    j=j+1;
                    dump(j) = generateDataDump(coordinate,Bands,correction);
                end
            end            
        end
        
        function [tf] = compareLinkID(coordinate)% before comparsion must load app1 into link1
            app1LinkID = coordinate.link1.app1.IDApparatus.setACCESS_ID;
            tf = isequal(coordinate.currentLinkID,app1LinkID);
        end
        %e.g loads App1 into link1 and link2
        function [] = loadAppIntoLinks(whichApparatus,sortedData,i) %App exists in two links
            switch whichApparatus
                case app1
                    loadApparatus(coordinate.link1.app1,sortedData,i);
                    loadApparatus(coordinate.link2.app1,sortedData,i);
                case app2
                    loadApparatus(coordinate.link1.app2,sortedData,i);
                    loadApparatus(coordinate.link3.app2,sortedData,i);
                case app3
                    loadApparatus(coordinate.link3.app3,sortedData,i);
                    loadApparatus(coordinate.link4.app3,sortedData,i);
                case app4
                    loadApparatus(coordinate.link2.app4,sortedData,i);
                    loadApparatus(coordinate.link4.app4,sortedData,i);
                otherwise
                    disp('Either incorrect Apparatus in parameter, link not initialised or no sorted data')
            end
        end
        
        function [coChannelIsolation] = coChanIsolation(Budget1,Budget2)
            %  CoCh_Isolation_B(j) = A2B_link_budget(j)-C2B_link_budget(j);
            %  CoCh_Isolation_D(j) = C2D_link_budget(j)-A2D_link_budget(j);
            coChannelIsolation = Budget1 - Budget2;
        end
        
        function [antBudget1,antBudget2,antBudget3,antBudget4] = antBudget(coordinate)
            antBudget1 = antennaBudget(coordinate.link1); %link1(app1,app2) A2B_link_budget
            antBudget2 = antennaBudget(coordinate.link2); %link2(app1,app4) A2D_link_budget
            antBudget3 = antennaBudget(coordinate.link3); %link3(app3,app2) C2B_link_budget
            antBudget4 = antennaBudget(coordinate.link4); %link4(app3,app4) C2D_link_budget
        end
        
        function [CoCh_pro_Link,AdjCh_pro_Link] = CorrectionF(correction,link,bands)
            %         CoCh_pro_D(j)=-60+correctionfactor(correction,link2(j),getBands(Datab));
            %         CoCh_pro_B(j)=-60+correctionfactor(correction,link3(j),getBands(Datab));
            %         AdjCh_pro_D(j)=-30+correctionfactor(correction,link2(j),getBands(Datab));
            %         AdjCh_pro_B(j)=-30+correctionfactor(correction,link3(j),getBands(Datab));
            temp = correctionfactor(correction,link,bands); % do I have to repeatly have to get datab?
            CoCh_pro_Link = -60 + temp;
            AdjCh_pro_Link = -30 + temp;
        end
        
        function [] = generateDataDump(coordinate,correction,index)
            index=index+1;
            [antBudget1(index),antBudget2(index),antBudget3(index),antBudget4(index)] = antBudget(coordinate,index);
            [coChannelIsolationB(index)] = coChanIsolation(antBudget1(index),antBudget3(index));
            [coChannelIsolationD(index)] = coChanIsolation(antBudget4(index),antBudget2(index));
            
            %% Protection Ratios
            link2Dist(index) = coordinate.link2.DistanceTxRx(); %distance app1,app4
            link3Dist(index) = coordinate.link3.DistanceTxRx(); %distance app3,app2
            [CoCh_pro_D(index),AdjCh_pro_D(index)] = CorrectionF(correction,link2Dist(index),Bands);
            [CoCh_pro_B(index),AdjCh_pro_B(index)] = CorrectionF(correction,link3Dist(index),Bands);
            
            %% Protection Margin - if negitive we can't use that channel
            Prot_Margin_B(index)=coChannelIsolationB(index)+CoCh_pro_B(index);
            Prot_Margin_D(index)=coChannelIsolationD(index)+CoCh_pro_D(index);
            %% find avaliable channels
            new_site(index)=coordinate.link1.app1.frequency1; % frequency/channel % site A Frequency/Chan
        end
        
      
        
        function [protect_corr]=correctionfactor(correction,path_length,bands) % Find protection factor offset value
            path_length=path_length/1000;%convert meters to km
            if correction<0 %rain rate
                %load rain rate data
                rain_rate=-correction;
                if bands.band>=15
                    if bands.band>=18
                        if bands.band>=22
                            if bands.band>=38 % 38 GHz band
                                a=-0.24;b=9.45;c=-33.5;
                                CF_40=a*path_length^2+b*path_length+c;
                                a=-0.75;b=15.24;c=-35;
                                CF_60=a*path_length^2+b*path_length+c;
                                a=-0.9874;b=18.793;c=-33.754;
                                CF_80=a*path_length^2+b*path_length+c;
                                protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
                                if protect_corr<-25.5,protect_corr=-25.5;end
                            else % 22 GHz band
                                a=-0.0815;b=3.9605;c=-34.584;
                                CF_40=a*path_length^2+b*path_length+c;
                                a=-0.1266;b=5.4266;c=-31.49;
                                CF_60=a*path_length^2+b*path_length+c;
                                a=-0.2017;b=7.0796;c=-30.828;
                                CF_80=a*path_length^2+b*path_length+c;
                                protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
                                if protect_corr<-23.5,protect_corr=-23.5;end
                            end
                        else % 18 GHz band
                            a=16.432;b=54.212;c=0;
                            CF_40 = a*log(path_length)-b;
                            a=19.768;b=52.135;c=0;
                            CF_60= a*log(path_length)-b;
                            a=20.439;b=47.256;c=0;
                            CF_80= a*log(path_length)-b;
                            protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
                            if protect_corr<-30,protect_corr=-30;end
                        end
                    else % 15 GHz band
                        pl_save=path_length;
                        if path_length<5,path_length=5;end
                        a=11.166;b=47.827;c=0;
                        CF_40 = a*log(path_length)-b;
                        a=13.833;b=47.227;c=0;
                        CF_60= a*log(path_length)-b;
                        a=13.097;b=39.813;c=0;
                        CF_80= a*log(path_length)-b;
                        protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
                        if pl_save<5, protect_corr=pl_save*((protect_corr-(-40))/(5-0))+(-40);end %linear interpolation between 5 & 0 km.
                    end
                else
                    protect_corr=0;
                    display('WARNING - rain rate CF undefined for band ',num2str(bands.band),'GHz');
                end
            else %path length
                path_lenth_corr=correction;
                %bands.band
                if str2double(bands.band)<=5
                    protect_corr_5 = 15.798*log(path_length)-73.5;
                    protect_corr_10= 15.664*log(path_length)-68.725;
                    protect_corr_20= 15.174*log(path_length)-62.23;
                    protect_corr=interp_PLCF(path_lenth_corr,protect_corr_5,protect_corr_10,protect_corr_20);
                    if protect_corr<-26.5,protect_corr=-26.5;end %PLCF floor value
                    if path_length>110, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
                elseif str2double(bands.band)<=8
                    protect_corr_5 = 15.633*log(path_length)-70.232;
                    protect_corr_10= 15.924*log(path_length)-66.753;
                    protect_corr_20= 15.151*log(path_length)-59.297;
                    protect_corr=interp_PLCF(path_lenth_corr,protect_corr_5,protect_corr_10,protect_corr_20);
                    if protect_corr<-34,protect_corr=-34;end %PLCF floor value
                    if path_length>100, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
                elseif str2double(bands.band)<=11
                    protect_corr_5 = 15.56*log(path_length)-61.962;
                    protect_corr_10= 15.523*log(path_length)-57.451;
                    protect_corr_20= 15.83*log(path_length)-53.951;
                    protect_corr=interp_PLCF(path_lenth_corr,protect_corr_5,protect_corr_10,protect_corr_20);
                    if protect_corr<-26,protect_corr=-26;end %PLCF floor value
                    if path_length>70, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
                elseif str2double(bands.band)<=13
                    protect_corr_5 = 15.837*log(path_length)-57.755;
                    protect_corr_10= 15.779*log(path_length)-53.901;
                    protect_corr_20= 15.79*log(path_length)-48.684;
                    protect_corr=interp_PLCF(path_lenth_corr,protect_corr_5,protect_corr_10,protect_corr_20);
                    if protect_corr<-20,protect_corr=-20;end %PLCF floor value
                    if path_length>70, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
                else
                    protect_corr=0;
                end
            end
        end
        
        function [protect_corr]=interp_PLCF(path_lenth_corr,protect_corr_5,protect_corr_10,protect_corr_20)
            if path_lenth_corr<=5
                protect_corr=protect_corr_5;
            elseif path_lenth_corr<=10
                protect_corr=((path_lenth_corr-5)*(-protect_corr_10+protect_corr_5)/(10-5)-protect_corr_5)
            elseif path_lenth_corr<=20
                protect_corr=((path_lenth_corr-10)*(-protect_corr_20+protect_corr_10)/(20-10)-protect_corr_10)
            else
                protect_corr=protect_corr_20;
                display('Warning Path Length correction factor undefined for PL value')
            end
            
        end
        
        function [protect_corr]=interp_RRCF(path_lenth_corr,protect_corr_5,protect_corr_10,protect_corr_20)
            if path_lenth_corr<=5
                protect_corr=protect_corr_5;
            elseif path_lenth_corr<=10
                protect_corr=((path_lenth_corr-5)*(-protect_corr_10+protect_corr_5)/(10-5)-protect_corr_5)
            elseif path_lenth_corr<=20
                protect_corr=((path_lenth_corr-10)*(-protect_corr_20+protect_corr_10)/(20-10)-protect_corr_10)
            else
                protect_corr=protect_corr_20;
                display('Warning Path Length correction factor undefined for PL value')
            end
        end
    end
end
