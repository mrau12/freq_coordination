classdef Test1 < matlab.unittest.TestCase
       
    properties
    end
    
    methods (Test)
        function testApparatusLocation(testCase)
           locApp = ApparatusLocation(-33.791132,151.087255);
           %actual output from saved in locApp
           actualSol1 = locApp.getLong();
           actualSol2 = locApp.getLat();
           %expected output
           expected1 = [151.087255];
           expected2 = [-33.791132];
           
           testCase.verifyEqual(actualSol1,expected1);
           testCase.verifyEqual(actualSol2,expected2);
        end   
    end
    
end

