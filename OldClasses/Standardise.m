tic
% Root folder for program.
rootPath = 'C:\Users\mike\Dropbox\SAT\Matlab\4. Licencing\4_4_2016';
%% Standarise data files from folder (\Antennas), so lookupAntDatabase(App) has uniform text files.
% New standardised antenna patterns.
standardFolder = 'stdAntennas';

% Old mixed antenna patterns.
nonStdFolderName = 'Antennas';

files = readAntennaPattern(nonStdFolderName,rootPath,standardFolder);
getFileList(files);
makeStdDirectory(files);

% IF file is (.dat, .txt, .adf ,.pla) THEN
% renames old patterns extension (.txt) and copies patterns to new
% directory.
copyAllFiles(files);

%% Second files object 'files2' used to create additional folder for copying
% .bla into. Bla files are troublesome files that need to be separately
% analysed.
root = 'C:\Users\mike\Dropbox\SAT\Matlab\4. Licencing\4_4_2016';
stdF = 'stdAntennasBLA';
nonStdF = 'Antennas';
files2 = readAntennaPattern(nonStdF,root,stdF);
getFileList(files2);
makeStdDirectory(files2);
copyAllFilesBla(files2);

%% Make new pattern object for troublesome files
filesBla = readAntennaPattern(stdF,root);
getFileList(filesBla);
filesBla.deleteCharLines(1)
toc