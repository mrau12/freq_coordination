function pass = passcode(charset);
%PASSCODE  password input dialog box.
%  passcode creates a modal dialog box that returns user password input.
%  Given characters are substituted with '*'-Signs like in usual Windows dialogs.
%
%  usage:
%  answer = PASSCODE
%     without input parameter allows to type any ASCII-Character
%  answer = PASSCODE('digit')
%     allows only digits as input characters [0-9]
%  answer = PASSCODE('letter')
%     allows only letters as input characters [a-z_A-Z]
%  answer = PASSCODE(<string>)
%     allows to use characters from the specified string only
%
%  See also PCODE.

% Version: v1.2 (03-Mar-2008)
% Author:  Elmar Tarajan [MCommander@gmx.de]

if nargin==0
    charset = default;
else
    if any(strcmp({'letter' 'digit'},charset))
        charset = eval(charset);
    elseif ~isa(charset,'char')
        error('string expected. Check input parameters.')
    end% if
end% if
%

ScreenSize = get(0,'ScreenSize');
hfig = figure('Menubar','none', ...
    'Units','Pixels', ...
    'Resize','off', ...
    'NumberTitle','off', ...
    'Name',['password required'], ...
    'Position',[ (ScreenSize(3:4)-[300 75])/2 300 200], ...
    'Color',[0.8 0.8 0.8], ...
    'WindowStyle','modal');
hedit = uicontrol('Parent',hfig, ...
    'Style','Edit', ...
    'Enable','inactive', ...
    'Units','Pixels','Position',[50 125 202 22], ...
    'FontSize',15, ...
    'String',[], ...
    'BackGroundColor',[0.7 0.7 0.7]);
hpass = uicontrol('Parent',hfig, ...
    'Style','Text', ...
    'Tag','password', ...
    'Units','Pixels','Position',[50 125 200 20], ...
    'FontSize',15, ...
    'BackGroundColor',[1 1 1]);
hwarn = uicontrol('Parent',hfig, ...
    'Style','Text', ...
    'Tag','error', ...
    'Units','Pixels','Position',[50 2 200 20], ...
    'FontSize',8, ...
    'String','character not allowed',...
    'Visible','off',...
    'ForeGroundColor',[1 0 0], ...
    'BackGroundColor',[0.8 0.8 0.8]);

cancel = uicontrol(hfig,'Style','pushbutton','String','Cancel',...
    'Position',[160 70 60 40],'Callback', 'set(gcbo,''userdata'',1)');

ok = uicontrol(hfig,'Style','pushbutton','String','Ok',...
    'Position',[80 70 60 40]);
%
set(hfig,'KeyPressFcn',{@keypress_Callback,hedit,hpass,hwarn,charset}, ...
    'CloseRequestFcn','uiresume')
%
uiwait
pass = get(hpass,'String');
compare1 = strcmp(pass,'matlab123');
set(handles.btnStop, 'userdata', 0);
while ~compare1
    set(hfig,'KeyPressFcn',{@keypress_Callback,hedit,hpass,hwarn,charset}, ...
        'CloseRequestFcn','uiresume')
    uiwait
   if get(cancel, 'userdata') % stop condition
		break;
	end
    pass = get(hpass,'userdata');
    compare1 = strcmp(pass,'matlab123');
    delete(hfig)
end

delete(hfig)

end
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function keypress_Callback(hObj,data,hedit,hpass,hwarn,charset)
%--------------------------------------------------------------------------
pass = get(hpass,'userdata');
%
switch data.Key
    case 'backspace'
        pass = pass(1:end-1);
        %
    case 'return'
        uiresume
        return
        %
        
    otherwise
        try
            if any(charset == data.Character)
                pass = [pass data.Character];
            else
                set(hwarn,'Visible','on')
                pause(0.5)
                set(hwarn,'Visible','off')
            end% if
        end% try
        %
end% switch
%
set(hpass,'userdata',pass)
set(hpass,'String',char('*'*sign(pass)))
end

function Cancel(hObject,eventdata)
% hObject    handle to cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.btnStop,'userdata',1)
end
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function charset = default
%--------------------------------------------------------------------------
% charset = [letter digit '<>[]{}()@!?*#=~-+_.,;:�$%&/|\'];
charset = char(1:255);
%
%
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function charset = digit
%--------------------------------------------------------------------------
charset = '0123456789';
%
%
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function charset = letter
%--------------------------------------------------------------------------
charset = char([65:90 97:122]);
%
%
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%