function varargout = Pass(varargin)
% GOSTOP MATLAB code for gostop.fig
%      GOSTOP, by itself, creates a new GOSTOP or raises the existing
%      singleton*.
%
%      H = GOSTOP returns the handle to a new GOSTOP or the handle to
%      the existing singleton*.
%
%      GOSTOP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GOSTOP.M with the given input arguments.
%
%      GOSTOP('Property','Value',...) creates a new GOSTOP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gostop_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gostop_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gostop

% Last Modified by GUIDE v2.5 21-Jun-2016 15:31:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Pass_OpeningFcn, ...
    'gui_OutputFcn',  @Pass_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gostop is made visible.
function Pass_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gostop (see VARARGIN)

% Choose default command line output for gostop
handles.output = hObject;

% Update handles structure

% UIWAIT makes gostop wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Pass_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Ok.
function Ok_Callback(hObject, eventdata, handles)
% hObject    handle to Ok (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.Cancel, 'userdata', 0);
pass = get(handles.Password, 'String');
compare1 = strcmp(pass,'matlab123');
while ~compare1
    uiwait
    pass = get(handles.Password, 'string');
    if get(handles.Cancel, 'userdata') % stop condition
        break;
    end
    compare1 = strcmp(pass,'matlab123');
end
GUI;
close(Pass);
% guidata(hObject, handles);
% key = get(gcf,'CurrentKey');
% if(strcmp (key , 'return'))
%     pass = get(handles.Password, 'string');
% end
% --- Executes on button press in Cancel.
function Cancel_Callback(hObject, eventdata, handles)
% hObject    handle to Cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.Cancel,'userdata',1)
close(Pass);


function Password_Callback(hObject, eventdata, handles)
% hObject    handle to password (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of password as text
%        str2double(get(hObject,'String')) returns contents of password as a double
