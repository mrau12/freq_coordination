classdef availableChannels < handle
    %AVAIBLECHANNELS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        exBand;
        comparsionChanApp;
        protectionMargin;
        protectionRatioTable;
       
        channel_Inteferes_TxLo;
        channel_Inteferes_TxHi;
        coch_attenuation_TxLo;
        coch_attenuation_TxHi;
        adj_attenuation_TxLo;
        adj_attenuation_TxHi;
    end
    
    methods
        function avChannels = availableChannels(exBand,comparsionChanApp)
            avChannels.exBand = exBand;
            avChannels.comparsionChanApp = comparsionChanApp;
            avChannel.protectionMargin
        end
      
        function testChanngelOverlap(avChannels)
            sym f;            
            for channel = 1:avChannels.exBand.numberOfChannels;
                start1 = avChannels.exBand(channel).LOWER_FREQ;
                stop1 = avChannels.exBand(channel).UPPER_FREQ;
                start2 = avChannels.comparsionChanApp.LOWER_FREQ;
                stop2 = avChannels.comparsionChanApp.UPPER_FREQ;
                adjBoundary1 = start1 - bandwidth/2;
                adjBoundary2 = stop1 + bandwidth/2;
                ch1 = heaviside(f-start1)-heaviside(f-stop1);
                ch2 = heaviside(f-start2)-heaviside(f-stop2);
                ch3 = heaviside(f-adjBoundary1)-heaviside(f-adjBoundary2);
            end
            avChannels.coChanOverlap(ch1,ch2)
            avChannels.adjChanOverlap(ch1,ch3)
        end
        
        function coChanOverlap(avChannels,ch1,ch2)
            if solve((ch1*ch2)==0)
                avChannels.protectionMargin = 60;
            else
                avChannels.protectionMargin = 0;
            end
        end
        
        function adjChanOverlap(avChannels,ch1,ch3)
            if solve((ch1*ch3)==0)
                avChannels.protectionMargin = 30;
            else
                avChannels.protectionMargin = 0;
            end
        end
    end
end


