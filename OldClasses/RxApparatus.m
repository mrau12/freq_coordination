% Michael McClellan
% SAT PTY LTD
% Created: OCT 2015
% Modified: APR 2016 08:00:00
% Revision 0.20
% Class Summary goes here
%  MVC: Model
% Examples:
%
% Provide sample usage code here
%
% See also: List related files here
classdef RxApparatus
    % write a description of the class here.
    properties
        % define the properties of the class here, (like fields of a struct)
        Receieve;
        angleAlpha;
        angleBeta
        angleDelta;
        angleGamma
        gainAw;
        gainAwt;
        emission;
        Latitude;
        Longitutde;
    end
    methods
        % constructor
        function obj = RxApparatus(latC, longC)
          obj.Latitude = latC;
          obj.Longitutde = longC;
        end
        % methods
        
    end
end