function varargout = passw(varargin)
% PASSW MATLAB code for passw.fig
%      PASSW, by itself, creates a new PASSW or raises the existing
%      singleton*.
%
%      H = PASSW returns the handle to a new PASSW or the handle to
%      the existing singleton*.
%
%      PASSW('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PASSW.M with the given input arguments.
%
%      PASSW('Property','Value',...) creates a new PASSW or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before passw_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to passw_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help passw

% Last Modified by GUIDE v2.5 21-Jun-2016 11:20:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @passw_OpeningFcn, ...
                   'gui_OutputFcn',  @passw_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
end

% --- Executes just before passw is made visible.
function passw_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to passw (see VARARGIN)

% Choose default command line output for passw
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes passw wait for user response (see UIRESUME)
% uiwait(handles.figure1);
end

% --- Outputs from this function are returned to the command line.
function varargout = passw_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
end


function username_Callback(hObject, eventdata, handles)
% hObject    handle to username (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of username as text
%        str2double(get(hObject,'String')) returns contents of username as a double
end

% --- Executes during object creation, after setting all properties.
function username_CreateFcn(hObject, eventdata, handles)
% hObject    handle to username (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

end

function pass_Callback(hObject, eventdata, handles)
% hObject    handle to pass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pass as text

end

% --- Executes during object creation, after setting all properties.
function pass_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

end
% --- Executes on button press in ok.
function ok_Callback(hObject, eventdata, handles)
% hObject    handle to ok (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

end
% --- Executes on button press in cancel.
function cancel_Callback(hObject, eventdata, handles)
% hObject    handle to cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(passw);
end


% --- Executes on key press with focus on pass and none of its controls.
function pass_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to pass (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
%        str2double(get(hObject,'String')) returns contents of pass as a double
% SizePass = size(get(handles.pass,'String')); % Find the number of asterisks
% if SizePass(2) > 0
%     asterisk(1,1:SizePass(2)) = '*'; % Create a string of asterisks the same size as the password
%     set(handles.pass,'String',asterisk) % Set the text in the password edit box to the asterisk string
% else
%     set(handles.pass,'String','')
% end
password = get(handles.pass,'String');
key = get(passw,'currentkey');

switch key
    case 'backspace'
        password = password(1:end-1); % Delete the last character in the password   
    case 'escape'
        % Close the login dialogj
        close(passw,[])
    otherwise
        password = [password; get(passw,'currentcharacter')]; % Add the typed character to the password
end
set(handles.pass,'String',password) % Store the password in its current state
set(hpass,'userdata',pass)
%set(hpass,'String',char('*'*sign(pass)))
end