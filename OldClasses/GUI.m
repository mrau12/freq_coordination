function varargout = GUI(varargin)
% GUI MATLAB code for GUI.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI

% Last Modified by GUIDE v2.5 08-Oct-2015 14:29:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end
if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
% To main code is in Function "pushbutton1_Callback"

% --- Executes just before GUI is made visible.
function GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI (see VARARGIN)

% Choose default command line output for GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in Get_data.
function Get_data_Callback(hObject, eventdata, handles)
% hObject    handle to Get_data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Site A
lat_C=str2double(get(handles.latitude_C,'String'));
long_C=str2double(get(handles.longitude_C,'String'));

% Site B
lat_D=str2double(get(handles.latitude_D,'String'));
long_D=str2double(get(handles.longitude_D,'String'));

% Midpoint
lat_mid=(lat_C+lat_D)/2;
long_mid=(long_C+long_D)/2;

%% get band info
band_index= get(handles.band,'Value'); %Gets frequncy units

%% find and display interfers
%raidus=200;
raidus=str2double(get(handles.raidus,'String'));

%%%data=SQLlookup(long_mid,lat_mid,frequency1,frequency2,raidus); % access SQL database return as cell
%%%set(handles.result,'String',length(data)-1); % display number of items

%% plot area
if isequal(data{:},'No Data')
    display('Please adjust search paramaters.');
else
    if length(data)>1
        axes(handles.axes1);
        xlim([lat_mid-2,lat_mid+2]); axis equal off
        length(data);
        %figs=scatter([long_D;long_C],[lat_D;lat_C],'kp','LineWidth',5);
        cla(handles.axes1)
        figs=scatter([long_D;long_C],[lat_D;lat_C],'kp','LineWidth',5);
        hold on;
        scatter([data{:,11}],[data{:,10}]);
        hold off;
        plot_google_map;
        hold off;
    else
        axes(handles.axes1);
        figs=scatter([lat_D;long_C],[lat_D;long_C],'kp','LineWidth',5);
        plot_google_map;
    end
    %savefig('test.fig')
    filename=get(handles.Job_Name,'String');
mkdir('jobs',filename);%make new directory
    saveas(handles.axes1,['jobs/',filename,'/',filename,datestr(date,30),'.jpg']);
    %saveas(figs,['jobs/',filename,'/',filename,'.jpg']);
    filename=['jobs/',filename,'/',datestr(date),'.xls'];
    %% write to data to file
    columns={'ACCESSID',...
        'FREQASS',...
        'MODEA',...
        'MODEB',...
        'TXPOWER',...
        'ANTAZ',...
        'ANTPOL',...
        'HEIGHT',...
        'SITEID',...
        'LATITUDE',...
        'LONGITUDE',...
        'ANTID',...
        'AUTHORISATIONDATE',...
        'EMISSION'...
        'ANTGAIN'...
        'ANTMODEL',...
        'ANTSIZE'};
size(columns)
T = cell2table(data,'VariableNames',columns);
writetable(T,filename,'Sheet','ACMA_cull');
end

%% --- Executes on button press in Coordinate.
function Coordinate_Callback(hObject, eventdata, handles)
% hObject    handle to Coordinate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%% Site C
lat_C=str2double(get(handles.latitude_C,'String'));
long_C=str2double(get(handles.longitude_C,'String'));
% Site D
lat_D=str2double(get(handles.latitude_D,'String'));
long_D=str2double(get(handles.longitude_D,'String'));
% Midpoint
lat_mid=(lat_C+lat_D)/2;
long_mid=(long_C+long_D)/2;
%% Get band info
band_index= get(handles.band,'Value'); %Gets frequncy units
conn = database.ODBCConnection('acma','matlab','matlab'); %matlab>2014
SQL_command=['SELECT * FROM  acma_new.bands WHERE indexb=',num2str(band_index)];
curs=exec(conn,SQL_command); %defines a command
SLQreturn=fetch(curs); %executes command
close(conn)
band = SLQreturn.Data;
names={'band','Type','fo','fl','fh','channels','bandwidth','TxRx_gap','Blackouts','channel_start','path_min','restrictions','page'};
bands=cell2table(band(2:end),'VariableNames',names);
frequency1=bands.fl;
frequency2=bands.fh;
raidus=str2double(get(handles.raidus,'String'));
data=SQLlookup(long_mid,lat_mid,frequency1,frequency2,raidus); % access SQL database return as cell
%% Establish new sites
%% LOS angle of sites
x=[lat_C,lat_D]; % Site 1 and 2 GDA94 Longs
y=[long_C,long_D]; % Site 1 and 2 GDA94 Lats
ang2B=atan2((y(1)-y(2)),(x(1)-x(2)));
ang2A=ang2B+pi;
%% Tx Power
%% Antenna
%% Polarization
switch get(handles.popupmenu5,'Value')
    case 1, pol='V'; % Vertical
    case 2, pol='H'; % Horizontal
end
%% Site builder
% columns=['ACCESS_ID,',...
%         'FREQ_ASS,',...
%         'MODE_,',...
%         'MODE_,',...
%         'TX_POWER,',...
%         'ANT_AZ,',...
%         'ANT_POL,',...
%         'HEIGHT,',...
%         'assign.SITE_ID,',...
%         'site.LATITUDE,',...
%         'site.LONGITUDE,',...
%         'assign.ANT_ID,',...
%         'AUTHORISATION_DATE,',...
%         'EMISSION,'...
%         'antenna.ANT_GAIN,'...
%         'antenna.ANT_MODEL',...
%         'antenna.ANT_SIZE,'];
%% get antenna size
% 0.3 1
% 0.6 2
% 0.8 3
% 1.2 4
% 1.8 5
% 2.4 6
% 3.0 7
% 3.7 8
% 4.6 9
switch get(handles.ant_size,'Value') %Gets frequncy units
    case 1,ANT_SIZE=0.3; % meters
    case 2,ANT_SIZE=0.6;
    case 3,ANT_SIZE=0.8;
    case 4,ANT_SIZE=1.2;
    case 5,ANT_SIZE=1.8;
    case 6,ANT_SIZE=2.4;
    case 7,ANT_SIZE=3.0;
    case 8,ANT_SIZE=3.7;
    case 9,ANT_SIZE=4.6;
end
%% set new link antenna
SITE_ID='4123';
date=datestr(now,29);
HEIGHT=5;
%TX_POWER=1;
%TX_POWER=get(handles.Txpower,'String');
TX_POWER=get(handles.Txpower,'Value');
ANT_ID=bands.band;
bore_gain=10*log10(0.6*(frequency1*pi*ANT_SIZE/(3*10^8))^2);
ANT_MODEL='DEFAULT';
%ANT_MODEL=ANT_MODEL{:};
switch get(handles.popupmenu4,'Value')
    case 1 %Tx/Rx TxHi@A
        SiteC={'0000',frequency1,'T','R',TX_POWER,ang2A,pol,HEIGHT,SITE_ID,(lat_C),(long_C),ANT_ID,date,'XXXX',bore_gain,ANT_MODEL,ANT_SIZE}; %TxHi 20067=NOTIONAL
        %SiteC={'0000',frequency1,'T','R',1,ang2A,pol,'XXXX',default_antenna,(lat_C),(long_C),ANT_ID,date,'XXXX',bore_gain,ANT_MODEL}; %TxHi 20067=NOTIONAL
        SiteD={'0000',frequency1,'R','T',TX_POWER,ang2B,pol,HEIGHT,SITE_ID,(lat_D),(long_D),ANT_ID,date,'XXXX',bore_gain,ANT_MODEL,ANT_SIZE}; %TxLo
    case 2 %Tx/Rx TxHi@B
        SiteC={'0000',frequency1,'R','T',TX_POWER,ang2A,pol,HEIGHT,SITE_ID,(lat_C),(long_C),ANT_ID,date,'XXXX',bore_gain,ANT_MODEL,ANT_SIZE}; %TxLo
        SiteD={'0000',frequency1,'T','R',TX_POWER,ang2B,pol,HEIGHT,SITE_ID,(lat_D),(long_D),ANT_ID,date,'XXXX',bore_gain,ANT_MODEL,ANT_SIZE}; %TxHi
    case 3 %Tx @ A
        SiteC={'0000',frequency1,'T','',TX_POWER,ang2A,pol,HEIGHT,SITE_ID,(lat_C),(long_C),ANT_ID,date,'XXXX',bore_gain,ANT_MODEL,ANT_SIZE}; 
        SiteD={'0000',frequency1,'R','',[NaN],ang2B,pol,HEIGHT,SITE_ID,(lat_D),(long_D),ANT_ID,date,'XXXX',bore_gain,ANT_MODEL,ANT_SIZE};
    case 4  %Tx @ B
        SiteC={'0000',frequency1,'R','',[NaN],ang2A,pol,HEIGHT,SITE_ID,(lat_C),(long_C),ANT_ID,date,'XXXX',bore_gain,ANT_MODEL,ANT_SIZE};
        SiteD={'0000',frequency1,'T','',TX_POWER,ang2B,pol,HEIGHT,SITE_ID,(lat_D),(long_D),ANT_ID,date,'XXXX',bore_gain,ANT_MODEL,ANT_SIZE};
end
%% Path Length correction
if get(handles.Rain,'Value')
    correction=get(handles.Rain_Rate,'Value');% gets the rain rate in (mm)
else
    correction=-get(handles.Rain_Rate,'Value');
end
%% Execte and save
[avaliable_channels,data_dump]=cordinate(SiteC,SiteD,data,correction,bands); % Cordinate 
filename=get(handles.Job_Name,'String');
%saveas(figs,['jobs/',filename,'.jpg'])
mkdir('jobs',filename);%make new directory
saveas(handles.axes1,['jobs/',filename,'/',filename,datestr(date,30),'.jpg']);
filename=['jobs/',filename,'/',datestr(date),'.xls'];
ava_names={'Channel','Site_Sense','Nearest_CoInterferer','Nearest_AdjInterferer','Frequency','Avaliability'};
T = array2table(avaliable_channels,'VariableNames',ava_names);

writetable(cell2table([SiteC(:)';SiteD(:)']),filename,'Sheet','Inputs')
writetable(T,filename,'Sheet','AvaChannels'); % write freq data to file
writetable(data_dump,filename,'Sheet','DataDump'); % write freq data to file

%% Site builder data types
%% Fetch Data


function latitude_C_Callback(hObject, eventdata, handles)
% hObject    handle to latitude_C (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of latitude_C as text
%        str2double(get(hObject,'String')) returns contents of latitude_C as a double


% --- Executes during object creation, after setting all properties.
function latitude_C_CreateFcn(hObject, eventdata, handles)
% hObject    handle to latitude_C (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function longitude_C_Callback(hObject, eventdata, handles)
% hObject    handle to longitude_C (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of longitude_C as text
%        str2double(get(hObject,'String')) returns contents of longitude_C as a double


% --- Executes during object creation, after setting all properties.
function longitude_C_CreateFcn(hObject, eventdata, handles)
% hObject    handle to longitude_C (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function freq_Callback(hObject, eventdata, handles)
% hObject    handle to freq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of freq as text
%        str2double(get(hObject,'String')) returns contents of freq as a double


% --- Executes during object creation, after setting all properties.
function freq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function result_Callback(hObject, eventdata, handles)
% hObject    handle to result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of result as text
%        str2double(get(hObject,'String')) returns contents of result as a double


% --- Executes during object creation, after setting all properties.
function result_CreateFcn(hObject, eventdata, handles)
% hObject    handle to result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1

% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function latitude_D_Callback(hObject, eventdata, handles)
% hObject    handle to latitude_D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of latitude_D as text
%        str2double(get(hObject,'String')) returns contents of latitude_D as a double

% --- Executes during object creation, after setting all properties.
function latitude_D_CreateFcn(hObject, eventdata, handles)
% hObject    handle to latitude_D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function longitude_D_Callback(hObject, eventdata, handles)
% hObject    handle to longitude_D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of longitude_D as text
%        str2double(get(hObject,'String')) returns contents of longitude_D as a double

% --- Executes during object creation, after setting all properties.
function longitude_D_CreateFcn(hObject, eventdata, handles)
% hObject    handle to longitude_D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2

% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double

% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in band.
function band_Callback(hObject, eventdata, handles)
% hObject    handle to band (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns band contents as cell array
%        contents{get(hObject,'Value')} returns selected item from band

% --- Executes during object creation, after setting all properties.
function band_CreateFcn(hObject, eventdata, handles)
% hObject    handle to band (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu4.
function popupmenu4_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu4

% --- Executes during object creation, after setting all properties.
function popupmenu4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu5.
function popupmenu5_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu5 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu5


% --- Executes during object creation, after setting all properties.
function popupmenu5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ant_size.
function ant_size_Callback(hObject, eventdata, handles)
% hObject    handle to ant_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ant_size contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ant_size


% --- Executes during object creation, after setting all properties.
function ant_size_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ant_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Job_Name_Callback(hObject, eventdata, handles)
% hObject    handle to Job_Name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Job_Name as text
%        str2double(get(hObject,'String')) returns contents of Job_Name as a double

% --- Executes during object creation, after setting all properties.
function Job_Name_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Job_Name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in get_range.
function get_range_Callback(hObject, eventdata, handles)
% hObject    handle to get_range (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% hObject    handle to Get_data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Site A
lat_C=str2double(get(handles.latitude_C,'String'));
long_C=str2double(get(handles.longitude_C,'String'));
% Site B
lat_D=str2double(get(handles.latitude_D,'String'));
long_D=str2double(get(handles.longitude_D,'String'));
% Midpoint
lat_mid=(lat_C+lat_D)/2;
long_mid=(long_C+long_D)/2;
%% get band info
% band_index= get(handles.band,'Value'); %Gets frequncy units
% conn = database.ODBCConnection('acma','matlab','matlab');
% SQL_command=['SELECT * FROM  acma_new.bands WHERE indexb=',num2str(band_index)];
% curs=exec(conn,SQL_command); %defines a command
% SLQreturn=fetch(curs); %executes command
% close(conn);
% band = SLQreturn.Data;
% names={'band','Type','fo','fl','fh','channels','bandwidth','TxRx_gap','Blackouts','channel_start','path_min','restrictions','page'};
% bands=cell2table(band(2:end),'VariableNames',names);
switch get(handles.popupmenu7,'Value') %Gets frequncy units
    case 1
        units=10^9;%GHz
    case 2
        units=10^6;%MHz
    case 3
        units=10^3;%KHz
end
frequency1=str2double(get(handles.freq1,'String'))*units;
frequency2=str2double(get(handles.freq2,'String'))*units;
%% find and display interfers
raidus=str2double(get(handles.raidus,'String'));
long_mid,lat_mid,frequency1,frequency2,raidus
data=SQLlookup(long_mid,lat_mid,frequency1,frequency2,raidus); % access SQL database return as cell
set(handles.result,'String',length(data)-1); % display number of items
%% plot area
if length(data)>1
    axes(handles.axes1);
    xlim([lat_mid-2,lat_mid+2]); axis equal off
    length(data);
    figs=scatter([long_D;long_C],[lat_D;lat_C],'kp','LineWidth',5);
    hold on
    scatter([data{:,11}],[data{:,10}]);
    hold off
    plot_google_map;
    hold off
else
     axes(handles.axes1);
     figs=scatter([lat_D;long_C],[lat_D;long_C],'kp','LineWidth',5);
     plot_google_map;
end
%savefig('test.fig')
filename=get(handles.Job_Name,'String');
saveas(figs,['jobs/',filename,'/',filename,'.jpg'])
filename=['jobs/',filename,'/',datestr(date),'.xls'];
%% write to data to file
%names={'Link_ID','Freq','Mode_a','Mode_b','Tx_power','Azemeth','Pol','Height','Site_ID','lat','long','ACMA_ant_No','AUTHORISATION_DATE','EMISSION','ANT_GAIN','ANT_MODEL'};
columns={'ACCESSID',...
        'FREQASS',...
        'MODEA',...
        'MODEB',...
        'TXPOWER',...
        'ANTAZ',...
        'ANTPOL',...
        'HEIGHT',...
        'SITEID',...
        'LATITUDE',...
        'LONGITUDE',...
        'ANTID',...
        'AUTHORISATIONDATE',...
        'EMISSION'...
        'ANTGAIN'...
        'ANTMODEL',...
        'ANTSIZE'};
T = cell2table(data,'VariableNames',columns);
writetable(T,filename,'Sheet','ACMA_cull');


function freq1_Callback(hObject, eventdata, handles)
% hObject    handle to freq1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of freq1 as text
%        str2double(get(hObject,'String')) returns contents of freq1 as a double


% --- Executes during object creation, after setting all properties.
function freq1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu7.
function popupmenu7_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu7 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu7


% --- Executes during object creation, after setting all properties.
function popupmenu7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function freq2_Callback(hObject, eventdata, handles)
% hObject    handle to freq2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of freq2 as text
%        str2double(get(hObject,'String')) returns contents of freq2 as a double


% --- Executes during object creation, after setting all properties.
function freq2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function raidus_Callback(hObject, eventdata, handles)
% hObject    handle to raidus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of raidus as text
%        str2double(get(hObject,'String')) returns contents of raidus as a double


% --- Executes during object creation, after setting all properties.
function raidus_CreateFcn(hObject, eventdata, handles)
% hObject    handle to raidus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Rain_Rate_Callback(hObject, eventdata, handles)
% hObject    handle to Rain_Rate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Rain_Rate as text
%        str2double(get(hObject,'String')) returns contents of Rain_Rate as a double


% --- Executes during object creation, after setting all properties.
function Rain_Rate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Rain_Rate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function Radio_B_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Radio_B (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in Rain.
function Rain_Callback(hObject, eventdata, handles)
% hObject    handle to Rain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Rain


% --- Executes during object creation, after setting all properties.
function PLCF_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PLCF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function Rain_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Rain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function [antennas] =get_defult_antennas(band)



function Txpower_Callback(hObject, eventdata, handles)
% hObject    handle to Txpower (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Txpower as text
%        str2double(get(hObject,'String')) returns contents of Txpower as a double


% --- Executes during object creation, after setting all properties.
function Txpower_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Txpower (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
