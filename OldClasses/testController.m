clear;
clc;
%-----------------> setup 1 variables

%outputApp3 = Commandline();

%-----------------> setup 2 variables
radius = 100; 
bandIndex = 8;
latitude1 = -33.791132;
longitude1 = 151.087255;
latitude2 = -33.772771;
longitude2 = 151.081349;
antSize = 2.4;
TxPower = 5;
ModelID = 'DEFAULT';
TxRx = 1; %Tx/Rx TxHi@A
correction = 0; % rain in mm

%-----------------> setup database
loc3 = ApparatusLocation(latitude1,longitude1);
loc4 = ApparatusLocation(latitude2,longitude2);
signal3_4 = ApparatusSignal(TxPower,ModelID); %Can input ant gain too
transmit3_4 = ApparatusTransmitType(TxRx);
data = Database(radius, bandIndex);%200 radius search & bandindex 15 is 8 Ghz
data.setMidCoord(loc3.latitude,loc3.longitude,loc4.latitude,loc4.longitude);
data.setBands();
data.getRawData();

%-----------------> apparatus
bandsAppy = ApparatusBands(antSize,data);
bandsAppy.setBoreGain(); %Make sure freq1 & antennaSize first
sorted = data.sortRawData();
AppC = Apparatus(signal3_4,bandsAppy,loc3,transmit3_4);
AppD = Apparatus(signal3_4,bandsAppy,loc4,transmit3_4);

%------------------> coordinate
coordinate = Coordinate(AppC,AppD,sorted,getBands(data),correction);

tic
%experimentLinkMatch
for device=1:size(coordinate.sortedData,1)
    coordinate.loadApp1orApp2(device);   
end
for device=1:size(coordinate.sortedData,1)
    if compareLinkID(coordinate,device)
     dataDump(device+1) = DataDump(coordinate,data,correction);
    end
   
end



toc