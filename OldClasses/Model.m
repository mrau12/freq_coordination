classdef Model
    %MODEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        dataDump; %writes to excel file?
        availableChannels;
        sumBudgetLinks;
        ApparatusA;
        ApparatusB;
        ApparatusC;
        ApparatusD;
        Database;
    end
        % methods
    methods    
        % creates SiteA
        function makeSite(Datab, Apparatus)
            control.ApparatusA = Apparatus();
            control.ApparatusA = sortRawData(Datab);
        end
        
        function [availableChannels,dataDump] = cordinate(sorted)
            sorted.checkDate();          
            assessInterfers();            
            InterferFreqDisplay();           
            check_data();% checks SQL database date
            % if channel is ok repeat for return path.
        end
        
        function InterferFreqDisplay(data_dump)
            data_dump(1,:)=[];%removes first row
            %% Display interfers in the frequency domain
            noise_floor=0;%dB
            for i=1:bands.channels
                s=bands.channel_start+bands.bandwidth*i; % Channel center
                s_l=s-(bands.bandwidth/2);% lower 3dD
                s_u=s+(bands.bandwidth/2);% upper 3dD
                channel_Inteferes_TxLo{i}=data_dump.Prot_Margin_B((data_dump.Site_Freq>s_l) & (data_dump.Site_Freq<s_u),1);
                channel_Inteferes_TxHi{i}=data_dump.Prot_Margin_B((data_dump.Site_Freq>(s_l+bands.TxRx_gap)) & (data_dump.Site_Freq<(s_u+bands.TxRx_gap)),1);
                coch_attenuation_TxLo=min(channel_Inteferes_TxLo{i});
                coch_attenuation_TxHi=min(channel_Inteferes_TxHi{i});
                adj_attenuation_TxLo=min(channel_Inteferes_TxLo{i}+30);
                adj_attenuation_TxHi=min(channel_Inteferes_TxHi{i}+30);
                plot([s-(1.2*bands.bandwidth/2),s_l,s_u,s+(1.2*bands.bandwidth/2)],[noise_floor,coch_attenuation_TxLo,coch_attenuation_TxLo,noise_floor]);
                if length(num2str(i))>1,offset='  '; else offset=' '; end
                text(s,coch_attenuation_TxLo,[num2str(i);offset]); %plots freespace path loss
                if i==1; hold on, end
                plot(bands.TxRx_gap+[s-(1.2*bands.bandwidth/2),s_l,s_u,s+(1.2*bands.bandwidth/2)],[noise_floor,coch_attenuation_TxHi,coch_attenuation_TxHi,noise_floor])
                text(bands.TxRx_gap+s,coch_attenuation_TxHi,[num2str(i);offset]); %plots freespace path loss
                avaliable_channels(i*2-1,1)=i;
                avaliable_channels(i*2,1)=i;
                avaliable_channels(i*2-1,2)=0;
                avaliable_channels(i*2,2)=1;
                avaliable_channels(i*2-1,3)=coch_attenuation_TxLo;
                avaliable_channels(i*2,3)=coch_attenuation_TxHi;
                avaliable_channels(i*2-1,4)=adj_attenuation_TxLo;
                avaliable_channels(i*2,4)=adj_attenuation_TxHi;
                avaliable_channels(i*2-1,5)=bands.fo+bands.bandwidth*i;
                avaliable_channels(i*2,5)=bands.fo+bands.bandwidth*i+bands.TxRx_gap;
                avaliable_channels(i*2-1,6)=avaliable_channels(i*2-1,3)>0;%Avaliability
                avaliable_channels(i*2,6)=avaliable_channels(i*2,3)>0;
            end
        end
        
        
        function assessInterfers(sorted)
            current_linkID=0;
            flag_co_channel=[];
            j=1;
            for i=1:size(sorted,1) % assess each Interferer criteria
                %% Match - finds matching "link ID's"
                if isequal(current_linkID,sorted{i,1}) % tests if current device matches data saved for siteA
                    j=j+1;
                    control.ApparatusB=sorted(i,1:end); %load new Interferer Site B
                    [A2B_link_budget(j),A2D_link_budget(j),C2B_link_budget(j),C2D_link_budget(j),A2B_pl(j),A2D_pl(j),C2B_pl(j),C2D_pl(j)]=find_signal_level(control.ApparatusA,control.ApparatusB,control.ApparatusC,control.ApparatusD);% finds and plots link budgets
                    CoCh_Isolation_B(j)=A2B_link_budget(j)-C2B_link_budget(j);
                    CoCh_Isolation_D(j)=C2D_link_budget(j)-A2D_link_budget(j);
                    %% Protection Ratios
                    CoCh_pro_D(j)=-60+correctionfactor(correction,A2D_pl(j),bands);
                    CoCh_pro_B(j)=-60+correctionfactor(correction,C2B_pl(j),bands);
                    AdjCh_pro_D(j)=-30+correctionfactor(correction,A2D_pl(j),bands);
                    AdjCh_pro_B(j)=-30+correctionfactor(correction,C2B_pl(j),bands);
                    %% Protection Margin - if negitive we can't use that channel
                    Prot_Margin_B(j)=CoCh_Isolation_B(j)+CoCh_pro_B(j);
                    Prot_Margin_D(j)=CoCh_Isolation_D(j)+CoCh_pro_D(j);
                    %% find avaliable channels
                    new_site(j)=siteA{1,2}; % frequency/channel
                else
                    current_linkID=sorted{i,1};% save link ID to compare to find matching pair
                    siteA=Sorted_data(i,1:end); % load next device to find its matching end
                end
            end
        end
        
        function platformSupported()
            if ismac
                disp('Platform not supported')
            elseif isunix
                disp('Platform not supported')
            elseif ispc
                disp('Platform is supported')
            else
                disp('Platform not supported')
            end
        end
    end
end