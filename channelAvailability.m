classdef channelAvailability < handle
    %CHANNELAVAILBILITY 
    % Creates a table of available (marked in table as 1's) or unavailable
    % (0's). If Protection Ratio less than wanted2unwanted then 1's for
    % that channel, else 0's
    
    properties
        protR;
        dataDump;
        available;
        unavailable;
        availabilityTable;
    end
    
    methods
        % Input: 
        % protRatio is old protM.protMarginAr
        % dataDump
        function chanAvail = channelAvailability(protRatio,dataDump)
              chanAvail.protR = protRatio;
              chanAvail.dataDump = dataDump;              
              chanAvail.availabilityTable = [];
        end
        
        % PR  < W2D
        function equalityFunctonB(chanAvail)
            if chanAvail.protR(index,index2) > chanAvail.dataDump(1,index3).wantedToUnwantedSignalB
                chanAvail.availabilityTable(index4,index5) = 1;
            else
                chanAvail.availabilityTable(index4,index5) = 0;
            end
        end
        
        function equalityFunctonA(chanAvail)
            if chanAvail.protR(index,index2) > chanAvail.dataDump(1,index3).wantedToUnwantedSignalA
                chanAvail.availabilityTable(index4,index5) = 1;
            else
                chanAvail.availabilityTable(index4,index5) = 0;
            end
        end
    end    
end

