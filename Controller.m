% Michael McClellan
% SAT PTY LTD
% Created: APR 2016 08:00:00
% Modified: APR 2016 08:00:00
% Revision 0.20
% The main file controlling Models and View
%  MVC: The Controller
% Just enter variables and  hit F5

clear;
clc;
Logon = LogonODBC('googleSQLAsia','root','matlab123');% Default settings, username,pass, etc for google SQL server

%h = waitbar(0,'Please wait...');
%outputApp3 = Commandline();
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Band Index
% Index 1	1.5Ghz Main
% Index 2	1.5Ghz Interleved
% Index 3	1.5Ghz DRCS
% Index 4	1.8Ghz Main
% Index 5	1.8Ghz Interleved
% Index 6	1.8Ghz 7 MHz
% Index 7	3.8Ghz Main
% Index 8	5Ghz Main
% Index 9	6Ghz Main
% Index 10	6Ghz 60 MHz
% Index 11	6.7Ghz Main
% Index 12	6.7Ghz 80 MHz
% Index 13	7.5Ghz Main
% Index 14	7.5Ghz 14 MHz
% Index 15	8Ghz Main
% Index 16	8Ghz 60 MHz
% Index 17	8.3Ghz Main
% Index 18	8.3Ghz Interleved
% Index 19	10Ghz Main
% Index 20	10Ghz 14 MHz
% Index 21	11Ghz Main
% Index 22	11Ghz 80 MHz
% Index 23	13Ghz Main
% Index 24	13Ghz Inter
% Index 25	15Ghz 7 MHz
% Index 26	15Ghz 14 MHz
% Index 27	15Ghz 28 MHz
% Index 28	18Ghz 13.75 MHz
% Index 29	18Ghz 27.5 MHz
% Index 30	18Ghz 55 MHz
% Index 31	18Ghz 55 MHz
% Index 32	22Ghz 7 MHz
% Index 33	22Ghz 14 MHz
% Index 34	22Ghz 28 MHz
% Index 35	22Ghz 50 MHz
% Index 36	22Ghz 56 MHz
% Index 37	28Ghz 28 MHz
% Index 38	28Ghz 56 MHz
% Index 39	28Ghz 112 MHz
% Index 40	38Ghz 7 MHz
% Index 41	38Ghz 14 MHz
% Index 42	38Ghz 28 MHz
% Index 43	49Ghz Main
% Index 44	50Ghz Main


%-----------------> Input VARIABLES

radius = 40; % km's
bandIndex = 21; %remember this is a index so index = 11 doesnt mean 11GHz
latitude1 = -33.791132;
longitude1 = 151.087255;
latitude2 = -33.772771;
longitude2 = 151.081349;

TxPower = 1;
ModelID = 'DEFAULT';  % folders in antenna folder: DEFAULT,89,99,NOTIONAL
TxRx = 1; %Tx/Rx TxHi@A

gain = 1;

polarity = 'V';
sizeAnt = 2.4;

correction = 0; % rain in mm

access = '0000';
height = 5;
emission = 'XXXX';
date = datetime('today');
% Outputs to folder /jobs

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------->SETUP INPUT APPARATUS: App3 & App4
bands = BandsSQL(bandIndex,Logon);

%-----------------> location object
loc_3 = ApparatusLocation(latitude1,longitude1);
loc_4 = ApparatusLocation(latitude2,longitude2);

%-----------------> creates two location Apparatus object and Calc Azimuth
setup1 = SetupLocation(loc_3,loc_4);

%-----------------> signal Apparatus object
signal3_4 = ApparatusSignal(gain,ModelID,bands.tableBands.bandwidth);

%-----------------> transmit Apparatus object
transmit3_4 = ApparatusTransmitType(TxRx);

%-----------------> Bands Apparatus object
bandsApp3_4 = ApparatusBands(bands);

%-----------------> Coordinate object
coordinate3_4 = ApparatusCoordinate(polarity,sizeAnt,TxPower,correction);

%-----------------> Setup 2
setup2 = SetupBandsCoordSigApp(bandsApp3_4,coordinate3_4,signal3_4);

%-----------------> ID Object
IdApp3_4 = ApparatusID(access,height,emission,date);

%-----------------> Input & merge sub-apparatus into main app object
AppC = Apparatus(getSigApp(setup2),getBandApp(setup2),getLoc3(setup1),transmit3_4,IdApp3_4,getCoordApp(setup2));
AppD = Apparatus(getSigApp(setup2),getBandApp(setup2),getLoc4(setup1),transmit3_4,IdApp3_4,getCoordApp(setup2));

%-----------------> DATABASE SETUP
data = Database1(radius,freqLow(bands),freqHigh(bands),AppC.LocationApparatus,AppD.LocationApparatus,Logon);

% Mid location for AppC and AppD
setMidCoord(data);

% Make SQL query and return the relevant data
getRawData(data);

% Sort relevant data so easier to manipulate
sorted = sortRawData(data);

%------------------> raw SQL Protection Ration Table
proRatioTABLE = ProtectionRatioSQL(Logon,theBand(bands));
setProRatioTable(proRatioTABLE);

%%%%%%%%%%%%%%%%%%%%%%%%%%%% EXPERIMENT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
protRat = ProtectionRatio(AppC,AppD,'D2D',sorted,proRatioTABLE,bands,correction);
setProtectionMargins(protRat);
%%%%%%%%%%%%%%%%%%%%%%%%%%%% AVAIL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
available = AvailableChan(protRat.protMarginAr,12);
newArray(available);
maxInColumn(available);

%------------------> Save
saveProtect = saveProtectionMargin(protRat);
createFileName(saveProtect,'test_ProtectionMargin');
createNames(saveProtect);
createTable(saveProtect);
writeTheTable(saveProtect);

distance = Distance(sorted,AppC,AppD);
setAllTables(distance);
setTable(distance);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%------------------> Coordinate
coordinate = Coordinate(AppC,AppD,sorted,bands,correction,proRatioTABLE);
setChannels(coordinate);

%------------------> Link Calc
tic

dumpIndex=1;
for device=2:size(coordinate.sortedData,1);
    %waitbar(device/size(coordinate.sortedData,1));
    if isequal(coordinate.sortedData{device-1,1},coordinate.sortedData{device,1})
        loadApp1App2(coordinate,device,bands);
        setCorrectionRatio(coordinate);        
        dataDump(dumpIndex) = DataDump(); % dataDump.empty()?
        dataDump(dumpIndex).setData(coordinate,bands);
        dumpIndex=dumpIndex+1;
        dumpIndex
    end  
end

toc
%------------------> Save  Dump
saveDump = saveDataDump();
addFileName(saveDump,'Dump');
tableValues(saveDump,dataDump);
objDumpToTable(saveDump);
writeDataDump(saveDump);
