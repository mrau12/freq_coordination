classdef Channel < handle
    %APPARATUSCHANNEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        CHAN_BANDWIDTH;
        CENTRE_FREQ;
        LOWER_FREQ;
        UPPER_FREQ;
        protectionMargin;
        frequencyOffset;
        modulationType;
        id;
    end
    
    methods
        % constructor
        function chan = Channel(mid,chan_bandwidth)
            switch nargin
                case 0
                    chan.UPPER_FREQ = [];
                    chan.CENTRE_FREQ = [];
                    chan.LOWER_FREQ = [];
                    chan.CHAN_BANDWIDTH = [];
                    chan.protectionMargin = [];
                    chan.frequencyOffset = [];
                    chan.modulationType = [];
                    chan.id = [];
                case 2
                    setMidFr(chan,mid);                    
                    setChanBandw(chan,chan_bandwidth);
                    chan.UPPER_FREQ = 0;
                    chan.LOWER_FREQ = 0;
                    chan.protectionMargin = [];
                    chan.frequencyOffset = [];
                    chan.modulationType = []; 
                    chan.id = [];
                otherwise
                    disp('error incorrect parameters for ApparatusChannel object')
            end
        end
        
        function setID(chan,id)
            chan.id = id;
        end
        
        function setUpperFr(chan)
            chan.UPPER_FREQ = chan.CENTRE_FREQ + (chan.CHAN_BANDWIDTH/2);
        end
        
        function setMidFr(chan,mid)
            chan.CENTRE_FREQ = mid;
        end
        
        function setLowerFr(chan)
            chan.LOWER_FREQ = chan.CENTRE_FREQ - (chan.CHAN_BANDWIDTH/2);
        end
        
        function setChanBandw(chan,bandwidth)
            chan.CHAN_BANDWIDTH = bandwidth;
        end
        
        function setProtectionMargin(chan,margin)
            chan.protectionMargin = margin;
        end
        
        function setDifference(chan,frequencyOffset)
            chan.frequencyOffset = frequencyOffset;
        end
        
         function setModulationType(chan,type)
            chan.modulationType = type;
        end
    end
end

