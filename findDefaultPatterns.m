function findDefaultPatterns(stringy)
fileNames = getAllFiles('C:\Users\mike\Dropbox\SAT\Matlab\4. Licencing\4_4_2016\stdAntennas');
SuccessfulFinds = 0;
 for fileNum = 1:size(fileNames,1)
     A = fileread(char(fileNames(fileNum,1)));
     B = strfind(A, stringy);
     
    if(B)
        disp('Filename: ')
        char(fileNames(fileNum,1))
        disp('Where is the string in file: ') % Char position
        B
        SuccessfulFinds = SuccessfulFinds + 1;
    end    
 end
 SuccessfulFinds
end

function fileList = getAllFiles(dirName)
dirData = dir(dirName);      %# Get the data for the current directory
dirIndex = [dirData.isdir];  %# Find the index for directories
fileList = {dirData(~dirIndex).name}';  %'# Get a list of the files
if ~isempty(fileList)
    fileList = cellfun(@(x) fullfile(dirName,x),...  %# Prepend path to files
        fileList,'UniformOutput',false);
end
subDirs = {dirData(dirIndex).name};  %# Get a list of the subdirectories
validIndex = ~ismember(subDirs,{'.','..'});  %# Find index of subdirectories
%#   that are not '.' or '..'
for iDir = find(validIndex)                  %# Loop over valid subdirectories
    nextDir = fullfile(dirName,subDirs{iDir});    %# Get the subdirectory path
    fileList = [fileList; getAllFiles(nextDir)];  %# Recursively call getAllFiles
end
end

