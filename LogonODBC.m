classdef LogonODBC < handle
    % Password & Username for database for ODBC connector for mysql server
    
    properties
        dbsource;
        username;
        password;
        message;
    end
    
    methods
        function Logon = LogonODBC(dbsource,username,password)
            switch nargin
                case 3
                    Logon.username = username;
                    Logon.password = password;
                    Logon.dbsource = dbsource;
                    Logon.message = [];
                    disp(['Datasource name: ',Logon.dbsource])
                    disp(['User: ',Logon.username])
                    disp('Pass: ******* ')
                    
                otherwise
                    Logon.username = username;
                    Logon.password = password;
                    Logon.dbsource = 'googleSQLAsia';
                    Logon.message = [];
                    disp(['Datasource name: ',Logon.dbsource])
                    disp(['User: ',Logon.username])
                    disp('Pass: ******* ')
            end
        end
        
        function user = getUser(Logon)
            user = char(Logon.username);
        end
        
        function pass = getPass(Logon)
            pass = char(Logon.password);
        end
        
        function tf = isConnected(Logon)
            conn = database('googleSQLAsia',Logon.username,Logon.password); %matlab>2014
            if ~isempty(conn.Message)
                fprintf(1, 'Successful log on\n');
                fprintf(2,'%s\n', conn.Message);
                Logon.message = conn.Message;
                tf = 1;
            else
                fprintf(1, 'Unsuccessful, Retry or Escape\n');
                fprintf(2,'%s\n', conn.Message);
                Logon.message = conn.Message;
                tf = 0;
            end
        end
        
        function tf = logonGranted(Logon)
            if ~(strfind(Logon.message, 'denied'))
                fprintf(1, 'Access granted\n');
                tf = 1;
            else
                fprintf(1, 'Access denied\n');
                tf = 0;
            end
            
        end
    end
end


