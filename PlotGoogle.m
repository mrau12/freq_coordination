classdef PlotGoogle < handle
    %PLOTGOOGLE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        sorted;
        limit;
        lat3;
        long3;
        long4;
        lat4;
        midLongitude;
        midLatitude;
        figs;
    end
    
    methods
        function pGoogle = PlotGoogle(sorted,limit,lat3,long3,long4,lat4)
            pGoogle.sorted = sorted;
            pGoogle.limit = limit;% axes(limit)
            pGoogle.lat3 = lat3;
            pGoogle.long3 = long3;
            pGoogle.long4 = long4;
            pGoogle.lat4 = lat4;
            pGoogle.midLongitude = (long3 + long4)/2;
            pGoogle.midLatitude = (lat3 + lat4)/2;
            pGoogle.figs = [];
        end
        
        function plotArea(pGoogle)        
           
            if length(pGoogle.sorted)>1
                axis(pGoogle.limit);
                xlim([pGoogle.midLatitude-2,pGoogle.midLongitude+2]); 
                axis equal off;
                length(pGoogle.sorted);
                pGoogle.figs = scatter([pGoogle.long4;pGoogle.long3],[pGoogle.lat4;pGoogle.lat3],'kp','LineWidth',5);
                hold on
                scatter([pGoogle.sorted{:,11}],[pGoogle.sorted{:,10}]);
                hold off
                plot_google_map;
                hold off
            else
                axis(pGoogle.limit);                
                pGoogle.figs = scatter([pGoogle.lat4;pGoogle.long3],[pGoogle.lat4;pGoogle.long3],'kp','LineWidth',5);
                plot_google_map;
            end            
        end
        
        function figs = getFigs(pGoogle)            
            figs = pGoogle.figs;
        end            
    end    
end

