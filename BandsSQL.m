classdef BandsSQL < handle
    %BANDSSQL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties        
        tableBands;        
    end
    
    methods
         % constructor
        function Bands = BandsSQL(bandIndex,Logon)           
            setBands(Bands,bandIndex,Logon);
        end       
        
        %% Band index from GUI
        % then finds it in sql database
        % then sets property bands in Database class
        % Input: band index
        % Output: sets class property bands
        function [] = setBands(Bands,bandIndex,Logon)           
            conn = database(Logon.dbsource,Logon.username,Logon.password); %matlab>2014
            if ~isempty(conn.Message)
                fprintf(2,'%s\n', conn.Message);
            else
                fprintf(1, 'Querying database for bands table: band,type,freqLow,freqHigh,et cetera... \n');
            end
            SQL_command = ['SELECT * FROM  acma_new.bands WHERE indexb=',num2str(bandIndex)];% "1.5GHz main" is index 1. "1.5GHz interleaved" is index 2. ect...
            curs = exec(conn,SQL_command); %defines a command
            SLQreturn=fetch(curs); %executes command
            close(conn)
            band = SLQreturn.Data;
            names = {'band','Type','fo','fL','fh','channels','bandwidth','TxRx_gap','Blackouts','channel_start','path_min','restrictions','page'};
            Bands.tableBands = cell2table(band(2:end),'VariableNames',names);
        end
        % Get bands table
        function Bs = getBands(Bands)
            Bs = Bands.tableBands;
        end  
        % Get low 
        function [freq1] = freqLow(Bands)
             freq1 = Bands.tableBands.fL;            
        end
        % Get high
        function [freq2] = freqHigh(Bands)
            freq2 = Bands.tableBands.fh;
        end
        
        function [theBand] = theBand(Bands)
            theBand = Bands.tableBands.band{1};
        end        
    end
end

