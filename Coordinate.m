% Michael McClellan
% SAT PTY LTD
% Created: OCT 2015
% Modified: APR 2016 08:00:00
% Revision 0.20
% Class Summary goes here
%  MVC: Model
% Examples:
%
% Provide sample usage code here
%
% See also: List related files here
classdef Coordinate < handle
    % write a description of the class here.
    properties
        link1;
        link2;
        link3;
        link4;
        
        % 1*******2
        %  *
        %    *
        %      *
        %        4
        % Angle between link1 and link2 aka at app1
        angle1;
        
        % 1*******2
        %       *
        %     *
        %   *
        % 3
        % Angle between link1 and link3 aka at app2
        angle2;
        
        %         2
        %       *
        %     *
        %   *
        % 3*******4
        % Angle between link4 and link3 aka at app3
        angle3;
        
        % 1
        %   *
        %     *
        %       *
        % 3*******4
        % Angle between link4 and link2 aka at app4
        angle4;
        
        current_LinkID;
        sortedData;
        bands;
        theBand;
        correction;
        Pro_D;
        Pro_B;
    end
    
    methods
        % CONSTRUCTOR
        function coordinate = Coordinate(App3,App4,sortedData,Bands,correction)
            
            App1 = Apparatus(); %empty App needs to be filled from sorted data
            App2 = Apparatus(); %empty App needs to be filled from sorted data
            coordinate.link1 = Link(App1,App2);  %empty
            coordinate.link2 = Link(App1,App4);  %half
            coordinate.link3 = Link(App3,App2);  %half
            coordinate.link4 = Link(App3,App4);  %full
            coordinate.current_LinkID = 0;
            coordinate.sortedData = sortedData;
            coordinate.bands = Bands;
            coordinate.correction = correction;
            coordinate.theBand  = Bands.tableBands.band;
            coordinate.Pro_D = 0;
            coordinate.Pro_B = 0;
            
        end
        
        %%% METHODS %%%
        
        % if comparedIDLink true, loads App2 (i.e siteB)  into links (e.g.
        % link1 and link3) and  loads App1 (i.e siteA)  into links (e.g.
        % link1 and link4)
        function loadApp1App2(coordinate,device,Bands)
            loadAppIntoLinks(coordinate,device-1,'app1',Bands);
            loadAppIntoLinks(coordinate,device,'app2',Bands);
        end
        
        % Loads an App into Two links
        % i.e. loads App1: link1 = Link(App1,App2) and link2 =
        % Link(App1,App4) , so App1 comes from a row in SortedData. App1 is
        % loaded into link1 and link2. App2 is empty (loaded later). App4
        % was inputed by user.
        function [] = loadAppIntoLinks(coordinate,device,whichApparatus,Bands) %App exists in two links
            switch whichApparatus
                case 'app1'
                    loadApparatus(coordinate.link1.app1,coordinate.sortedData,device,Bands);
                    loadApparatus(coordinate.link2.app1,coordinate.sortedData,device,Bands);
                case 'app2'
                    loadApparatus(coordinate.link1.app2,coordinate.sortedData,device,Bands);
                    loadApparatus(coordinate.link3.app2,coordinate.sortedData,device,Bands);
                case 'app3'
                    loadApparatus(coordinate.link3.app3,coordinate.sortedData,device,Bands);
                    loadApparatus(coordinate.link4.app3,coordinate.sortedData,device,Bands);
                case 'app4'
                    loadApparatus(coordinate.link2.app4,coordinate.sortedData,device,Bands);
                    loadApparatus(coordinate.link4.app4,coordinate.sortedData,device,Bands);
                otherwise
                    disp('Either incorrect Apparatus in parameter, link not initialised or no sorted data')
            end
        end
        
        % device (i.e row from sortedData) is equivalent of object
        % Apparatus but in array form.
        function [tf] = compareCurrentToNextID(coordinate,device)
            tf = isequal(coordinate.sortedData{device-1,1},coordinate.sortedData{device,1});
        end
        
        %% Link budgets between Apparatus Wanted signals: C->D, A->B, Unwanted: C->B, A->D,
        function [antBudget1,antBudget2,antBudget3,antBudget4] = antBudget(coordinate)
            setAnglesBetweenLinks(coordinate);
            antBudget1 = antennaBudget(coordinate.link1,0,0); %link1(app1,app2) A2B_link_budget
            antBudget2 = antennaBudget(coordinate.link2,coordinate.angle1,coordinate.angle4); %link2(app1,app4) A2D_link_budget
            antBudget3 = antennaBudget(coordinate.link3,coordinate.angle3,coordinate.angle2); %link3(app3,app2) C2B_link_budget
            antBudget4 = antennaBudget(coordinate.link4,0,0); %link4(app3,app4) C2D_link_budget
        end
        
        %% Link angles between Apparatus
        function setAnglesBetweenLinks(coordinate)
            coordinate.angle1 = angleBetweenLinks(coordinate.link1,coordinate.link2);
            coordinate.angle2 = angleBetweenLinks(coordinate.link1,coordinate.link3);
            coordinate.angle3 = angleBetweenLinks(coordinate.link4,coordinate.link2);
            coordinate.angle4 = angleBetweenLinks(coordinate.link4,coordinate.link3);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% EXPERIMENT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function runSpectrum(coordinate, mod2mod,proRatioTABLE,bands)
           % Create all victims, that are going to be interfered by AppC
           % and AppD
            victims = Victims(coordinate.sortedData);
            setVictims(victims);
            
            protMarginAr = [];
            for index = 1:victimListLength(victims)
                spectrum(index) = Spectrum(bands,proRatioTABLE,victims.vics(1,index),mod2mod,victims.vics(1,index).id);
                lookUpProtectionRatioRules(spectrum(index));
                setProtMarginTable(spectrum(index));
                setTable(spectrum(index));
                protMarginAr = horzcat(protMarginAr,spectrum(1, index).arrayProtMargin');
            end
        end
        %% Correct Factor %First calc A2D_pl & C2B_pl
        function setCorrectionRatio(coordinate,spectrum)
            distA_B =  DistanceTxRx(coordinate.link1);
            distC_D =  DistanceTxRx(coordinate.link4);
            [coordinate.pro_D] = lookUpProtectionRatio(spectrum) + correctionFactor(coordinate,distA_B);
            [coordinate.pro_B] = lookUpProtectionRatio(spectrum) + correctionFactor(coordinate,distC_D);
        end
        
        
        %% Finds protection factor offset value (refer to acma fx3 graph PROTECTION RATIO CORRECTION FACTORS MULITPATH)
        % Inputs:
        % Path length between sites in (km)
        % Rain (mm) (which line of three or more on graph in acma fx3)
        % Output:
        % Correction factor(dB)
        function [protect_corr] = correctionFactor(coordinate,path_length)
            
            path_length=path_length/1000;%convert meters to km
            if coordinate.correction<0 %rain rate
                %load rain rate data
                rain_rate=-coordinate.correction;
                if coordinate.theband>=15
                    if coordinate.theband>=18
                        if coordinate.theband>=22
                            if coordinate.theband>=38 % 38 GHz band
                                a=-0.24;b=9.45;c=-33.5;
                                CF_40=a*path_length^2+b*path_length+c;
                                a=-0.75;b=15.24;c=-35;
                                CF_60=a*path_length^2+b*path_length+c;
                                a=-0.9874;b=18.793;c=-33.754;
                                CF_80=a*path_length^2+b*path_length+c;
                                protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
                                if protect_corr<-25.5,protect_corr=-25.5;end
                            else % 22 GHz band
                                a=-0.0815;b=3.9605;c=-34.584;
                                CF_40=a*path_length^2+b*path_length+c;
                                a=-0.1266;b=5.4266;c=-31.49;
                                CF_60=a*path_length^2+b*path_length+c;
                                a=-0.2017;b=7.0796;c=-30.828;
                                CF_80=a*path_length^2+b*path_length+c;
                                protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
                                if protect_corr<-23.5,protect_corr=-23.5;end
                            end
                        else % 18 GHz band
                            a=16.432;b=54.212;c=0;
                            CF_40 = a*log(path_length)-b;
                            a=19.768;b=52.135;c=0;
                            CF_60= a*log(path_length)-b;
                            a=20.439;b=47.256;c=0;
                            CF_80= a*log(path_length)-b;
                            protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
                            if protect_corr<-30,protect_corr=-30;end
                        end
                    else % 15 GHz band
                        pl_save=path_length;
                        if path_length<5,path_length=5;end
                        a=11.166;b=47.827;c=0;
                        CF_40 = a*log(path_length)-b;
                        a=13.833;b=47.227;c=0;
                        CF_60= a*log(path_length)-b;
                        a=13.097;b=39.813;c=0;
                        CF_80= a*log(path_length)-b;
                        protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
                        if pl_save<5, protect_corr=pl_save*((protect_corr-(-40))/(5-0))+(-40);end %linear interpolation between 5 & 0 km.
                    end
                else
                    protect_corr=0;
                    display('WARNING - rain rate CF undefined for band ',num2str(coordinate.theband),'GHz');
                end
            else %path length
                %path_lenth_corr=dump.correction;
                %bands.band
                if str2double(coordinate.theBand)<=5
                    protect_corr_5 = 15.798*log(path_length)-73.5;
                    protect_corr_10= 15.664*log(path_length)-68.725;
                    protect_corr_20= 15.174*log(path_length)-62.23;
                    protect_corr=interp_PLCF(coordinate,protect_corr_5,protect_corr_10,protect_corr_20);
                    if protect_corr<-26.5,protect_corr=-26.5;end %PLCF floor value
                    if path_length>110, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
                elseif str2double(coordinate.theBand)<=8
                    protect_corr_5 = 15.633*log(path_length)-70.232;
                    protect_corr_10= 15.924*log(path_length)-66.753;
                    protect_corr_20= 15.151*log(path_length)-59.297;
                    protect_corr=interp_PLCF(coordinate,protect_corr_5,protect_corr_10,protect_corr_20);
                    if protect_corr<-34,protect_corr=-34;end %PLCF floor value
                    if path_length>100, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
                elseif str2double(coordinate.theBand)<=11
                    protect_corr_5 = 15.56*log(path_length)-61.962;
                    protect_corr_10= 15.523*log(path_length)-57.451;
                    protect_corr_20= 15.83*log(path_length)-53.951;
                    protect_corr=interp_PLCF(coordinate,protect_corr_5,protect_corr_10,protect_corr_20);
                    if protect_corr<-26,protect_corr=-26;end %PLCF floor value
                    if path_length>70, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
                elseif str2double(coordinate.theBand)<=13
                    protect_corr_5 = 15.837*log(path_length)-57.755;
                    protect_corr_10= 15.779*log(path_length)-53.901;
                    protect_corr_20= 15.79*log(path_length)-48.684;
                    protect_corr=interp_PLCF(coordinate,protect_corr_5,protect_corr_10,protect_corr_20);
                    if protect_corr<-20,protect_corr=-20;end %PLCF floor value
                    if path_length>70, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
                else
                    protect_corr=0;
                end
            end
        end
        
        function [protect_corr]=interp_PLCF(coordinate,protect_corr_5,protect_corr_10,protect_corr_20)
            if coordinate.correction<=5
                protect_corr=protect_corr_5;
            elseif coordinate.correction<=10
                protect_corr =((coordinate.correction-5)*(-protect_corr_10+protect_corr_5)/(10-5)-protect_corr_5);
            elseif coordinate.correction<=20
                protect_corr =((coordinate.correction-10)*(-protect_corr_20+protect_corr_10)/(20-10)-protect_corr_10);
            else
                protect_corr=protect_corr_20;
                display('Warning Path Length correction factor undefined for PL value')
            end
            
        end
        
        function [protect_corr]=interp_RRCF(coordinate,protect_corr_5,protect_corr_10,protect_corr_20)
            if coordinate.correction<=5
                protect_corr=protect_corr_5;
            elseif coordinate.correction<=10
                protect_corr =((coordinate.correction-5)*(-protect_corr_10+protect_corr_5)/(10-5)-protect_corr_5);
            elseif coordinate.correction<=20
                protect_corr =((coordinate.correction-10)*(-protect_corr_20+protect_corr_10)/(20-10)-protect_corr_10);
            else
                protect_corr=protect_corr_20;
                display('Warning Path Length correction factor undefined for PL value')
            end
        end
        
    end
end
