classdef SetupLocation
    %USED for input Apparatus App3(siteC) and App4 (siteD)
    % Sets up to Location apparatus
    % Calculates Azimuth (ANT_AZ), needs two sets of App Location
    % to do calculation
    
    properties (Access = private)
        loc_3;
        loc_4;
    end
    
    methods
        function calcLoc = SetupLocation(loc3,loc4)  
            
            calcLoc.loc_3 = loc3;
            calcLoc.loc_4 = loc4;
            
            %-----------------> Calc Azimuth
            [alpha,beta] = calcAlpha(calcLoc);  
            
            %-----------------> App3 azimuth is Alpha
            setAz(calcLoc.loc_3,alpha)
            
            %-----------------> App4 azimuth is Beta
            setAz(calcLoc.loc_4,beta)
        end
        
        % Intermediate methods that changes some data inside App3 & App4,
        % App3 & App4 with changed data passed into Coordinate()
        % sortedData -----changes(az_ant, boreGain)---> Apparatusn()
        % existing link's boresite angle
        function [alpha,beta] = calcAlpha(calcLoc)
            alpha = atan2( (getLong(calcLoc.loc_3)-getLong(calcLoc.loc_4)),(getLat(calcLoc.loc_3)-getLat(calcLoc.loc_4)) );
            beta = alpha + pi;
        end
        
        function threeL = getLoc3(calcLoc)
           threeL = calcLoc.loc_3;
        end
        
        function fourL =  getLoc4(calcLoc)
           fourL = calcLoc.loc_4;
        end
    end    
end

