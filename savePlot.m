classdef savePlot
    %SAVEPLOT Summary of this class goes here
    %   Detailed explanation goes here
    properties
        theFilename;
        figs;
    end
    
    methods
        function saveP = savePlot(figs,name)
            saveP.theFilename = name;
            saveP.figs = figs;
        end
        
        %% createFolder
        function createFolder(saveP)
            if ~exist('Jobs/googlePlot','dir')
                mkdir('Jobs/googlePlot');
            end       
            
        end
        
        function  saveFile(saveP)
            saveas(saveP.figs,['Jobs/googlePlot/',saveP.theFilename,'.jpeg']);
            % saveP.figs;
            % imageData = export_fig -jpg;           
           % movefile(imageData,'Jobs/googlePlot');
        end
        
        %% Open saved file
        function openFolder(saveP)
            %set(0,'DefaultFigurePaperPositionMode','auto')
            winopen(['Jobs/googlePlot/',saveP.theFilename,'.jpeg']);
        end
    end
end

