classdef Apparatus < handle % just for site C and D???
    % write a description of the class here.
    properties (Constant)
        light_speed=3*10^8;
        
    end
    properties
        defaultBandArray;
        fileList;
        nameList;
        theGain;
        theAngle;
        
        % Sub-objects
        % Signal sub-properties:
        %         ACCESS_ID;
        %         HEIGHT;
        %         emission;
        %         date;
        IDApparatus;
        
        % Trans sub-properties:
        %               Mode1;
        %               Mode2;
        %               --->
        %               Tx/Rx TxHi@A
        %               Tx/Rx TxHi@B
        %               Tx @ A
        %               Tx @ B
        TranTypeApp;
        
        
        
        % Signal sub-properties:        
        %               ANT_GAIN;
        %               ANT_MODEL;
        %               BANDWIDTH; bandwidth for each app channel        
        SignalApparatus;
        
        
        % Bands sub-properties:
        %               ANT_ID;  bands from database class
        %               ANT_SIZE;
        %               frequency1; freq1 from database class
        %               The Band frequency        
        BandsApparatus;
        
        % Location Sub-properties:
        %               latitude;
        %               longitude;
        %               SITE_ID;        
        LocationApparatus;
        
        %         pol; % Polarity
        %         ANT_SIZE; % Anntena
        %         TX_POWER; % Power
        %         correction; %Rain (mm)
        CoordinateApparatus;
       
       
    end
    
    methods
        % constructor
        function App = Apparatus(sigApp,bandsApp,locApp,tranTypApp,IDApp,coApp)
            switch nargin
                case 0
                    %empty apparattus
                    App.theGain = [] ;
                    App.theAngle = [] ;
                    App.defaultBandArray = [];
                    
                    App.IDApparatus = ApparatusID();
                    App.SignalApparatus = ApparatusSignal();
                    App.BandsApparatus = ApparatusBands();
                    App.LocationApparatus = ApparatusLocation();
                    App.TranTypeApp = ApparatusTransmitType();
                    App.CoordinateApparatus = ApparatusCoordinate();
                    
                case 3
                    App.theGain = [] ;
                    App.theAngle = [] ;
                    App.defaultBandArray = [];
                    
                    App.SignalApparatus = sigApp;
                    App.BandsApparatus = bandsApp;
                    App.LocationApparatus = locApp;
                    App.TranTypeApp = ApparatusTransmitType();
                    App.IDApparatus = ApparatusID('0000',5,'XXXX',datetime('today'));
                    App.CoordinateApparatus = ApparatusCoordinate();
                   
                case 4
                    App.theGain = [];
                    App.theAngle = [];
                    App.defaultBandArray = [];
                    
                    App.SignalApparatus = sigApp;
                    App.BandsApparatus = bandsApp;
                    App.LocationApparatus = locApp;
                    App.TranTypeApp = tranTypApp; %% Need function that can pass info between sub-objects
                    App.IDApparatus = ApparatusID('0000',5,'XXXX',datetime('today'));
                    App.CoordinateApparatus = ApparatusCoordinate();
                 
                case 6 %IF ALL APPs inputted then setBoreGain()
                    App.theGain = [] ;
                    App.theAngle = [] ;
                    App.defaultBandArray = [];
                    
                    App.SignalApparatus = sigApp;
                    App.BandsApparatus = bandsApp;
                    App.LocationApparatus = locApp;
                    App.TranTypeApp = tranTypApp;
                    App.IDApparatus = IDApp;
                    App.CoordinateApparatus = coApp;
                   
                otherwise
                    disp('error incorrect parameters for Apparatus object')
            end
            %Arr = [3.9,6.1,6.2,6.8,7.4,7.8,8,8.3,10,11,13,18,22];
            setDefaultBandArray(App); % Looks at default folder checks for new default patterns
        end
        
        % METHODS
        
        function [] = find_antenna_pattern(App)
            %% antenna
            removeIllegChar(App);
            [antenna,offset] = lookupAntDatabase(App);
            %% Convert pattern to radians
            App.theAngle=0;
            App.theGain=0;
            trigger=0;
            for i=1:size(antenna{1},1)
                if str2double(antenna{1}{i})==-180
                    trigger=1;
                    offset=i-1;
                end
                if trigger
                    App.theAngle(i-offset)=str2double(antenna{1}{i});
                    App.theGain(i-offset)=str2double(antenna{2}{i});
                    if isnan(App.theAngle(i-offset))
                        display(['skipped line. Antenna file ',App.SignalApparatus.ANT_MODEL,' need work.'])
                        App.theAngle(i-offset)=[];
                        App.theGain(i-offset)=[];
                        offset=offset-1;
                    end
                    if isequal(App.theAngle(i-offset),180)
                        App.theAngle=App.theAngle*2*pi/360;
                        break
                    end
                end
            end
           
        end
        
        function setDefaultBandArray(App,Arr)
            
            switch nargin
                case 2
                    App.defaultBandArray = sort(Arr);
                case 1
                    % App.defaultBandArray = [3.9,6.1,6.2,6.8,7.4,7.8,8.3,8,10,11,13,18,22];
                    getDefaultBandFileNames(App,'Antennas/DEFAULT');
                    fileListFrontName(App);
                    bandsInDefaultFolder(App);
                otherwise
                    disp('Incorrect input parameters for setDefaultBandArray in Apparatus.m')
            end
        end
        
        function bandsInDefaultFolder(App)
            for index = 1:length(App.nameList);
                bandsInFolder(index) = str2double(regexp(App.nameList{1,index}, '^.*(?=(\G))', 'match'));
            end
            App.defaultBandArray = unique(bandsInFolder);
        end
        
        function fileListFrontName(App)
            for index = 1:size(App.fileList);
                [~,name,~] = fileparts(App.fileList{index});
                App.nameList{end+1} = name;
            end
        end
        
        function  getDefaultBandFileNames(App,dirName)
            dirData = dir(dirName);      %# Get the data for the current directory
            dirIndex = [dirData.isdir];  %# Find the index for directories
            App.fileList = {dirData(~dirIndex).name}';  %'# Get a list of the files
            if ~isempty(App.fileList)
                App.fileList = cellfun(@(x) fullfile(dirName,x),...  %# Prepend path to files
                    App.fileList,'UniformOutput',false);
            end
            subDirs = {dirData(dirIndex).name};  %# Get a list of the subdirectories
            validIndex = ~ismember(subDirs,{'.','..'});  %# Find index of subdirectories
            %#   that are not '.' or '..'
            for iDir = find(validIndex)                  %# Loop over valid subdirectories
                nextDir = fullfile(dirName,subDirs{iDir});    %# Get the subdirectory path
                App.fileList = [App.fileList; getAllFiles(nextDir)];  %# Recursively call getAllFiles
            end
        end
        
        %% Remove illegal characters for antenna name
        function [] = removeIllegChar(App)
            
            App.SignalApparatus.ANT_MODEL={strrep(char(App.SignalApparatus.ANT_MODEL),'.','')};% format file name for database lookup
            App.SignalApparatus.ANT_MODEL={strrep(char(App.SignalApparatus.ANT_MODEL),' ','')};
            App.SignalApparatus.ANT_MODEL={strrep(char(App.SignalApparatus.ANT_MODEL),')','')};
            App.SignalApparatus.ANT_MODEL={strrep(char(App.SignalApparatus.ANT_MODEL),'(','')};
            App.SignalApparatus.ANT_MODEL={strrep(char(App.SignalApparatus.ANT_MODEL),'-','')};
            App.SignalApparatus.ANT_MODEL={strrep(char(App.SignalApparatus.ANT_MODEL),'/','')};
        end
        
        %% Look through each antenna database folder and look for filename, from file load antenna pattern
        % Outputs: 1. antenna array of gain and angles 2. s is the offset
        % inside txt file where first angle and gain starts
        function [antenna,s] = lookupAntDatabase(App)
            disp('Look up App in .dat database');
            App.SignalApparatus.ANT_MODEL;
            if isequal(char(App.SignalApparatus.ANT_MODEL),'NOTIONAL')
                disp('notional opened')
                file_name='Antennas/89/PL877GE.DAT';% default antenna
                s=10; % first data point offset
                antenna_data = fopen(file_name, 'r');% load antenna data
                antenna = textscan(antenna_data, '%s%s');% import mixed data types as strings (%s) 2 columbs
                fclose(antenna_data);
            elseif exist(['Antennas/89/',char(App.SignalApparatus.ANT_MODEL),'.DAT'],'file')>0
                file_name=['Antennas/89/',char(App.SignalApparatus.ANT_MODEL),'.DAT'];
                s=10; % first data point offset
                antenna_data = fopen(file_name, 'r');% load antenna data
                antenna = textscan(antenna_data, '%s%s');% import mixed data types as strings (%s) 2 columbs
                fclose(antenna_data);
            elseif exist(['Antennas/89/',char(App.SignalApparatus.ANT_MODEL),'.adf'],'file')>0
                file_name=['Antennas/89/',char(App.SignalApparatus.ANT_MODEL),'.adf'];
                s=10; % first data point offset
                antenna_data = fopen(file_name, 'r');% load antenna data
                antenna = textscan(antenna_data, '%s%s');% import mixed data types as strings (%s) 2 columbs
                fclose(antenna_data);
            elseif exist(['Antennas/89/',char(App.SignalApparatus.ANT_MODEL),'.txt'],'file')>0
                file_name=['Antennas/89/',char(App.SignalApparatus.ANT_MODEL),'.txt'];
                s=3; % first data point offset
                antenna_data = fopen(file_name, 'r');% open file for reading - antenna data
                antenna = textscan(antenna_data, '%s%s', 'delimiter',',');% import mixed data types as strings (%s) 2 columbs
                fclose(antenna_data);
            elseif exist(['Antennas/89/',char(App.SignalApparatus.ANT_MODEL),'.bla'],'file')>0
                file_name=['Antennas/89/',char(App.SignalApparatus.ANT_MODEL),'.bla'];
                s=12; % first data point offset
                antenna_data = fopen(file_name, 'r');% load antenna data
                antenna = textscan(antenna_data, '%s%s');% import mixed data types as strings (%s) 2 columbs
                fclose(antenna_data);
            elseif exist(['Antennas/99/',char(App.SignalApparatus.ANT_MODEL),'.adf'],'file')>0
                file_name=['Antennas/99/',char(App.SignalApparatus.ANT_MODEL),'.adf'];
                s=30; % first data point offset
                antenna_data = fopen(file_name, 'r');% load antenna data
                antenna = textscan(antenna_data, '%s%s', 'delimiter',',');% import mixed data types as strings (%s) 2 columbs
                fclose(antenna_data);
            elseif exist(['Antennas/99/',char(App.SignalApparatus.ANT_MODEL),'.txt'],'file')>0
                file_name=['Antennas/99/',char(App.SignalApparatus.ANT_MODEL),'.txt'];
                s=31; % first data point offset
                antenna_data = fopen(file_name, 'r');% load antenna data
                antenna = textscan(antenna_data, '%s%s', 'delimiter',',');% import mixed data types as strings (%s) 2 columbs
                fclose(antenna_data);
            elseif exist(['Antennas/99/',char(App.SignalApparatus.ANT_MODEL),'.dat'],'file')>0
                file_name=['Antennas/99/',char(App.SignalApparatus.ANT_MODEL),'.dat'];
                s=25; % first data point offset
                antenna_data = fopen(file_name, 'r');% load antenna data
                antenna = textscan(antenna_data, '%s%s', 'delimiter',',');% import mixed data types as strings (%s) 2 columbs
                fclose(antenna_data);
                
            elseif exist(['Antennas/DEFAULT/',char(App.BandsApparatus.theBand),'G',num2str(App.CoordinateApparatus.ANT_SIZE),'.adf'],'file')>0
                file_name=['Antennas/DEFAULT/',char(App.BandsApparatus.theBand),'G',num2str(App.CoordinateApparatus.ANT_SIZE),'.adf'];
                s=30; % first data point offset
                antenna_data = fopen(file_name, 'r');% load antenna data
                antenna = textscan(antenna_data, '%s%s', 'delimiter',',');% import mixed data types as strings (%s) 2 columbs
                fclose(antenna_data);
            else
                logUnsuccessfulSearch(App);
                % Exhausted search for antenna model in other folders, now
                % going to using default model for band and ant size.
                %
                % If cannot find model at the current band, try the next
                % band in default folder. e.g. If no 8G0.3 then look at
                % 8.3G0.3
                currentDefaultBand = str2double(App.BandsApparatus.theBand);
                nextBandUp = nextDefaultBand(App,currentDefaultBand); % returns 0 if there is no default .asf for larger band
                
                if nextBandUp ~= 0 || exist(['Antennas/DEFAULT/',char(nextBandUp),'G',num2str(App.CoordinateApparatus.ANT_SIZE),'.adf'],'file')>0
                    
                    disp((['NO Antennas/DEFAULT/',char(App.BandsApparatus.theBand),'G',num2str(App.CoordinateApparatus.ANT_SIZE),'.adf']))
                    disp((['CHECKING INSTEAD Antennas/DEFAULT/',num2str(nextBandUp),'G',num2str(App.CoordinateApparatus.ANT_SIZE),'.adf']))
                    if exist( ['Antennas/DEFAULT/',num2str(nextBandUp),'G',num2str(App.CoordinateApparatus.ANT_SIZE),'.adf'] ,'file')>0
                        file_name=['Antennas/DEFAULT/',num2str(nextBandUp),'G',num2str(App.CoordinateApparatus.ANT_SIZE),'.adf'];
                        s=30; % first data point offset
                        antenna_data = fopen(file_name, 'r');% load antenna data
                        antenna = textscan(antenna_data, '%s%s', 'delimiter',',');% import mixed data types as strings (%s) 2 columbs
                        fclose(antenna_data);
                    else
                        disp((['NO Antennas/DEFAULT/',num2str(nextBandUp),'G',num2str(App.CoordinateApparatus.ANT_SIZE),'.adf']))
                    end
                else
                    display(['Antennas/DEFAULT/',char(App.BandsApparatus.theBand),'G',num2str(App.CoordinateApparatus.ANT_SIZE),'.adf', ' antenna data does not exist. Please add to database. ']);
                    display([char(App.BandsApparatus.ANT_ID),' antenna data does not exist. Please add to database. ']) % show antenna type
                    display(['neither does .asf file for next highest band higher than ',char(App.BandsApparatus.theBand)])
                    logUnsuccessfulSearch(App);
                end
            end
        end
        
        function logUnsuccessfulSearch(App)
            fileID = fopen('NoAntennaPatternLog.txt','at');
            errorStr = strcat(' Model: ',char(App.SignalApparatus.ANT_MODEL),' Band: ', char(App.BandsApparatus.theBand), ' Size: ',num2str(App.CoordinateApparatus.ANT_SIZE));
            fprintf(fileID,errorStr);
            fprintf(fileID,'\n');
            fclose(fileID);
        end
        
        % eliminates any repetition, sorts elements, turns into vector
        % (for interp1)
        function [] = uniqueSort(App)
            [~,AI,~] = unique(App.theAngle);
            App.theGain = App.theGain(AI);
            App.theAngle = App.theAngle(AI);
        end
        
        function [] = removeNulls(App)
            % So a(isnan(a)) = [] would replace nan's with []'s
            App.theGain(isnan(App.theAngle(:))) = [];%  remove null elements
            App.theAngle(isnan(App.theAngle(:))) = [];% remove null elements
            App.theAngle(isnan(App.theGain(:))) = [];% remove null elements
            App.theGain(isnan(App.theGain(:))) = [];%  remove null elements
            App.theGain(any(diff(App.theAngle,1,2),2)) = [];%  remove repeated angle elements
            App.theAngle(any(diff(App.theAngle,1,2),2)) = [];%  remove repeated angle elements
        end
        
        % So the bore site from the file says the gain is 0 at angle 0.
        % We know this isn�t true. What they do is normalise the antenna patterns to 0.
        % So to get the real gain we need to multiply the gain by the boresite gain
        % (or if it�s in dB {which in this case we are} just add them).
        function [] = findBoreGain(App)
            if App.theGain(find(App.theAngle==0))==0; % Find bore gain. if zero add bore gain
                App.BandsApparatus.boreGain=App.SignalApparatus.ANT_GAIN;
                paddingMatrix(App);
                App.theGain=App.theGain+App.BandsApparatus.boreGain;
            end
        end
        
        function [ant1_gain] = interpolation(App,patternAngle)
            try ant1_gain = interp1(App.theAngle,App.theGain,patternAngle);
                ant1_gain = ant1_gain + App.SignalApparatus.ANT_GAIN;
            catch ME,
                
                disp(' Gain: ');
                App.SignalApparatus.ANT_GAIN
                disp(' Model: ');
                App.SignalApparatus.ANT_MODEL
                App.theGain
                App.theAngle
                logUnsuccessfulSearch(App);
                rethrow(ME)
            end
        end
        
        % Need extend to work on this function, so it pads out bore_gain
        % with zero when bore gain has some numbers in it
        function [] = paddingMatrix(App)
            [m,n] = size(App.theGain);
            X = zeros(m,n);
            App.BandsApparatus.boreGain = X;
        end
        
        % Co-ordinates for App
        function [lat,long] = ApparatusLatLong(App)
            lat = getLat(App.LocationApparatus);
            long = getLong(App.LocationApparatus);
        end
        
        % Convert a device (row) in sortedData to an Apparatus obj
        function [] = loadApparatus(App, sortedData, row, Bands)
            App.IDApparatus.setACCESS_ID(sortedData{row,1});
            App.BandsApparatus.setFreq1Manual(sortedData{row,2});
            App.TranTypeApp.setMode1(sortedData{row,3});
            App.TranTypeApp.setMode2(sortedData{row,4});
            setTxPower(App.CoordinateApparatus,sortedData{row,5});
            setAz(App.LocationApparatus,sortedData{row,6});
            setPol(App.CoordinateApparatus,sortedData{row,7})
            App.IDApparatus.setHeight(sortedData{row,8});
            App.LocationApparatus.setSITE_ID(sortedData{row,9});
            App.LocationApparatus.setLat(sortedData{row,10});
            App.LocationApparatus.setLong(sortedData{row,11});
            App.BandsApparatus.setAntIDManual(sortedData{row,12});
            App.IDApparatus.setDate(sortedData{row,13});
            App.IDApparatus.setEmission(sortedData{row,14});
            App.SignalApparatus.setANT_GAIN(sortedData{row,15});
            App.SignalApparatus.setANT_MODEL(sortedData{row,16});
            setAntSize(App.CoordinateApparatus,sortedData{row,17});
            App.BandsApparatus.theBand = theBand(Bands);  
            App.SignalApparatus.BANDWIDTH = sortedData{row,18};
        end
        
        % Input: Apparatus obj
        % Output: txpower
        function txpower = getTxPower(App)
            txpower = getTxPower(App.CoordinateApparatus);
        end
        
        % Works out next default band from default band array
        % Input: Current band set in controller, App.defaultBandArray set
        % in constructor
        % Output: Either the next band in array or zero (no next band)
        function [nextBand] = nextDefaultBand(App,currentDefaultBand)
            if isempty(App.defaultBandArray)  %If not empty or equal null
                nextBand = 0;
            else
                nextDefaultBand = NextBand(App,currentDefaultBand);
                if isequal(fix(nextDefaultBand),fix(currentDefaultBand)) % Next must be similiar to current band frequency 8.3G does go to 8.5Gso out: nextBand
                    nextBand = nextDefaultBand;
                else
                    nextBand = 0; % (follow from last comment) else 10G doesnt go to 11G so out: 0
                end
            end
        end
        % Input: currentDefaultBand set at begining from Controller.m
        % Output: next larger band, compared to currentband
        function nextB = NextBand(App,currentDefaultBand)
            tempArray = App.defaultBandArray;
            tempArray = [tempArray currentDefaultBand]; % add current to the end of temp
            tempArr = unique(tempArray);
            indexOfCurrent = find(tempArr==currentDefaultBand);
            indexOfNextLargest = indexOfCurrent + 1;
            nextB = tempArr(indexOfNextLargest);
        end
        
        % Input: App
        % Output: A String, that corresponds to whether emission modulation designation is Digital or Analogue
        % Digital: 1,2,7
        % Analogue: 3,8
        % Composite: 9
        % Resources: http://www.acma.gov.au/webwr/_assets/main/lib100342/emission(rib8).pdf
        function modul = getModulationFromEmission(App)
            % A number is last if it is not followed (anywhere following it not just immediately) by any other number
            expression = '(\d+)(?!.*\d)';
            string = App.IDApparatus.emission;
            [match,~] = regexp(string,expression);
            %modul = digitalOrAnalogue(match);
            if isequal(match,'1')
                modul = 'D';
            elseif isequal(match,2)   
                modul = 'D';
            elseif isequal(match,3)   
                modul = 'A';
            elseif isequal(match,7)   
                modul = 'D';  
            elseif isequal(match,8)   
                modul = 'A';  
            elseif isequal(match,9)   
                modul = 'C';  
            else
                disp('Error digitalOrAnalogue function, doesnt match digital, analogue, or composite')
                match
            end
        end
        
        function dac = digitalOrAnalogue(modul) 
            if isequal(modul,'1')
                dac = 'D';
            elseif isequal(modul,'2')   
                dac = 'D';
            elseif isequal(modul,'3')   
                dac = 'A';
            elseif isequal(modul,'7')   
                dac = 'D';  
            elseif isequal(modul,'8')   
                dac = 'A';  
            elseif isequal(modul,'9')   
                dac = 'C';  
            else
                disp('Error digitalOrAnalogue function, doesnt match digital, analogue, or composite')
                modul
            end
        end
        
    end
end