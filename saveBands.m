classdef saveBands < handle
    %SAVEBANDS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        filename;
        table;
    end
    
    methods
        function save = saveBands(filename,bands)            
            save.filename = [filename,'-',datestr(date),'.csv'];
            save.table = bands.tableBands;           
        end
        
        %% Filename of folder
        function addFileName(save,theFilename)
            if ~exist('Jobs','dir')
                mkdir('Jobs');
            end
            save.filename=[theFilename,'-',datestr(date),'.csv'];
        end            
        
        %% Write table to csv
        function [] = writeTable(save)
            writetable(save.table,strcat('Jobs/',save.filename));
            %This warning is displayed when your requested worksheet does
            %not exist and is created.
            warning('off','MATLAB:xlswrite:AddSheet');
        end
        
        %% Open saved file in Excel
        function openFolder(save)
            winopen(strcat('Jobs/',save.filename));
        end        
    end    
end

