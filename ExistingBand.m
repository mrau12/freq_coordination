classdef ExistingBand
    %EXISTINGCHANNELBAND Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Channels; %array of channel objects, if 8 chan then 8 objs
        TxRxGap;
        start;
        numberOfChannels;
        ArrBandwCentre; 
    end
    
    methods
        function exBand = ExisitngBand(existingChan,Gap,start,noChans)
            switch nargin
                case 0
                    exBand.Channels = [];
                    exBand.TxRxGap = [];
                    exBand.start = [];
                    exBand.numberOfChannels = [];
                case 4
                    exBand.Channels = existingChan;
                    exBand.TxRxGap = Gap;
                    exBand.start = start;
                    exBand.numberOfChannels = noChans;
                otherwise
                    disp('error defining ExistingBand Object')
            end
        end
        
        function setBandwidthAndCentre(exBand)
            for channel = 1:exBand.numberOfChannels;
               exBand.ArrCentre = [exBand.Channels(channel).CENTRE_FREQ, exBand.ArrBandwCentre];
               exBand.ArrBandwidth = [exBand.Channels(channel).CHAN_BANDWIDTH, exBand.ArrBandwCentre];               
            end
        end
    end    
end

