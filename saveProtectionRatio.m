classdef saveProtectionRatio < handle
    %SAVEPROTECTIONMARGIN Summary of this class goes here
    %   Detailed explanation goes here
    
    properties 
         filename;
         names; % table columns names
         table;
         data;
         totalChannels;
    end
    
    methods
        function save = saveProtectionRatio(protM)
              save.filename = [];
              save.names = [];
              save.table = [];
              save.data = protM.protMarginAr; %protM.protMarginAr
              save.totalChannels = protM.bands.tableBands.channels;
        end
        
        function createNames(save)
            save.names = {'Vicitm_ID'};
            mainChans = save.totalChannels/2;
            for mainChannelNum = 1:mainChans
                save.names{end+1} = strcat('Channel',num2str(mainChannelNum));
            end 
            for primeChannelNum = 1:mainChans
                save.names{end+1} = strcat('pChannel',num2str(primeChannelNum));
            end 
        end
        
         function createFileName(save,theFilename)
            if ~exist('Jobs','dir')
                mkdir('Jobs');
            end
            save.filename=[theFilename,'-',datestr(date),'.csv'];
         end
        
         function createTable(save)
             save.table = cell2table(save.data','VariableNames',save.names);
         end
         
         function writeTheTable(save)             
             writetable(save.table,strcat('Jobs/',save.filename));
            %This warning is displayed when your requested worksheet does
            %not exist and is created.
            warning('off','MATLAB:xlswrite:AddSheet');
         end 
    end    
end

