classdef Victims < handle
    %VICTIM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        sorted;
        vics;
        numOfVictims;
       
    end
    
    methods
        function victims = Victims(sortedData)
            victims.sorted  = sortedData;
            victims.vics = 0;
            victims.numOfVictims = 0;
        end
        
        function len = victimListLength(victims)
            len = length(victims.sorted);
        end
        
        function setVictims(victims)
            for victimNumber = 1:length(victims.sorted)
                vic(victimNumber) = setVictim(victims,victimNumber);
            end
            victims.vics = vic;            
        end
        
        function theVic = setVictim(victims,row)
        vic = Channel(victims.sorted{row,2},victims.sorted{row,18});
        setUpperFr(vic);
        setLowerFr(vic);
        setID(vic,victims.sorted{row,1});
        theVic = vic;
        end
    end    
end

