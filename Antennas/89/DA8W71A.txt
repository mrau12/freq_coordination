RFS 
DA 8 - W71 A  (P) 
8 ; Parabolic antenna single polarisation 
NONE 
NONE 
20030601  
HQM 010703a 
7125 - 8500 MHZ 
43.1 dBi 
1.1 Deg 
HH 67
-180.0  -68.00 
 -95.0  -68.00 
 -85.0  -51.00 
 -75.0  -46.00 
 -60.0  -46.00 
 -40.0  -45.00 
 -30.0  -42.00 
 -25.0  -37.00 
 -15.0  -35.00 
 -11.0  -34.00 
 -10.0  -31.00 
  -6.5  -29.00 
  -4.0  -25.00 
  -2.0  -24.40 
  -1.9  -22.00 
  -1.8  -19.70 
  -1.7  -17.60 
  -1.6  -15.60 
  -1.5  -13.70 
  -1.4  -11.90 
  -1.3  -10.30 
  -1.2   -8.80 
  -1.1   -7.40 
  -1.0   -6.10 
  -0.9   -4.90 
  -0.8   -3.90 
  -0.7   -3.00 
  -0.6   -2.20 
  -0.5   -1.50 
  -0.4   -1.00 
  -0.3   -0.50 
  -0.2   -0.20 
  -0.1   -0.10 
   0.0    0.00 
   0.1   -0.10 
   0.2   -0.20 
   0.3   -0.50 
   0.4   -1.00 
   0.5   -1.50 
   0.6   -2.20 
   0.7   -3.00 
   0.8   -3.90 
   0.9   -4.90 
   1.0   -6.10 
   1.1   -7.40 
   1.2   -8.80 
   1.3  -10.30 
   1.4  -11.90 
   1.5  -13.70 
   1.6  -15.60 
   1.7  -17.60 
   1.8  -19.70 
   1.9  -22.00 
   2.0  -24.40 
   4.0  -25.00 
   6.5  -29.00 
  10.0  -31.00 
  11.0  -34.00 
  15.0  -35.00 
  25.0  -37.00 
  30.0  -42.00 
  40.0  -45.00 
  60.0  -46.00 
  75.0  -46.00 
  85.0  -51.00 
  95.0  -68.00 
 180.0  -68.00 
HV 17
-180.0  -68.00 
 -90.0  -68.00 
 -80.0  -57.00 
 -60.0  -57.00 
 -30.0  -49.00 
  -9.0  -49.00 
  -6.0  -45.00 
  -2.0  -30.00 
   0.0  -30.00 
   2.0  -30.00 
   6.0  -45.00 
   9.0  -49.00 
  30.0  -49.00 
  60.0  -57.00 
  80.0  -57.00 
  90.0  -68.00 
 180.0  -68.00 
VV 67
-180.0  -68.00 
 -95.0  -68.00 
 -85.0  -51.00 
 -75.0  -46.00 
 -60.0  -46.00 
 -40.0  -45.00 
 -30.0  -42.00 
 -25.0  -37.00 
 -15.0  -35.00 
 -11.0  -34.00 
 -10.0  -31.00 
  -6.5  -29.00 
  -4.0  -25.00 
  -2.0  -24.40 
  -1.9  -22.00 
  -1.8  -19.70 
  -1.7  -17.60 
  -1.6  -15.60 
  -1.5  -13.70 
  -1.4  -11.90 
  -1.3  -10.30 
  -1.2   -8.80 
  -1.1   -7.40 
  -1.0   -6.10 
  -0.9   -4.90 
  -0.8   -3.90 
  -0.7   -3.00 
  -0.6   -2.20 
  -0.5   -1.50 
  -0.4   -1.00 
  -0.3   -0.50 
  -0.2   -0.20 
  -0.1   -0.10 
   0.0    0.00 
   0.1   -0.10 
   0.2   -0.20 
   0.3   -0.50 
   0.4   -1.00 
   0.5   -1.50 
   0.6   -2.20 
   0.7   -3.00 
   0.8   -3.90 
   0.9   -4.90 
   1.0   -6.10 
   1.1   -7.40 
   1.2   -8.80 
   1.3  -10.30 
   1.4  -11.90 
   1.5  -13.70 
   1.6  -15.60 
   1.7  -17.60 
   1.8  -19.70 
   1.9  -22.00 
   2.0  -24.40 
   4.0  -25.00 
   6.5  -29.00 
  10.0  -31.00 
  11.0  -34.00 
  15.0  -35.00 
  25.0  -37.00 
  30.0  -42.00 
  40.0  -45.00 
  60.0  -46.00 
  75.0  -46.00 
  85.0  -51.00 
  95.0  -68.00 
 180.0  -68.00 
VH 17
-180.0  -68.00 
 -90.0  -68.00 
 -80.0  -57.00 
 -60.0  -57.00 
 -30.0  -49.00 
  -9.0  -49.00 
  -6.0  -45.00 
  -2.0  -30.00 
   0.0  -30.00 
   2.0  -30.00 
   6.0  -45.00 
   9.0  -49.00 
  30.0  -49.00 
  60.0  -57.00 
  80.0  -57.00 
  90.0  -68.00 
 180.0  -68.00 
