% Michael McClellan
% SAT PTY LTD
% Created: OCT 2015
% Modified: APR 2016 08:00:00
% Revision 0.20
% Does the main call of SQL database
% MVC: Model
% Examples:
%
% Provide sample usage code here
%
% See also: List related files here
classdef Database1 < handle
    % write a description of the class here.
    properties
        % define the properties of the class here, (like fields of a struct)
        midLongitude; % ATENTION % we lose some decimal places
        midLatitude; % ATENTION % we lose some decimal places
        radius;
        rawData; % dont need to save this as property since only gets used once
        lowFreq;
        highFreq;
        app3Loc;
        app4Loc;
        Logon;
        Message;
    end
    
    %checks and displays ACMA data date
    methods
        
        % constructor
        function Datab = Database1(radius,low,high,appCLoc,appDLoc,Logon)
            %% The radius (km) in which database look at sites from inputed
            % sites C and D
            Datab.radius = radius;
            Datab.lowFreq = low;
            Datab.highFreq = high;
            Datab.app3Loc = appCLoc; %siteC
            Datab.app4Loc = appDLoc; %siteD
            Datab.Logon = Logon;
            Datab.Message = '';
            
        end
        
        function delete(Datab)           
        end
        
        function [] = checkDate(Logon)
            
            %% checks that the datebase is no less than a day old
            day_c=datestr(now,7);
            month_c=datestr(now,5);
            year_c=datestr(now,10);
            conn = database(Logon.dbname,Logon.username,Logon.password,Logon.driver,Logon.dburl);
            SQL_command='SELECT * FROM  acma_new.curr_date ';
            curs=exec(conn,SQL_command); %defines a command
            SLQreturn=fetch(curs); %executes command
            close(conn)
            day_A=num2str(SLQreturn.Data{3});
            month_A=num2str(SLQreturn.Data{2});
            year_A=num2str(SLQreturn.Data{1});
            display(['The current date is ',day_c,'/',month_c,'/',year_c,'/'])
            display(['The ACMA database was last updated at ',num2str(SLQreturn.Data{4}),' hrs. On the ',num2str(SLQreturn.Data{3}),'/',num2str(SLQreturn.Data{2}),'/',num2str(SLQreturn.Data{1}),])
            if ~(isequal(day_A,day_c)||isequal(str2num(day_A),str2num(num2str(day_c))-1))
                error('ACMA database is out of date. Please update database or contact Daniel B Gain on 0498728789 or email @ dan@satpty.com.')
            end
        end
        
        %% Calculates the latitude midpoint between site C and site D
        % and calculates the longitude midpoint between site C and site D
        % Input: (latC,longC, latD, and longD) from gui
        % Output: midLong and midLat
        function [] = setMidCoord(Datab)
            Datab.midLongitude = (getLong(Datab.app3Loc)+getLong(Datab.app4Loc))/2;
            Datab.midLatitude = (getLat(Datab.app3Loc)+getLat(Datab.app4Loc))/2;           
        end
        
        %% OLD data types               NEW Apparatus data types
        % columns=['ACCESS_ID,',...1 ------> app.ACCESS_ID
        %         'FREQ_ASS,',...2 --------> app.frequency1
        %         'MODE_,',...3 -----------> app.Mode1;
        %         'MODE_,',...4 -----------> app.Mode2;
        %         'TX_POWER,',...5  -------> app.TX_POWER;
        %         'ANT_AZ,',...6
        %         'ANT_POL,',...7 ---------> app.pol;
        %         'HEIGHT,',...8 ----------> app.HEIGHT;
        %         'assign.SITE_ID,',..9 ---> app.SITE_ID;
        %         'site.LATITUDE,',...10---> app.latitude;
        %         'site.LONGITUDE,',...11--> app.longitude;
        %         'assign.ANT_ID,',...12 ----> app.ANT_ID;
        %         'AUTHORISATION_DATE,',..13-> app.date;
        %         'EMISSION,'...14---------> app.emission;
        %         'antenna.ANT_GAIN,'...15-> app.ANT_GAIN;
        %         'antenna.ANT_MODEL,',...16--> app.ANT_MODEL;
        %         'antenna.ANT_SIZE',... 17--> app.ANT_SIZE;
        %         'assign.BANDWIDTH;  18--> app.BANDWIDTH;
        %Calculates Datab.rawData from SQL lookup 
        function [] = getRawData(Datab)       
            mainQuery(Datab);
            warningIncorrectParameters(Datab);        
            cell2mat(Datab.rawData(:,11)); % cell2mat(Datab.rawData(:,10));
            vdist_C=vdist(cell2mat(Datab.rawData(:,10)),cell2mat(Datab.rawData(:,11)),Datab.midLatitude,Datab.midLongitude); %calculate the vertical distance from C
            Datab.rawData((vdist_C>(Datab.radius*10^3)),:)=[]; % deletes unnessary entry
        end
        
        % Connects to database for main antenna query
        function mainQuery(Datab)            
            %% removes entries based on polar distance from sites
            %whos data lat long
            %%
            %% Define physical area
            %latitude_a: 1 Degree = 111 Km
            %Londitude: 1 Degree = Cos(Londitude)*111 Km
            %input: midlong,midlat,freq1,freq2,raidus
            Raidus_km=Datab.radius;%Km
            raidus_deg_lat=Raidus_km/111;
            raidus_deg_long=Raidus_km/(cos(Datab.midLongitude)*111);
            lat_start= Datab.midLatitude-raidus_deg_lat;
            lat_stop=Datab.midLatitude+raidus_deg_lat;
            long_start=Datab.midLongitude-raidus_deg_long;
            long_stop=Datab.midLongitude+raidus_deg_long;
            %% Define frequency search area - need to elabrate
            f_start=Datab.lowFreq;           
            f_stop=Datab.highFreq;
            %%  connect to server
            % Visit http://au.mathworks.com/help/database/ug/mysql-odbc-windows.html
            % for info on how to setup ODBC connector
            columns=['ACCESS_ID,',...
                'FREQ_ASS,',...
                'MODE_,',...
                'MODE_,',...
                'TX_POWER,',...
                'ANT_AZ,',...
                'ANT_POL,',...
                'HEIGHT,',...
                'assign.SITE_ID,',...
                'site.LATITUDE,',...
                'site.LONGITUDE,',...
                'assign.ANT_ID,',...
                'AUTHORISATION_DATE,',...
                'EMISSION,'...
                'antenna.ANT_GAIN,'...
                'antenna.ANT_MODEL,',...
                'antenna.ANT_SIZE,',...
                'assign.BANDWIDTH'];
                
            
            if long_start>long_stop;
                temp=long_start;
                long_start=long_stop;
                long_stop=temp;
            end
            SQL_command=['SELECT ',columns,' FROM  acma_new.site ',...
                'LEFT JOIN acma_new.assign ON assign.SITE_ID=site.SITE_ID ',...
                'LEFT JOIN acma_new.antenna ON assign.ANT_ID=antenna.ANT_ID ',...
                'WHERE (FREQ_ASS BETWEEN ',...
                num2str(f_start),' AND ',num2str(f_stop),') AND (LATITUDE BETWEEN ',...
                num2str(lat_start),' AND ',num2str(lat_stop),') AND (LONGITUDE BETWEEN ',num2str(long_start),...
                ' AND ',num2str(long_stop),') '];  
            try
            conn = database.ODBCConnection(Datab.Logon.dbsource,Datab.Logon.username,Datab.Logon.password);
            Datab.Message = conn.Message;
            databaseConnectedMsg(Datab);
            curs=exec(conn,SQL_command); %defines a command            
            SLQreturn=fetch(curs); %executes command
            Datab.rawData = SLQreturn.Data;       
            close(conn);
            catch
                close(conn);
                display('ERROR: Database1 connection...SQL query maybe taking to long')
            end
        end
        
        function databaseConnectedMsg(Datab)            
             if ~isempty(Datab.Message)
                fprintf(2,'%s\n', Datab.Message);
            else
                fprintf(1, 'Querying database for table: ACCESS_ID,FREQ_ASS,MODE,....,ANT_SIZE,BANDWIDTH.\n');
            end
        end
        
        %Warning empty rawData built SQL server
        function warningIncorrectParameters(Datab)
            if isequal(Datab.rawData,0)                
                display('ERROR: No data was found in those search paramaters. Database1 error');                
            elseif isequal(Datab.rawData{:},'No Data')                
                display('ERROR: No data was found in those search paramaters Database1 error');                
            end         
        end
        
        %Sorts data by frequency
        function [sortedData] = sortRawData(Datab)
            sortedData = sortrows(Datab.rawData,[2,1,-3]);% Sort by frequency
        end
        
    end
end