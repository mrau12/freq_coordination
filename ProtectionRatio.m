classdef ProtectionRatio < handle
    %PROTECTIONMARGINS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        mod2mod; % Digital to Digital
        victims; % ACCESSID	FREQASS	 MODEA	MODEB	TXPOWER	ANTAZ	ANTPOL	HEIGHT	SITEID	LATITUDE	LONGITUDE
        proRatTable; % Protection Ratio Rules for one inputted band (from fx3 rali)
        bands; % Relevant info for specific (input band) e.g. upper, lower freq, channel start, bandwidth ,etc
        correction; % Protection factor offset value for average rain per annum
        distance; % Table distance between victims and input apps
        protMarginAr;
        spectrum;
        %% spectrum %%
        %% Interferer Channels
        % It contains a spectrum of victim IDs. Each Victim has a spectrum of channels (e.g. channel 1,2,3,4,5,6,7,8).
        % Each channel contains: Channel Bandwidth, centre frequency, lower
        % freq, protectionMargin, freq Offset, Modulation type, Id
        %% Victim Channel
        % victimChannel - singular Victim channel being compared against interferer band  
        % victim ID;
        %% Misc - data used calc between victim and interferer
        % tableProRatio - Protection Ratio Rules for one inputted band (from fx3 rali)    
        % rules - simplified table protection ratio(dB) for multiple frequency offsets i.e. cochannel and adjchannel
        % mod2mod - modulation type ---> modulation type e.g. digital ---> analogue
        % savetable;
        % arrayProtMargin;
        % correction;
        % theBand;
        
    end
    
    methods
        function protM = ProtectionRatio(AppC,AppD,m2m,sorted,proRatioTABLE,bands,correction)
            setMod2Mod(protM,m2m);
            setAllVictims(protM,sorted);
            setEmptySpectrumObjs(protM);
            setupDistance(protM,sorted,AppC,AppD);
            setProtectionTable(protM,proRatioTABLE);
            setBands(protM,bands);
            setCorrection(protM,correction);
            protM.protMarginAr = [];           
        end
        
        % correction for rain interference        
        function setCorrection(protM,correction)
            protM.correction = correction;
        end
        
%       {'band','Type','fo','fL','fh','channels','bandwidth','TxRx_gap','Blackouts','channel_start','path_min','restrictions','page'}; 
        function setBands(protM,bands)
            protM.bands = bands;
        end
        
        % Protection Ratio Rules for specific band
        function setProtectionTable(protM,proRatioTABLE)
             protM.proRatTable = proRatioTABLE;
        end
        
        % Setting Moulation to Modulation
        function setMod2Mod(protM,m2m)
            protM.mod2mod = m2m;
        end
        
        %        ACCESSID	FREQASS	 MODEA	MODEB	TXPOWER	ANTAZ	ANTPOL	HEIGHT	SITEID	LATITUDE	LONGITUDE	ANTID	AUTHORISATIONDATE	EMISSION	ANTGAIN	ANTMODEL	ANTSIZE	BANDWIDTH
        % Eg:   % {'1511511/1',10735000000.0000,'T','T',1,13.6999998092651,'H',NaN,'48711',-33.8200988769531,151.184997558594,'1096','2000-07-27','40M0D7W',49,'UHX10-107R',3,40000000;
                % '1511511/1',10735000000.0000,'R','R',NaN,193.679992675781,'H',NaN,'4245',-33.6926994323731,151.222000122070,'325','2000-07-27','40M0D7W',46.5000000000000,'UHX8-107H',2.40000009536743,40000000}
                % ...
        function setAllVictims(protM,sorted)
            victms = Victims(sorted);
            setVictims(victms);
            protM.victims = victms;
        end 
        
        % Sets up table distance between victims and input apps
        % {'Licence_No','Site_ID','Site_LAT','Site_LONG' ,'dist_Site_2_AppC','dist_Site_2_AppD'}
        function setupDistance(protM,sorted,AppC,AppD)
             protM.distance = Distance(sorted,AppC,AppD);
        end
        % output: '1181133/2'
        function ident = getVictimIdentity(protM,index)
           ident = protM.victims.vics(1,index).id;
        end
        
        % output: vict  = ChanBandwidth,CentreFreq,LowerFreq,UpperFreq,ProtectionMargin,FreqOffset,Modtype,id(eg'1181133/2')
        function vict = getVictim(protM,index)
           vict = protM.victims.vics(1,index);
        end 
        
        function setEmptySpectrumObjs(protM)
            for index = 1:victimListLength(protM.victims)
            s(index) = Spectrum();
            end
            protM.spectrum = s;
        end
        
        % Calc between victim chan and infer chans
        % Output: Protection ratios for Channels
        function setProtectionMargins(protM) 
            protM.protMarginAr = [];            
            for index = 1:victimListLength(protM.victims)
                protM.spectrum(index) = Spectrum(protM.bands,protM.proRatTable,getVictim(protM,index),protM.mod2mod,getVictimIdentity(protM,index),protM.correction);
                lookUpProtectionRatioRules(protM.spectrum(index));
                setProtMarginTable(protM.spectrum(index));
                setTable(protM.spectrum(index));
                protM.protMarginAr = horzcat(protM.protMarginAr,addProtnMarginAndFactor(protM,index));
            end          
        end
        
        function [channelProtMargin] = addProtnMarginAndFactor(protM,index)
           
           [channelProtMargin] = protM.spectrum(1,index).arrayProtMargin';
           [v2C,v2D] = correctionFactors(protM,index);
           for index = 2:length(channelProtMargin)
                % if (.pol,'H')
             channelProtMargin{index} = channelProtMargin{index} + v2C;
           end
        end
        
        function [pro_corr_VicNum2AppC,pro_corr_VicNum2AppD] = correctionFactors(protM,num)
            dist_from_AppC = getDistFromAppC(protM.distance,num);
            dist_from_AppD = getDistFromAppD(protM.distance,num);
            [pro_corr_VicNum2AppC] = correctionFactor(protM,dist_from_AppC);
            [pro_corr_VicNum2AppD] = correctionFactor(protM,dist_from_AppD);
        end        
       
           %% Finds protection factor offset value (refer to acma fx3 graph PROTECTION RATIO CORRECTION FACTORS MULITPATH)
        % Inputs:
        % Path length between sites in (km)
        % Rain (mm) (which line of three or more on graph in acma fx3)
        % Output:
        % Correction factor(dB)
        function [protect_corr] = correctionFactor(protM,path_length)
            
            path_length=path_length/1000;%convert meters to km
            if protM.correction<0 %rain rate
                %load rain rate data
                rain_rate=-protM.correction;
                if protM.bands.tableBands.band>=15
                    if protM.bands.tableBands.band>=18
                        if protM.bands.tableBands.band>=22
                            if protM.bands.tableBands.band>=38 % 38 GHz band
                                a=-0.24;b=9.45;c=-33.5;
                                CF_40=a*path_length^2+b*path_length+c;
                                a=-0.75;b=15.24;c=-35;
                                CF_60=a*path_length^2+b*path_length+c;
                                a=-0.9874;b=18.793;c=-33.754;
                                CF_80=a*path_length^2+b*path_length+c;
                                protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
                                if protect_corr<-25.5,protect_corr=-25.5;end
                            else % 22 GHz band
                                a=-0.0815;b=3.9605;c=-34.584;
                                CF_40=a*path_length^2+b*path_length+c;
                                a=-0.1266;b=5.4266;c=-31.49;
                                CF_60=a*path_length^2+b*path_length+c;
                                a=-0.2017;b=7.0796;c=-30.828;
                                CF_80=a*path_length^2+b*path_length+c;
                                protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
                                if protect_corr<-23.5,protect_corr=-23.5;end
                            end
                        else % 18 GHz band
                            a=16.432;b=54.212;c=0;
                            CF_40 = a*log(path_length)-b;
                            a=19.768;b=52.135;c=0;
                            CF_60= a*log(path_length)-b;
                            a=20.439;b=47.256;c=0;
                            CF_80= a*log(path_length)-b;
                            protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
                            if protect_corr<-30,protect_corr=-30;end
                        end
                    else % 15 GHz band
                        pl_save=path_length;
                        if path_length<5,path_length=5;end
                        a=11.166;b=47.827;c=0;
                        CF_40 = a*log(path_length)-b;
                        a=13.833;b=47.227;c=0;
                        CF_60= a*log(path_length)-b;
                        a=13.097;b=39.813;c=0;
                        CF_80= a*log(path_length)-b;
                        protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
                        if pl_save<5, protect_corr=pl_save*((protect_corr-(-40))/(5-0))+(-40);end %linear interpolation between 5 & 0 km.
                    end
                else
                    protect_corr=0;
                    display('WARNING - rain rate CF undefined for band ',num2str(protM.bands.tableBands.band),'GHz');
                end
            else %path length
                %path_lenth_corr=dump.correction;
                %bands.band
                if str2double(protM.bands.tableBands.band)<=5
                    protect_corr_5 = 15.798*log(path_length)-73.5;
                    protect_corr_10= 15.664*log(path_length)-68.725;
                    protect_corr_20= 15.174*log(path_length)-62.23;
                    protect_corr=interp_PLCF(protM,protect_corr_5,protect_corr_10,protect_corr_20);
                    if protect_corr<-26.5,protect_corr=-26.5;end %PLCF floor value
                    if path_length>110, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
                elseif str2double(protM.bands.tableBands.band)<=8
                    protect_corr_5 = 15.633*log(path_length)-70.232;
                    protect_corr_10= 15.924*log(path_length)-66.753;
                    protect_corr_20= 15.151*log(path_length)-59.297;
                    protect_corr=interp_PLCF(protM,protect_corr_5,protect_corr_10,protect_corr_20);
                    if protect_corr<-34,protect_corr=-34;end %PLCF floor value
                    if path_length>100, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
                elseif str2double(protM.bands.tableBands.band)<=11
                    protect_corr_5 = 15.56*log(path_length)-61.962;
                    protect_corr_10= 15.523*log(path_length)-57.451;
                    protect_corr_20= 15.83*log(path_length)-53.951;
                    protect_corr=interp_PLCF(protM,protect_corr_5,protect_corr_10,protect_corr_20);
                    if protect_corr<-26,protect_corr=-26;end %PLCF floor value
                    if path_length>70, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
                elseif str2double(protM.bands.tableBands.band)<=13
                    protect_corr_5 = 15.837*log(path_length)-57.755;
                    protect_corr_10= 15.779*log(path_length)-53.901;
                    protect_corr_20= 15.79*log(path_length)-48.684;
                    protect_corr=interp_PLCF(protM,protect_corr_5,protect_corr_10,protect_corr_20);
                    if protect_corr<-20,protect_corr=-20;end %PLCF floor value
                    if path_length>70, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
                else
                    protect_corr=0;
                end
            end
        end
        
        function [protect_corr]=interp_PLCF(coordinate,protect_corr_5,protect_corr_10,protect_corr_20)
            if coordinate.correction<=5
                protect_corr=protect_corr_5;
            elseif coordinate.correction<=10
                protect_corr =((coordinate.correction-5)*(-protect_corr_10+protect_corr_5)/(10-5)-protect_corr_5);
            elseif coordinate.correction<=20
                protect_corr =((coordinate.correction-10)*(-protect_corr_20+protect_corr_10)/(20-10)-protect_corr_10);
            else
                protect_corr=protect_corr_20;
                display('Warning Path Length correction factor undefined for PL value')
            end
            
        end
        
        function [protect_corr]=interp_RRCF(coordinate,protect_corr_5,protect_corr_10,protect_corr_20)
            if coordinate.correction<=5
                protect_corr=protect_corr_5;
            elseif coordinate.correction<=10
                protect_corr =((coordinate.correction-5)*(-protect_corr_10+protect_corr_5)/(10-5)-protect_corr_5);
            elseif coordinate.correction<=20
                protect_corr =((coordinate.correction-10)*(-protect_corr_20+protect_corr_10)/(20-10)-protect_corr_10);
            else
                protect_corr=protect_corr_20;
                display('Warning Path Length correction factor undefined for PL value')
            end
        end
        
    end    
end


