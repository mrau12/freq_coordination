classdef AvailableChan < handle
    %AVAILABLECHAN Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        protectMargins; % Input array
        cleanProtMargins; % Protection Margins array without Victim ID info
        availableChannels; % Output array
        chSize; % # Channels
        maxValues; % maximum value from channels for victim
    end
    
    methods
        function available = AvailableChan(protM,channelsSize)
            available.protectMargins = protM;
            available.chSize = channelsSize;
        end
        
        function maxInColumn(available)  
            for col = 1:available.cleanProtMargins
            [M,~] = max([available.cleanProtMargins{:,col}]);
            available.maxValues = M;
            end
        end
        
        function newArray(available)
            available.protectMargins;
            B = available.protectMargins;
            B(1,:) = [];
            available.cleanProtMargins = B;
        end
        
    end    
end

