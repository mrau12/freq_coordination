classdef ApparatusBands < handle
    %BANDSAPPARATUS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        ANT_ID; %bands from bands class        
        frequency1; %freq1 from bands class        
        boreGain;
        theBand;
    end
    
    methods
        function BandsApp = ApparatusBands(Bands)
            switch nargin
                case 0
                    BandsApp.ANT_ID = [];                  
                    BandsApp.frequency1 = [];
                    BandsApp.boreGain = []; 
                    BandsApp.theBand = [];
                     
                case 1                    
                    setFrequency1(BandsApp,Bands); 
                    setTheBand(BandsApp,Bands);  
                    
                otherwise
                    disp('error incorrect parameters for AppBands object')
            end
        end
        
        function [] = setTheBand(BandsApp,Bands)            
            BandsTable = getBands(Bands);
            BandsApp.theBand = BandsTable.band;
        end
        
        function [] = setFrequency1(BandsApp,Bands)
            BandsApp.frequency1 = freqLow(Bands);
        end %make database object first
        
        function [] = setAntIDManual(BandsApp,ant_id)        
            BandsApp.ANT_ID = ant_id;
        end
        
        function [] = setFreq1Manual(BandsApp,freq)
            BandsApp.frequency1 = freq;
        end     
      
    end
end

