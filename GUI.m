function varargout = GUI(varargin)
% GUI MATLAB code for GUI.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI

% Last Modified by GUIDE v2.5 12-Jul-2016 17:56:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @GUI_OpeningFcn, ...
    'gui_OutputFcn',  @GUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end
if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
% To main code is in Function "pushbutton1_Callback"
end
% --- Executes just before GUI is made visible.
function GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI (see VARARGIN)

% Choose default command line output for GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);
end
% --- Outputs from this function are returned to the command line.
function varargout = GUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
end
% --- Executes on button press in Get_data.
function Get_data_Callback(hObject, eventdata, handles)
% hObject    handle to Get_data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%
%% Name of file in Jobs
jobName = get(handles.Job_Name,'String');

%% Limit of axes in Plot
limit = handles.axes1;

%% Radius
radius = str2double(get(handles.radius,'String'));

%% Gets frequncy units
bandIndex = get(handles.band,'Value');

%% Logon
Logon = LogonODBC('googleSQLAsia','root','matlab123');% Default settings, username,pass, etc for google SQL server
%
% Site C / AppC
lat3 = str2double(get(handles.latitude_C,'String'));
long3 = str2double(get(handles.longitude_C,'String'));
%
% Site D / AppD
lat4 = str2double(get(handles.latitude_D,'String'));
long4 = str2double(get(handles.longitude_D,'String'));

%-----------------> SETUP INPUT APPARATUS: App3 & App4
bands = BandsSQL(bandIndex,Logon);

%-----------------> location object
loc_3 = ApparatusLocation(lat3,long3);
loc_4 = ApparatusLocation(lat4,long4);

%-----------------> DATABASE SETUP
data = Database1(radius,freqLow(bands),freqHigh(bands),loc_3,loc_4,Logon);
% Set(handles.result,'String',size(sorted,1)); % display number of items
% Mid location for AppC and AppD
setMidCoord(data);

% Make SQL query and return the relevant data
getRawData(data);

% Sort relevant data so easier to read or manipulate
sorted = sortRawData(data);

%-----------------> Plot area
pGoogle = PlotGoogle(sorted,limit,lat3,long3,long4,lat4);
plotArea(pGoogle);

%-----------------> Jpg Plot
figs = getFigs(pGoogle);
saveP = savePlot(figs,jobName);
createFolder(saveP);

%----------------->Share Sorted, so available to coordinate object call
handles.sorted = sorted;
guidata(hObject,handles);%Need this line to solidify copy to handles

%------------------> Sites in search area
res = size(sorted,1);
set(handles.result,'String',res);

%------------------> String concat, name .csv files, ties names to jobname

bandsFileName = strcat(jobName,'_Bands');
appFileName = strcat(jobName,'_Apparatus');

%------------------> Save bands
saveBand = saveBands(bandsFileName,bands);
writeTable(saveBand);

%------------------> Save Apparatus within given radius search
saveApp = saveApparatus(sorted);
createFileName(saveApp,appFileName);
objSQLToTable(saveApp);
writeSQL(saveApp);

msgbox('Operation Completed','getData');
end

%% --- Executes on button press in Coordinate.
function Coordinate_Callback(hObject, eventdata, handles)
% hObject    handle to Coordinate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

jobName = get(handles.Job_Name,'String');

clc;
%% Set New link
%
% Site C / AppC
lat3 = str2double(get(handles.latitude_C,'String'));
long3 = str2double(get(handles.longitude_C,'String'));
%
% Site D / AppD
lat4 = str2double(get(handles.latitude_D,'String'));
long4 = str2double(get(handles.longitude_D,'String'));
%
%siteID = '4123';
date = datestr(now,29);
access = '0000';
emission = 'XXXX';
height = 5;

% bore_gain = 10*log10(0.6*(frequency1*pi*ANT_SIZE/(3*10^8))^2);
%gain = 1;

ModelID = 'DEFAULT';  % folders in antenna folder: DEFAULT,89,99,NOTIONAL

%% Radius
radius = str2double(get(handles.radius,'String'));

%% Gets frequncy units
bandIndex = get(handles.band,'Value');

%% Tx Power
TxPower = str2double(get(handles.Txpower,'String'));

%% TxRx
TxRx = get(handles.popupmenu4,'Value');

%% Mod to Mod
switch get(handles.m2m,'Value')
    case 1, m2m = 'D2D'; % Digital to digital
    case 2, m2m = 'D2A'; % Digital to analogue
    case 3, m2m = 'A2A'; % Analogue to analogue
    case 4, m2m = 'A2D'; % Analogue to digital    
end

%% Polarization
switch get(handles.popupmenu5,'Value')
    case 1, polarity = 'V'; % Vert
    case 2, polarity = 'H'; % Horizontal
end

%% Antenna Size
switch get(handles.ant_size,'Value') %Gets frequncy units
    case 1,sizeAnt = 0.3; % meters
    case 2,sizeAnt = 0.6;
    case 3,sizeAnt = 0.8;
    case 4,sizeAnt = 1.2;
    case 5,sizeAnt = 1.8;
    case 6,sizeAnt = 2.4;
    case 7,sizeAnt = 3.0;
    case 8,sizeAnt = 3.7;
    case 9,sizeAnt = 4.6;
end

%% Rain mm
%correction = 0;

if get(handles.Rain,'Value')
    correction = get(handles.Rain_Rate,'Value');% gets the rain rate in (mm)
else
    correction = -get(handles.Rain_Rate,'Value');
end

Logon = LogonODBC('googleSQLAsia','root','matlab123');% Default settings, username,pass, etc for google SQL server

%----------------->SETUP INPUT APPARATUS: App3 & App4
bands = BandsSQL(bandIndex,Logon);

%% Gain
gain = 10*log10(0.6*(freqLow(bands)*pi*sizeAnt/(3*10^8))^2);

%-----------------> location object
loc_3 = ApparatusLocation(lat3,long3);
loc_4 = ApparatusLocation(lat4,long4);

%-----------------> creates two location Apparatus object and Calc Azimuth
setup1 = SetupLocation(loc_3,loc_4);

%-----------------> signal Apparatus object
signal3_4 = ApparatusSignal(gain,ModelID);

%-----------------> transmit Apparatus object
transmit3_4 = ApparatusTransmitType(TxRx);

%-----------------> Bands Apparatus object
bandsApp3_4 = ApparatusBands(bands);

%-----------------> Coordinate object
coordinate3_4 = ApparatusCoordinate(polarity,sizeAnt,TxPower,correction);

%-----------------> Setup 2
setup2 = SetupBandsCoordSigApp(bandsApp3_4,coordinate3_4,signal3_4);

%-----------------> ID Object
IdApp3_4 = ApparatusID(access,height,emission,date);

%-----------------> Input & merge sub-apparatus into main app object
AppC = Apparatus(getSigApp(setup2),getBandApp(setup2),getLoc3(setup1),transmit3_4,IdApp3_4,getCoordApp(setup2));
AppD = Apparatus(getSigApp(setup2),getBandApp(setup2),getLoc4(setup1),transmit3_4,IdApp3_4,getCoordApp(setup2));

if (isempty(handles.sorted))
    disp('No previous Get data query...Looking up')
    %-----------------> DATABASE SETUP
    data1 = Database1(radius,freqLow(bands),freqHigh(bands),AppC.LocationApparatus,AppD.LocationApparatus,Logon);
    
    %Mid location for AppC and AppD
    setMidCoord(data1);
    
    % Make SQL query and return the relevant data
    getRawData(data1);
    
    %----------------> If NO earlier (aka handles.sorted) THEN lookup rawdata per usual ELSE use existing sorted (earlier button press 'Get
    % Data' generates handles.sorted) % Why? Saves a look up.
        
    %Sort relevant data so easier to manipulate
    sorted = sortRawData(data1);
else
    sorted = handles.sorted;
end

%-----------------> Sites in search area
set(handles.result,'String',size(sorted,1));

%------------------> Coordinate
coordinate = Coordinate(AppC,AppD,sorted,bands,correction);

%------------------> Link Calc
h = waitbar(0,'Please wait...');

dumpIndex=1;
numberOfDevices = size(coordinate.sortedData,1);
for device=2:numberOfDevices;
     waitbar(device/size(coordinate.sortedData,1));
    if isequal(coordinate.sortedData{device-1,1},coordinate.sortedData{device,1})
        loadApp1App2(coordinate,device,bands);
        dataDump(dumpIndex) = DataDump();
        dataDump(dumpIndex).setData(coordinate,bands);
        dumpIndex=dumpIndex+1;
        dumpIndex
    end
end
close(h)

%------------------> raw SQL Protection Ration Table
proRatioTABLE = ProtectionRatioSQL(Logon,theBand(bands));
setProRatioTable(proRatioTABLE);

%-----------------> Available Channel Calc
tic

protRat = ProtectionRatio(AppC,AppD,m2m,sorted,proRatioTABLE,bands,correction);
setProtectionMargins(protRat);

toc
%----------------->
bandsFileName = strcat(jobName,'_Bands');
appFileName = strcat(jobName,'_Apparatus');
dumpFileName = strcat(jobName,'_Dump');
marginFileName = strcat(jobName,'_ProtectionRatio');

%------------------> Save bands
saveBand = saveBands(bandsFileName,bands);
writeTable(saveBand);

%------------------> Save Apparatus within given radius search
saveApp = saveApparatus(sorted);
createFileName(saveApp,appFileName);
objSQLToTable(saveApp);
writeSQL(saveApp);

if dumpIndex > 1 %
%------------------> Save Dump
saveDump = saveDataDump();
addFileName(saveDump,dumpFileName);
tableValues(saveDump,dataDump);
objDumpToTable(saveDump);
writeDataDump(saveDump);
else
 disp('No Links')
end

%------------------> Save Available channels
saveMargin = saveProtectionMargin(protRat);
createFileName(saveMargin,marginFileName);
createNames(saveMargin);
createTable(saveMargin);
writeTheTable(saveMargin); 
%-----------------> Open files in Excel
% openFolder(saveDump);
% openFolder(saveAva);


msgbox('Operation Completed');
end
function latitude_C_Callback(hObject, eventdata, handles)
% hObject    handle to latitude_C (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of latitude_C as text
%        str2double(get(hObject,'String')) returns contents of latitude_C as a double
end

% --- Executes during object creation, after setting all properties.
function latitude_C_CreateFcn(hObject, eventdata, handles)
% hObject    handle to latitude_C (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function longitude_C_Callback(hObject, eventdata, handles)
% hObject    handle to longitude_C (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of longitude_C as text
%        str2double(get(hObject,'String')) returns contents of longitude_C as a double
end

% --- Executes during object creation, after setting all properties.
function longitude_C_CreateFcn(hObject, eventdata, handles)
% hObject    handle to longitude_C (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

end

function freq_Callback(hObject, eventdata, handles)
% hObject    handle to freq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of freq as text
%        str2double(get(hObject,'String')) returns contents of freq as a double
end

% --- Executes during object creation, after setting all properties.
function freq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end
function result_Callback(hObject, eventdata, handles)
% hObject    handle to result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of result as text
%        str2double(get(hObject,'String')) returns contents of result as a double

end
% --- Executes during object creation, after setting all properties.
function result_CreateFcn(hObject, eventdata, handles)
% hObject    handle to result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end
% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
end
% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end
% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1
end
% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end
function latitude_D_Callback(hObject, eventdata, handles)
% hObject    handle to latitude_D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of latitude_D as text
%        str2double(get(hObject,'String')) returns contents of latitude_D as a double
end
% --- Executes during object creation, after setting all properties.
function  latitude_D_CreateFcn(hObject, eventdata, handles)
% hObject    handle to latitude_D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end
function longitude_D_Callback(hObject, eventdata, handles)
% hObject    handle to longitude_D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of longitude_D as text
%        str2double(get(hObject,'String')) returns contents of longitude_D as a double
end
% --- Executes during object creation, after setting all properties.
function longitude_D_CreateFcn(hObject, eventdata, handles)
% hObject    handle to longitude_D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end
% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
end
% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end
function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double
end
% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end
% --- Executes on selection change in band.
function band_Callback(hObject, eventdata, handles)
% hObject    handle to band (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns band contents as cell array
%        contents{get(hObject,'Value')} returns selected item from band
end
% --- Executes during object creation, after setting all properties.
function band_CreateFcn(hObject, eventdata, handles)
% hObject    handle to band (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end
% --- Executes on selection change in popupmenu4.
function popupmenu4_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu4
end
% --- Executes during object creation, after setting all properties.
function popupmenu4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

% --- Executes on selection change in popupmenu5.
function popupmenu5_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu5 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu5

end
% --- Executes during object creation, after setting all properties.
function popupmenu5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

% --- Executes on selection change in ant_size.
function ant_size_Callback(hObject, eventdata, handles)
% hObject    handle to ant_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ant_size contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ant_size
end

% --- Executes during object creation, after setting all properties.
function ant_size_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ant_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end
function Job_Name_Callback(hObject, eventdata, handles)
% hObject    handle to Job_Name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Job_Name as text
%        str2double(get(hObject,'String')) returns contents of Job_Name as a double
end
% --- Executes during object creation, after setting all properties.
function Job_Name_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Job_Name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

% --- Executes on button press in get_range.
function get_range_Callback(hObject, eventdata, handles)
% hObject    handle to get_range (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% hObject    handle to Get_data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%%Job name
inputFilename = get(handles.Job_Name,'String');

%---------------> Logon
Logon = LogonODBC('googleSQLAsia','root','matlab123');% Default settings, username,pass, etc for google SQL server

%% Limit of axes in Plot
limit = handles.axes1;

%% Search radius
radius = str2double(get(handles.radius,'String'));

%% Get frequncy units from gui
switch get(handles.popupmenu7,'Value')
    case 1
        units = 10^9;%GHz
    case 2
        units = 10^6;%MHz
    case 3
        units = 10^3;%KHz
end

%% Start and Stop Frequency
frequency1 = str2double(get(handles.freq1,'String'))*units;
frequency2 = str2double(get(handles.freq2,'String'))*units;

%% Site C / AppC
lat3 = str2double(get(handles.latitude_C,'String'));
long3 = str2double(get(handles.longitude_C,'String'));

%% Site D / AppD
lat4 = str2double(get(handles.latitude_D,'String'));
long4 = str2double(get(handles.longitude_D,'String'));

%-----------------> Location object
appCLoc = ApparatusLocation(lat3,long3);
appDLoc = ApparatusLocation(lat4,long4);

%-----------------> Sql query
dataSql = Database1(radius,frequency1,frequency2,appCLoc,appDLoc,Logon);
setMidCoord(dataSql); % Set middle coordinates
getRawData(dataSql);
sorted = sortRawData(dataSql);

%----------------->Share Sorted
handles.sorted = sorted;
guidata(hObject,handles);

%-----------------> Sites in search area
set(handles.result,'String',size(sorted,1));

%-----------------> Plot area
pGoogle = PlotGoogle(sorted,limit,lat3,long3,long4,lat4);
plotArea(pGoogle);

%----------------> Jpg Plot
figs = getFigs(pGoogle);
saveP = savePlot(figs,inputFilename);
createFolder(saveP);
%saveFile(saveP);
%openFolder(saveP); % error Positioning Figure for ResizeFcn.

%---------------> Save range
saveJobRange = saveApparatus(sorted);
createFileName(saveJobRange,inputFilename);
objSQLToTable(saveJobRange);
writeSQL(saveJobRange);
end


function freq1_Callback(hObject, eventdata, handles)
% hObject    handle to freq1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of freq1 as text
%        str2double(get(hObject,'String')) returns contents of freq1 as a double
end

% --- Executes during object creation, after setting all properties.
function freq1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

% --- Executes on selection change in popupmenu7.
function popupmenu7_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu7 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu7
end

% --- Executes during object creation, after setting all properties.
function popupmenu7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function freq2_Callback(hObject, eventdata, handles)
% hObject    handle to freq2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of freq2 as text
%        str2double(get(hObject,'String')) returns contents of freq2 as a double
end

% --- Executes during object creation, after setting all properties.
function freq2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

end

function radius_Callback(hObject, eventdata, handles)
% hObject    handle to radius (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of radius as text
%        str2double(get(hObject,'String')) returns contents of radius as a double
end

% --- Executes during object creation, after setting all properties.
function radius_CreateFcn(hObject, eventdata, handles)
% hObject    handle to radius (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end
function Rain_Rate_Callback(hObject, eventdata, handles)
% hObject    handle to Rain_Rate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Rain_Rate as text
%        str2double(get(hObject,'String')) returns contents of Rain_Rate as a double
end

% --- Executes during object creation, after setting all properties.
function Rain_Rate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Rain_Rate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

% --- Executes during object creation, after setting all properties.
function Radio_B_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Radio_B (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
end

% --- Executes on button press in Rain.
function Rain_Callback(hObject, eventdata, handles)
% hObject    handle to Rain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Rain
end

% --- Executes during object creation, after setting all properties.
function PLCF_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PLCF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
end

% --- Executes during object creation, after setting all properties.
function Rain_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Rain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
end

function [antennas] =get_defult_antennas(band)
end


function Txpower_Callback(hObject, eventdata, handles)
% hObject    handle to Txpower (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Txpower as text
%        str2double(get(hObject,'String')) returns contents of Txpower as a double
end

% --- Executes during object creation, after setting all properties.
function Txpower_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Txpower (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


% --- Executes on selection change in m2m.
function m2m_Callback(hObject, eventdata, handles)
% hObject    handle to m2m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns m2m contents as cell array
%        contents{get(hObject,'Value')} returns selected item from m2m
end

% --- Executes during object creation, after setting all properties.
function m2m_CreateFcn(hObject, eventdata, handles)
% hObject    handle to m2m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end
