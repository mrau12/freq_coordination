% Michael McClellan
% SAT PTY LTD
% Created: OCT 2015
% Modified: APR 2016 08:00:00
% Revision 0.20
% Class Summary goes here
%  MVC: Model
% Examples:
%
% Provide sample usage code here
%
% See also: List related files here
classdef Commandline < handle
    % write a description of the class here.
    properties
        % define the properties of the class here, (like fields of a struct)
        bandIndex;
        radius;
        cLatitude;
        cLongitude;
        dLatitude;
        dLongitude;
        f1;
        f2;
        Hz;
        fx3Bands;
        polarity;
        antenna;
        transType;
        txPower;
        rain;
        modelID;
    end
    
    methods
        function obj = Commandline()
            inputRadius(obj);
            inputBandIndex(obj);
            inputCLatitude(obj);
            inputCLongitude(obj);
            inputDLatitude(obj);
            inputDLongitude(obj);
            inputTransType(obj);
            inputRain(obj);
        end
        function obj = inputBandIndex(obj)
            prompt = 'Enter a Band Index value: ';
            obj.bandIndex = input(prompt);             
        end
        function obj = inputRadius(obj)
            prompt = 'Enter a radius value: ';
            obj.radius = input(prompt);             
        end
        function obj = inputCLatitude(obj)
            prompt = 'Enter a site c latitude value: ';
            obj.cLatitude = input(prompt);             
        end
        function obj = inputCLongitude(obj)
            prompt = 'Enter a site c Longitude value: ';
            obj.cLongitude = input(prompt);             
        end
        function obj = inputDLatitude(obj)
            prompt = 'Enter a site d Latitude value: ';
            obj.dLatitude = input(prompt);             
        end
        function obj = inputDLongitude(obj)
            prompt = 'Enter a site d Longitude value: ';
            obj.dLongitude = input(prompt);             
        end
        function obj = inputF1(obj)
            prompt = 'Enter a f1 value: ';
            obj.f1 = input(prompt);             
        end
        function obj = inputF2(obj)
            prompt = 'Enter a f2 value: ';
            obj.f2 = input(prompt);             
        end
         function obj = inputHz(obj)
            prompt = 'Enter a Hz value: ';
            obj.Hz = input(prompt);             
        end
        function obj = inputFx3Bands(obj)
            prompt = 'Enter a fx3Bands value: ';
            obj.fx3Bands = input(prompt);             
        end
        function obj = inputPolarity(obj)
            prompt = 'Enter a polarity value: ';
            obj.polarity = input(prompt);             
        end
        function obj = inputModelID(obj)
            prompt = 'Enter a antenna value: ';
            obj.modelID = input(prompt);             
        end
        function obj = inputTransType(obj)
            prompt = 'Enter a transType value: ';
            obj.transType = input(prompt);             
        end
        function obj = inputTxPower(obj)
            prompt = 'Enter a txPower value: ';
            
            obj.txPower = input(prompt);             
        end
        function obj = inputRain(obj)            
            prompt = 'Enter a rain value: ';
            variable = input(prompt);
             defaultPrompt(obj,variable,defaultvalue)
        end    
        
        function obj = defaultPrompt(obj,property,variable,defaultvalue)
             if size(variable)==0;
                obj.property = defaultvalue;
            else
                obj.property = variable;
             end
        end               
    end
end