classdef DataDump < handle
    %DATADUMP Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        linkID;
        SiteAId;
        SiteBId;        
        antBudget1;
        antBudget3;
        wantedToUnwantedSignalB; % Used to be coChannelIsolationB
        CoCh_pro_B;
        Prot_Margin_B;
        antBudget4;
        antBudget2;
        wantedToUnwantedSignalD; % Used to be coChannelIsolationD
        CoCh_pro_D;
        Prot_Margin_D;
        AdjCh_pro_D;
        AdjCh_pro_B;
        A2B_pl;
        A2D_pl;
        C2B_pl;
        C2D_pl;
        assTxFreqApp1;
        
        CoChannelNum;        
        proRatioTable;
        
        theband;
        correction;       
    end
    
    methods
        %% Create empty data dump Constructor
        function dump = DataDump()
            dump.linkID = 0;
            dump.SiteAId = 0; 
            dump.SiteBId = 0;            
            dump.antBudget1 = 0;
            dump.antBudget2 = 0;
            dump.wantedToUnwantedSignalB = 0;
%             dump.CoCh_pro_B = 0; 
%             dump.Prot_Margin_B = 0;
            dump.antBudget3 = 0;
            dump.antBudget4 = 0;
            dump.wantedToUnwantedSignalD = 0;
%             dump.CoCh_pro_D = 0;         
%             dump.Prot_Margin_D = 0;
%             dump.AdjCh_pro_D = 0;
%             dump.AdjCh_pro_B = 0;
            dump.A2B_pl = 0;
            dump.A2D_pl = 0;
            dump.C2B_pl = 0;
            dump.C2D_pl = 0;  
            dump.assTxFreqApp1 = 0;
            dump.CoChannelNum = 0;
            dump.proRatioTable = 0;
            dump.CoChannelNum = 0;
        end
        
        %% Setup all variables
        function setData(dump,coordinate,bands) 
           % setProtectionRatio(dump,coordinate);
            setSiteAId(dump,coordinate);
            setSiteBId(dump,coordinate);
            setLinkID(dump,coordinate);
            setSiteAssfreq(dump,coordinate);
            % setCorrection(dump,correction);            
            setBudgets(dump,coordinate);
            setWantedUnwanted(dump); 
            setBandFreq(dump,bands);
            setDistance1(dump,coordinate)
            % setCorrectionFactor(dump);
%             setProtectionMargin(dump);  
            setDistance2(dump,coordinate);
            % setProRatioTable(dump,ProRatioTable);
%             setCoChannelNum(dump,coordinate)
        end
        
        function setCoChannelNum(dump,coordinate)
           dump.CoChannelNum = coordinate.channelNumber;
        end
        
        %% Set protection ratio table from SQL lookup
        function setProRatioTable(dump,ProRatioTable)
            dump.proRatioTable = ProRatioTable;
        end
        %% Set Band Freq
        function setBandFreq(dump,bands)
             dump.theband = theBand(bands);             
        end
        
        %% Set Assigned Transmit Freq
        function setSiteAssfreq(dump,coordinate)
             dump.assTxFreqApp1 = coordinate.link1.app1.BandsApparatus.frequency1;             
        end
        
        %% Site A ID
        function setSiteAId(dump,coordinate)
             dump.SiteAId = coordinate.link1.app1.LocationApparatus.SITE_ID;
        end
        %% Site B ID
        function setSiteBId(dump,coordinate)
             dump.SiteBId = coordinate.link1.app2.LocationApparatus.SITE_ID;
        end
        
         %% Link ID A_B
        function setLinkID(dump,coordinate)
             dump.linkID =  coordinate.link1.app1.IDApparatus.ACCESS_ID;            
        end
        
        %% Rain correction
        function setCorrection(dump,correction)
            dump.correction = correction;
        end   
        
        %% Antenna Budgets
        function setBudgets(dump,coordinate)
            [dump.antBudget1,dump.antBudget2,dump.antBudget3,dump.antBudget4] = antBudget(coordinate);
        end   
        
        %% Wanted:Unwanted Signal
        function setWantedUnwanted(dump)
            [dump.wantedToUnwantedSignalB] = dump.antBudget1 - dump.antBudget3; % wanted (dBm) - unwanted(dBm)
            [dump.wantedToUnwantedSignalD] = dump.antBudget4 - dump.antBudget2;            
        end
        
        %% Cross interference Distance
        function setDistance1(dump,coordinate)           
            dump.A2D_pl = coordinate.link2.DistanceTxRx(); %distance app1,app4
            dump.C2B_pl = coordinate.link3.DistanceTxRx(); %distance app3,app2 
        end
        
        %% Inputed link (a->b) and comparsion link (c->d)
        function setDistance2(dump,coordinate)           
            dump.A2B_pl = coordinate.link1.DistanceTxRx();              
            dump.C2D_pl = coordinate.link4.DistanceTxRx();   
        end
        
        %% Protection Margin - if negitive we can't use that channel
        function setProtectionMargin(dump)
            dump.Prot_Margin_B = dump.wantedToUnwantedSignalB + dump.CoCh_pro_B;
            dump.Prot_Margin_D = dump.wantedToUnwantedSignalD + dump.CoCh_pro_D;
        end
        
        %% Set Protection Ratios
         function setProtectionRatio(dump,coordinate)
           dump.CoCh_pro_D = coordinate.CoCh_pro_D;
           dump.AdjCh_pro_D = coordinate.AdjCh_pro_D;
           dump.CoCh_pro_B = coordinate.CoCh_pro_B;
           dump.AdjCh_pro_B = coordinate.AdjCh_pro_B;
         end
%         %% Correct Factor %First calc A2D_pl & C2B_pl
%         function setCorrectionFactor(dump)  
%                        
%             [dump.CoCh_pro_D,dump.AdjCh_pro_D] = CorrectionFactorLink(dump,dump.A2D_pl);
%             
%             [dump.CoCh_pro_B,dump.AdjCh_pro_B] = CorrectionFactorLink(dump,dump.C2B_pl);       
%         end
%         
%         function [CoCh_pro_Link,AdjCh_pro_Link] = CorrectionFactorLink(dump,distance)
%             % distance
%             temp = correctionfactor(dump,distance);
%             % if (CoordinateApparatus.pol,'H')
%             CoCh_pro_Link = lookUpCoChProRatio() + temp;
%             AdjCh_pro_Link = -30 + temp;
% %              else 
% %              end
%         end         
%         
%         function proRatio = lookUpCoChProRatio(dump)
%             dump.proRatioTable
%              linkMod = getLinkModulation(link)
%         end
%         
%         function [protect_corr] = correctionfactor(dump,path_length)
%            
%             path_length=path_length/1000;%convert meters to km
%             if dump.correction<0 %rain rate
%                 %load rain rate data
%                 rain_rate=-dump.correction;
%                 if dump.theband>=15
%                     if dump.theband>=18
%                         if dump.theband>=22
%                             if dump.theband>=38 % 38 GHz band
%                                 a=-0.24;b=9.45;c=-33.5;
%                                 CF_40=a*path_length^2+b*path_length+c;
%                                 a=-0.75;b=15.24;c=-35;
%                                 CF_60=a*path_length^2+b*path_length+c;
%                                 a=-0.9874;b=18.793;c=-33.754;
%                                 CF_80=a*path_length^2+b*path_length+c;
%                                 protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
%                                 if protect_corr<-25.5,protect_corr=-25.5;end
%                             else % 22 GHz band
%                                 a=-0.0815;b=3.9605;c=-34.584;
%                                 CF_40=a*path_length^2+b*path_length+c;
%                                 a=-0.1266;b=5.4266;c=-31.49;
%                                 CF_60=a*path_length^2+b*path_length+c;
%                                 a=-0.2017;b=7.0796;c=-30.828;
%                                 CF_80=a*path_length^2+b*path_length+c;
%                                 protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
%                                 if protect_corr<-23.5,protect_corr=-23.5;end
%                             end
%                         else % 18 GHz band
%                             a=16.432;b=54.212;c=0;
%                             CF_40 = a*log(path_length)-b;
%                             a=19.768;b=52.135;c=0;
%                             CF_60= a*log(path_length)-b;
%                             a=20.439;b=47.256;c=0;
%                             CF_80= a*log(path_length)-b;
%                             protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
%                             if protect_corr<-30,protect_corr=-30;end
%                         end
%                     else % 15 GHz band
%                         pl_save=path_length;
%                         if path_length<5,path_length=5;end
%                         a=11.166;b=47.827;c=0;
%                         CF_40 = a*log(path_length)-b;
%                         a=13.833;b=47.227;c=0;
%                         CF_60= a*log(path_length)-b;
%                         a=13.097;b=39.813;c=0;
%                         CF_80= a*log(path_length)-b;
%                         protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
%                         if pl_save<5, protect_corr=pl_save*((protect_corr-(-40))/(5-0))+(-40);end %linear interpolation between 5 & 0 km.
%                     end
%                 else
%                     protect_corr=0;
%                     display('WARNING - rain rate CF undefined for band ',num2str(dump.theband),'GHz');
%                 end
%             else %path length
%                 %path_lenth_corr=dump.correction;
%                 %bands.band
%                 if str2double(dump.theband)<=5
%                     protect_corr_5 = 15.798*log(path_length)-73.5;
%                     protect_corr_10= 15.664*log(path_length)-68.725;
%                     protect_corr_20= 15.174*log(path_length)-62.23;
%                     protect_corr=interp_PLCF(dump,protect_corr_5,protect_corr_10,protect_corr_20);
%                     if protect_corr<-26.5,protect_corr=-26.5;end %PLCF floor value
%                     if path_length>110, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
%                 elseif str2double(dump.theband)<=8
%                     protect_corr_5 = 15.633*log(path_length)-70.232;
%                     protect_corr_10= 15.924*log(path_length)-66.753;
%                     protect_corr_20= 15.151*log(path_length)-59.297;
%                     protect_corr=interp_PLCF(dump,protect_corr_5,protect_corr_10,protect_corr_20);
%                     if protect_corr<-34,protect_corr=-34;end %PLCF floor value
%                     if path_length>100, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
%                 elseif str2double(dump.theband)<=11
%                     protect_corr_5 = 15.56*log(path_length)-61.962;
%                     protect_corr_10= 15.523*log(path_length)-57.451;
%                     protect_corr_20= 15.83*log(path_length)-53.951;
%                     protect_corr=interp_PLCF(dump,protect_corr_5,protect_corr_10,protect_corr_20);
%                     if protect_corr<-26,protect_corr=-26;end %PLCF floor value
%                     if path_length>70, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
%                 elseif str2double(dump.theband)<=13
%                     protect_corr_5 = 15.837*log(path_length)-57.755;
%                     protect_corr_10= 15.779*log(path_length)-53.901;
%                     protect_corr_20= 15.79*log(path_length)-48.684;
%                     protect_corr=interp_PLCF(dump,protect_corr_5,protect_corr_10,protect_corr_20);
%                     if protect_corr<-20,protect_corr=-20;end %PLCF floor value
%                     if path_length>70, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
%                 else
%                     protect_corr=0;
%                 end
%             end
%         end % Find protection factor offset value
%         
%         function [protect_corr]=interp_PLCF(dump,protect_corr_5,protect_corr_10,protect_corr_20)
%             if dump.correction<=5
%                 protect_corr=protect_corr_5;
%             elseif dump.correction<=10
%                 protect_corr =((dump.correction-5)*(-protect_corr_10+protect_corr_5)/(10-5)-protect_corr_5);
%             elseif dump.correction<=20
%                 protect_corr =((dump.correction-10)*(-protect_corr_20+protect_corr_10)/(20-10)-protect_corr_10);
%             else
%                 protect_corr=protect_corr_20;
%                 display('Warning Path Length correction factor undefined for PL value')
%             end
%             
%         end
%         
%         function [protect_corr]=interp_RRCF(dump,protect_corr_5,protect_corr_10,protect_corr_20)
%             if dump.correction<=5
%                 protect_corr=protect_corr_5;
%             elseif dump.correction<=10
%                 protect_corr =((dump.correction-5)*(-protect_corr_10+protect_corr_5)/(10-5)-protect_corr_5);
%             elseif dump.correction<=20
%                 protect_corr =((dump.correction-10)*(-protect_corr_20+protect_corr_10)/(20-10)-protect_corr_10);
%             else
%                 protect_corr=protect_corr_20;
%                 display('Warning Path Length correction factor undefined for PL value')
%             end
%         end
%         
     end    
end

