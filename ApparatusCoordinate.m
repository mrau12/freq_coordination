classdef ApparatusCoordinate < handle
    %APPARATUSCOORDINATE Properties related to Coordinate button/calculation
    
    properties
        %properties that must be set before can 'Coordinate'
        pol; % Polarity
        ANT_SIZE; % Anntena
        TX_POWER; % Power
        correction; %Rain (mm)
    end
    
    methods
        function CoApp = ApparatusCoordinate(polarity,size,power,correction)
            switch nargin
                case 0
                    CoApp.pol = [];
                    CoApp.ANT_SIZE = [];
                    CoApp.TX_POWER = [];
                    CoApp.correction = [];
                    
                case 4
                    setPolarisation(CoApp,polarity);
                    setAntennaSize(CoApp,size);
                    setTransmitPower(CoApp,power);
                    setCorrection(CoApp,correction);
                    
                otherwise
                    disp('error incorrect parameters for AppID object')
            end
        end
        
        function setPolarisation(CoApp,polarity)
            switch polarity
                case 'V', CoApp.pol = 'V'; % Vertical
                case 'H', CoApp.pol = 'H'; % Horizontal
                %case CR
            end
        end
        
        function setTransmitPower(CoApp,power)
            CoApp.TX_POWER = power;
        end
        
        function setAntennaSize(CoApp,size)
            size;
            switch round(size,1)
                case 0
                    CoApp.ANT_SIZE=0; % meters
                case 0.3000
                    CoApp.ANT_SIZE=0.3; % meters
                case 0.6000
                    CoApp.ANT_SIZE=0.6;
                case 0.8000
                    CoApp.ANT_SIZE=0.8;
                case 0.9000
                    CoApp.ANT_SIZE=0.9;
                case 1
                    CoApp.ANT_SIZE=1;
                case 1.2000
                    CoApp.ANT_SIZE=1.2;
                case 1.8000
                    CoApp.ANT_SIZE=1.8;
                case 2.4000
                    CoApp.ANT_SIZE=2.4;
                case 3.0000
                    CoApp.ANT_SIZE=3.0;
                case 3.7000
                    CoApp.ANT_SIZE=3.7;
                case 4.6000
                    CoApp.ANT_SIZE=4.6;
                case 11
                    CoApp.ANT_SIZE=11;
                otherwise warning('Unexpected size. i.e Should be  0.3, 0.6, 0.8, 1.2, 1.8, 2.4...4.6 etc' );
                    
            end
        end
        %Set methods
        function setCorrection(CoApp,correction)
            CoApp.correction = correction;
        end
        
        function setPol(CoApp,Pol)
            CoApp.pol = Pol;
        end
        
        function setAntSize(CoApp,size)
            CoApp.ANT_SIZE = size;
        end
        
        function setTxPower(CoApp,power)
            CoApp.TX_POWER = power;
        end
        %Get methods
        function power = getTxPower(CoApp)
            power = CoApp.TX_POWER;
        end
    end    
end

