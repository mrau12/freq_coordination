classdef readAntennaPattern <handle
    % UNTITLED Summary of this class goes here
    % Detailed explanation goes here
    
    properties
        %%Input non-standard
        nonStdFolderName; % directory of non-standardised files
        fileList; % names of all files including from sub directories
        %%Output standardised
        rootPath; % parent directory of standardised antenna files
        standardFolderName; % folder name of standardised antenna files  
        HV = '[Hh].[Vv]|[Hh][Vv]';                     
        VH = '[Vv].[Hh]|[Vv][Hh]';
        HH = '[Hh].[Hh]|[Hh][Hh]|[H][h]|\b[Hh][Oo][Rr][Ii][Zz][Oo][Nn][Tt][Aa][Ll]\b';
        VV = '[Vv].[Vv]|[Vv][Vv]|\b[Vv][Ee][Rr][Tt][Ii][Cc][Aa][Ll]';
        
    end
    
    methods
        function files = readAntennaPattern(nonStdName,root,stdName)
            switch nargin
                case 2
                    files.nonStdFolderName = nonStdName;
                    files.rootPath = root;
                    files.standardFolderName = [];
                    files.fileList = [];
                    
                case 3
                    files.nonStdFolderName = nonStdName;
                    files.rootPath = root;
                    files.standardFolderName = stdName;
                    files.fileList = [];
            end
        end
        
        function setParentFolder(files,rootF)
            switch nargin
                case 2
                    files.rootDirectory = rootF;
                case 1
                    files.rootDirectory = 'C:\Users\mike\Dropbox\SAT\Matlab\4. Licencing\4_4_2016';
                otherwise
                    disp('Either too many or not enough inputs for setParentFolder')
            end
        end
        function setFolderName(files,stdF)
            switch nargin
                case 2
                    files.standardFolderName = stdF;
                case 1
                    files.standardFolderName = 'stdAntennas';
                otherwise
                    disp('Either too many or not enough inputs for setFolderName')
            end
        end
        
        function setDirName(files,nonStdF)
            switch nargin
                case 2
                    files.nonStdFolderName = nonStdF;
                case 1
                    files.nonStdFolderName = 'Antennas';
                otherwise
                    disp('Either too many or not enough inputs for setDirName')
            end
        end
        
        function getFileList(files)
            wholeName = char(strcat(files.rootPath,{'\'},files.nonStdFolderName)); % full path of non standard ant folder
            dirData = dir(wholeName);      %# Get the data for the current directory
            dirIndex = [dirData.isdir];  %# Find the index for directories
            files.fileList = {dirData(~dirIndex).name}';  %'# Get a list of the files
            if ~isempty(files.fileList)
                files.fileList = cellfun(@(x) fullfile(wholeName,x),...  %# Prepend path to files
                    files.fileList,'UniformOutput',false);
            end
            subDirs = {dirData(dirIndex).name};  %# Get a list of the subdirectories
            validIndex = ~ismember(subDirs,{'.','..'});  %# Find index of subdirectories
            %#   that are not '.' or '..'
            for iDir = find(validIndex)                  %# Loop over valid subdirectories
                nextDir = fullfile(wholeName,subDirs{iDir});    %# Get the subdirectory path
                files.fileList = [files.fileList; getAllFiles(nextDir)];  %# Recursively call getAllFiles
            end
        end
        
        function makeStdDirectory(files)
            fullFileDir = char(strcat(files.rootPath,{'\'},files.standardFolderName));
            if ~exist(fullFileDir,'dir')
                mkdir(files.standardFolderName,files.standardFolderName);
            else
                disp('Directory already exists')
            end
        end
        
        function antenna = standarisedAll(files)
            
        end
                
        function antenna = editFile(files,index)
            
            fid=fopen(files.fileList{index},'r');
            antenna = textscan(fid, '%s%s'); 
            fclose(fid);
        end
        
        % regex pattern match to find different string combinations e.gg Hv
        % HV, H/v, HV,etc and return where it is indexed
        function [Index] = getIndexPolarity(files,index,expression)
            switch expression
                case 'HV'
                    regexExpression = '[Hh].[Vv]|[Hh][Vv]';
                case 'VH'
                    regexExpression = '[Vv].[Hh]|[Vv][Hh]';
                case 'HH'
                    regexExpression = '[Hh].[Hh]|[Hh][Hh]|[H][h]|\b[Hh][Oo][Rr][Ii][Zz][Oo][Nn][Tt][Aa][Ll]\b';
                case 'VV'
                    regexExpression = '[Vv].[Vv]|[Vv][Vv]|\b[Vv][Ee][Rr][Tt][Ii][Cc][Aa][Ll]';
                otherwise
                    regexExpression = expression;
                    
            end
            fid=fopen(files.fileList{index},'r');
            fileDirectory = files.fileList{index};
            filetext = fileread(fileDirectory); %Is this the right function: filesread?
            [Index] = regexp(filetext,regexExpression);
            fclose(fid);            
        end
            
        
        function inx = numBetweenStrMatch(Str,subString)
           inx = strfind(Str, subString);
        end
        
        function funcReplaceString(InputFile, OutputFile, SearchString, ReplaceString)
            
            %% change data [e.g. initial conditions] in model file % InputFile - string % OutputFile - string % SearchString - string % ReplaceString - string
            % read whole model file data into cell array
            fid = fopen(InputFile);
            data = textscan(fid, '%s', 'Delimiter', '\n', 'CollectOutput', true); 
            fclose(fid);
            % modify the cell array
            % find the position where changes need to be applied and insert new data for
            I = 1:length(data{1});
            tf = strcmp(data{1}{I}, SearchString);
            % search for this string in the array
            if tf == 1
                data{1}{I} = ReplaceString;% replace with this string end                
            end
            % write the modified cell array into the text file
            fid = fopen(OutputFile, 'w');
            for I = 1:length(data{1})
                fprintf(fid, '%s\n', char(data{1}{I}));
            end
            
            fclose(fid);
        end
      
        
        % WARNING slow ~15 seconds
        function copyAllFiles(files)
            for index = 1:size(files.fileList)
                type = getFileType(files,index);
                if (type)
                    copyFile(files,index);
                end
            end
        end
        
        % Copies and renames bla files
        function copyAllFilesBla(files)
            for index = 1:size(files.fileList)
                type = getFileTypeBla(files,index);
                if (type)
                    copyFile(files,index);
                end
            end
        end
        
        % Copies file from Antenna folder to standardised Antenna folder
        % WARNING first run makeStdDir & getFileList
        function copyFile(files,index)
            myFile = char(files.fileList(index));
            [~,name,~] = fileparts(char(myFile));
            fullDirDest = char(strcat(files.rootPath,{'\'},files.standardFolderName));
            newStdName = char(strcat(name,{'.txt'}));
            fullFile = strcat(fullDirDest,{'\'},newStdName);
            copyfile(char(myFile),char(fullFile));
        end
        
        % Returns file extension
        % Could implement faster version?
        function type = getFileType(files,index)
            myFile = char(files.fileList(index));
            [~,~,extension] = fileparts(char(myFile));
            switch extension
                case '.dat'
                    type = 1;
                case '.DAT'
                    type = 1;
                case '.pla'
                    type = 1;
                case '.PLA'
                    type = 1;
                case '.adf'
                    type = 1;
                case '.ADF'
                    type = 1;
                case '.TXT'
                    type = 1;
                case '.txt'
                    type = 1;
                otherwise
                    type = 0;
                    disp('file extension unknown')
                    index
                    extension
            end
        end
        
        function type = getFileTypeBla(files,index)
            myFile = char(files.fileList(index));
            [~,~,extension] = fileparts(char(myFile));
            switch extension
                case '.bla'
                    type = 1;
                case '.BLA'
                    type = 1;
                otherwise
                    type = 0;
                    disp('file extension unknown')
                    index
                    extension
            end
        end
        
        function deleteCharLines(files,index)
            anyStringWithChars = '[a-zA-Z]';
            fid=fopen(char(files.fileList(index))) ; % the original file
            fidd=fopen('new filename.txt','w') ; % the new file
            while ~feof(fid) ; % reads the original till last line
                tline=fgets(fid) ; %
                if regexp(tline,anyStringWithChars);
                   hh = ~isempty(regexp(tline,files.HH)); % returns 1 or 0 that works  with below || statement
                   vv = ~isempty(regexp(tline,files.VV));
                   hv = ~isempty(regexp(tline,files.HV));
                   vh = ~isempty(regexp(tline,files.VH));
                    if hh || vv || hv || vh;
                         fwrite(fidd,tline) ;
                    end
                else
                    fwrite(fidd,tline) ;
                end
            end
            fclose all ;
        end
        
        function standardisePla(files)
            
        end
        
        function standardiseAdf(files)
            
        end
        
        function standardiseTxt(files)
            
        end
        
        function standardise(files)
            
        end
    end
    
end

