classdef Spectrum < handle
    % SPECTRUM Summary of this class goes here
    % Detailed explanation goes here
    
    % Must install http://au.mathworks.com/support/sysreq/files/SystemRequirements-Release2015a_SupportedCompilers.pdf?requestedDomain=www.mathworks.com&sec=win64
    % for mex to work
    properties
        channels; % Interefer Channels (band)
        numOfChannels;
        victimChannel; % singular Victim channel being comapred against interferer band  
        victimID;
        tableProRatio;        
        rules; % simplified table protection ratio(dB) for multiple frequency offsets i.e. cochannel and adjchannel
        mod2mod; % modulation type ---> modulation type e.g. digital ---> analogue
        savetable;
        arrayProtMargin;
        correction;
        theBand;
        distanceVic2Int;
    end
    
    methods
        
        %% Constructor
        function spectrum = Spectrum(bands,proRatioTABLE,victim,mod2mod,id,correction)
            switch nargin
                case 6
                    setTheBand(spectrum,bands);
                    setNumOfChannels(spectrum,bands);
                    setChannelsEmptyObjects(spectrum);                    
                    setChannels(spectrum,bands);
                    setProRatioTable(spectrum,proRatioTABLE);
                    setVictim(spectrum,victim);
                    setVictimID(spectrum,id);
                    setMod2Mod(spectrum,mod2mod);
                    setAllOffset(spectrum);
                    spectrum.savetable = [];
                    spectrum.arrayProtMargin = [];
                    spectrum.correction = correction;
                    
                case 0
                    spectrum.channels =[]; 
                    spectrum.numOfChannels=[];
                    spectrum.victimChannel=[]; 
                    spectrum.victimID=[];
                    spectrum.tableProRatio=[];
                    spectrum.rules=[];
                    spectrum.mod2mod=[];
                    spectrum.savetable=[];
                    spectrum.arrayProtMargin=[];
                    spectrum.correction=[];
                    spectrum.theBand=[];
                    spectrum.distanceVic2Int=[];
                otherwise
                    disp('Error:unable to create Spectrum() object')
            end
        end
        
        %% Distance
        function distVic2Int(spectrum)
        end
        
        %% What band are we looking at?
        function setTheBand(spectrum,bands)
            spectrum.theBand = bands.tableBands.band;
        end
        
        %% ID for Channels
        function setVictimID(spectrum,id)
            spectrum.victimID = id;
        end
        %% Choosen band from gui ---> look up sql ---> how many Channels
        function setNumOfChannels(spectrum,bands)
            spectrum.numOfChannels = bands.tableBands.channels;
        end
        
        %% Create empty Channels objects 
        function setChannelsEmptyObjects(spectrum)
            for number = 1:spectrum.numOfChannels
            c(number) = Channel();
            end
            spectrum.channels = c;
        end
        
        %% Channels (band) defined by bandsSQL
        function setChannels(spectrum,bands)
             mainChannels(spectrum,bands);
             primeChannels(spectrum,bands);
        end
        
        %% Set main Channels
        function mainChannels(spectrum,bands)
            start1 = bands.tableBands.channel_start;
            bandwidth1 = bands.tableBands.bandwidth;            
            % first channel
            spectrum.channels(1) = Channel(start1,bandwidth1); 
            setUpperFr(spectrum.channels(1));
            setLowerFr(spectrum.channels(1));
             for mainNumber = 2:(bands.tableBands.channels/2)
                nextMid = spectrum.channels(mainNumber-1).CENTRE_FREQ + spectrum.channels(mainNumber-1).CHAN_BANDWIDTH;
                spectrum.channels(mainNumber) = Channel(nextMid,bandwidth1);
                setUpperFr(spectrum.channels(mainNumber));
                setLowerFr(spectrum.channels(mainNumber));
             end
            
        end
        
        %% Set Prime Channels
        function primeChannels(spectrum,bands)
            start1 = bands.tableBands.channel_start;
            bandwidth1 = bands.tableBands.bandwidth;
            TxRx_gap = bands.tableBands.TxRx_gap;
            % first prime channel; in the middle of channels
            spectrum.channels((bands.tableBands.channels/2)+1) = Channel((start1+TxRx_gap),bandwidth1); 
            setUpperFr(spectrum.channels((bands.tableBands.channels/2)+1));
            setLowerFr(spectrum.channels((bands.tableBands.channels/2)+1));
            Chan2OfPrime = ((bands.tableBands.channels/2)+2);
            for primeNumber = Chan2OfPrime:bands.tableBands.channels
                nextMid = spectrum.channels(primeNumber-1).CENTRE_FREQ + spectrum.channels(primeNumber-1).CHAN_BANDWIDTH;
                spectrum.channels(primeNumber) = Channel(nextMid,bandwidth1);
                setUpperFr(spectrum.channels(primeNumber));
                setLowerFr(spectrum.channels(primeNumber));
            end
        end
        
        %% Set Protection Ratio Table
        function setProRatioTable(spectrum,proRatioTABLE)
            spectrum.tableProRatio = proRatioTABLE;
        end
        
        %% Set Victim Channel
        function setVictim(spectrum,victim)
            spectrum.victimChannel = victim;
        end
        
        %% Set modulation to modulation
        function setMod2Mod(spectrum,mod2mod)
            spectrum.mod2mod = mod2mod;
        end
        
        %% Get modulation to modulation
        function mod2mod = getMod2Mod(spectrum)
            mod2mod = spectrum.mod2mod;
        end
        
        %% Size of rule table
        function numOfRules = getAmountOfRules(spectrum)
            numOfRules = size(spectrum.rules,1);
        end
        
        %% Finds out where vic centre freq is compared to channel(num) centre freq
        function setChanOffset(spectrum,ChannelNum)
           offset = abs(spectrum.channels(ChannelNum).CENTRE_FREQ - spectrum.victimChannel.CENTRE_FREQ);
           spectrum.channels(ChannelNum).frequencyOffset = offset;
        end
        
        %% Set difference for all Channels
        function setAllOffset(spectrum)
           for channelNum = 1:spectrum.numOfChannels;
               setChanOffset(spectrum,channelNum);
           end
        end     

        %% Finds protection margin from table (check protection ratio table in acma fx3)
        %
        %          A*****>B (Rx Victim)
        %                 ^
        %                *
        %               *
        %              *
        %             *
        %            *
        % (Tx Inter)C****>D      
        %
        
        function lookUpProtectionRatioRules(spectrum)
            %% Modulation to modulation
            % Refines table
            mod2Mod(spectrum);
            
            %% Bandwidth of the Tx and Rx
            % Refines table
            bandwidthForTxRx(spectrum);            
            
            %% Channel Offset
            % Outputs: protection margin for channels
            offsetForChannels(spectrum);   
        end
        
        %% Modulation type --> type
        function mod2Mod(spectrum)           
            table = spectrum.tableProRatio.tableProRatio;
            m2m = getMod2Mod(spectrum);
            tsize = size(table.modulation,1);
            for index = 1:tsize
                if ~strcmp(table.modulation(index,1), m2m)
                   table.modulation(index,1) = []; %delete
                end
            end            
        end
        
        %% Bandwidth of Tx interefer & Rx victim
        function bandwidthForTxRx(spectrum)           
            % Get table of protection ratios
            table = spectrum.tableProRatio.tableProRatio;
            
            txIntefererBandwidth = spectrum.channels(1).CHAN_BANDWIDTH; 
            % All interefer chans have same bandwidth
            rxVictimBandwidth = spectrum.victimChannel.CHAN_BANDWIDTH;            
            % Get Tx interefer's bandwidth to shorten list
            shorterlist = table(table.channelSize1 == txIntefererBandwidth,:);
            % Get Rx victim's bandwidth to shorten list
            spectrum.rules = shorterlist(shorterlist.channelSize2 == rxVictimBandwidth,:);
        end
        
        %% Channel Offset
        function offsetForChannels(spectrum)
            % Main & Prime Channels
            table = spectrum.rules;
            for channelNum = 1:spectrum.numOfChannels;
                spectrum.channels(channelNum).protectionMargin = table.protectionRatio(table.frequencyOffset == spectrum.channels(channelNum).frequencyOffset);
            end
        end
        
        %% Creates cell array to input into saveTable
        function setProtMarginTable(spectrum) 
            C{1,1} = spectrum.victimID;
            for index = 2:spectrum.numOfChannels+1
            C{1,index} = spectrum.channels(1,index-1).protectionMargin;
            end
            spectrum.arrayProtMargin = C;
        end
        
        %% Creates table saveTable
        function setTable(spectrum)
            names = {'Vicitm_ID'};
            for channelNum = 1:spectrum.numOfChannels;
              names{end+1} = strcat('Channel', num2str(channelNum));
            end             
            spectrum.savetable = cell2table(spectrum.arrayProtMargin,'VariableNames',names);
        end
       
%          function setCorrectionRatio(coordinate,spectrum)
%             distA_B =  DistanceTxRx(coordinate.link1);
%             distC_D =  DistanceTxRx(coordinate.link4);
%             [pro_D] = lookUpProtectionRatio(spectrum) + correctionFactor(coordinate,distA_B);
%             [pro_B] = lookUpProtectionRatio(spectrum) + correctionFactor(coordinate,distC_D);
%          end  
        
        %% Finds protection factor offset value (refer to acma fx3 graph PROTECTION RATIO CORRECTION FACTORS MULITPATH)
        % Inputs: 
        % Path length between sites in (km)  
        % Rain (mm) (which line of three or more on graph in acma fx3)
        % Output:
        % Correction factor(dB)
        function [protect_corr] = correctionFactor(spectrum,path_length)
           
            path_length=path_length/1000;%convert meters to km
            if spectrum.correction<0 %rain rate
                %load rain rate data
                rain_rate=-spectrum.correction;
                if spectrum.theband>=15
                    if spectrum.theband>=18
                        if spectrum.theband>=22
                            if spectrum.theband>=38 % 38 GHz band
                                a=-0.24;b=9.45;c=-33.5;
                                CF_40=a*path_length^2+b*path_length+c;
                                a=-0.75;b=15.24;c=-35;
                                CF_60=a*path_length^2+b*path_length+c;
                                a=-0.9874;b=18.793;c=-33.754;
                                CF_80=a*path_length^2+b*path_length+c;
                                protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
                                if protect_corr<-25.5,protect_corr=-25.5;end
                            else % 22 GHz band
                                a=-0.0815;b=3.9605;c=-34.584;
                                CF_40=a*path_length^2+b*path_length+c;
                                a=-0.1266;b=5.4266;c=-31.49;
                                CF_60=a*path_length^2+b*path_length+c;
                                a=-0.2017;b=7.0796;c=-30.828;
                                CF_80=a*path_length^2+b*path_length+c;
                                protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
                                if protect_corr<-23.5,protect_corr=-23.5;end
                            end
                        else % 18 GHz band
                            a=16.432;b=54.212;c=0;
                            CF_40 = a*log(path_length)-b;
                            a=19.768;b=52.135;c=0;
                            CF_60= a*log(path_length)-b;
                            a=20.439;b=47.256;c=0;
                            CF_80= a*log(path_length)-b;
                            protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
                            if protect_corr<-30,protect_corr=-30;end
                        end
                    else % 15 GHz band
                        pl_save=path_length;
                        if path_length<5,path_length=5;end
                        a=11.166;b=47.827;c=0;
                        CF_40 = a*log(path_length)-b;
                        a=13.833;b=47.227;c=0;
                        CF_60= a*log(path_length)-b;
                        a=13.097;b=39.813;c=0;
                        CF_80= a*log(path_length)-b;
                        protect_corr=interp_RRCF(rain_rate,CF_40,CF_60,CF_80);
                        if pl_save<5, protect_corr=pl_save*((protect_corr-(-40))/(5-0))+(-40);end %linear interpolation between 5 & 0 km.
                    end
                else
                    protect_corr=0;
                    display('WARNING - rain rate CF undefined for band ',num2str(spectrum.theband),'GHz');
                end
            else %path length
                %path_lenth_corr=dump.correction;
                %bands.band
                if str2double(spectrum.theBand)<=5
                    protect_corr_5 = 15.798*log(path_length)-73.5;
                    protect_corr_10= 15.664*log(path_length)-68.725;
                    protect_corr_20= 15.174*log(path_length)-62.23;
                    protect_corr=interp_PLCF(spectrum,protect_corr_5,protect_corr_10,protect_corr_20);
                    if protect_corr<-26.5,protect_corr=-26.5;end %PLCF floor value
                    if path_length>110, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
                elseif str2double(spectrum.theBand)<=8
                    protect_corr_5 = 15.633*log(path_length)-70.232;
                    protect_corr_10= 15.924*log(path_length)-66.753;
                    protect_corr_20= 15.151*log(path_length)-59.297;
                    protect_corr=interp_PLCF(spectrum,protect_corr_5,protect_corr_10,protect_corr_20);
                    if protect_corr<-34,protect_corr=-34;end %PLCF floor value
                    if path_length>100, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
                elseif str2double(spectrum.theBand)<=11
                    protect_corr_5 = 15.56*log(path_length)-61.962;
                    protect_corr_10= 15.523*log(path_length)-57.451;
                    protect_corr_20= 15.83*log(path_length)-53.951;
                    protect_corr=interp_PLCF(spectrum,protect_corr_5,protect_corr_10,protect_corr_20);
                    if protect_corr<-26,protect_corr=-26;end %PLCF floor value
                    if path_length>70, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
                elseif str2double(spectrum.theBand)<=13
                    protect_corr_5 = 15.837*log(path_length)-57.755;
                    protect_corr_10= 15.779*log(path_length)-53.901;
                    protect_corr_20= 15.79*log(path_length)-48.684;
                    protect_corr=interp_PLCF(spectrum,protect_corr_5,protect_corr_10,protect_corr_20);
                    if protect_corr<-20,protect_corr=-20;end %PLCF floor value
                    if path_length>70, display(['WARNING - PLCF undefined for a path of length ',num2str(path_length,3),' Km']);end
                else
                    protect_corr=0;
                end
            end
        end 
        
        function [protect_corr]=interp_PLCF(coordinate,protect_corr_5,protect_corr_10,protect_corr_20)
            if coordinate.correction<=5
                protect_corr=protect_corr_5;
            elseif coordinate.correction<=10
                protect_corr =((coordinate.correction-5)*(-protect_corr_10+protect_corr_5)/(10-5)-protect_corr_5);
            elseif coordinate.correction<=20
                protect_corr =((coordinate.correction-10)*(-protect_corr_20+protect_corr_10)/(20-10)-protect_corr_10);
            else
                protect_corr=protect_corr_20;
                display('Warning Path Length correction factor undefined for PL value')
            end
            
        end
        
        function [protect_corr]=interp_RRCF(coordinate,protect_corr_5,protect_corr_10,protect_corr_20)
            if coordinate.correction<=5
                protect_corr=protect_corr_5;
            elseif coordinate.correction<=10
                protect_corr =((coordinate.correction-5)*(-protect_corr_10+protect_corr_5)/(10-5)-protect_corr_5);
            elseif coordinate.correction<=20
                protect_corr =((coordinate.correction-10)*(-protect_corr_20+protect_corr_10)/(20-10)-protect_corr_10);
            else
                protect_corr=protect_corr_20;
                display('Warning Path Length correction factor undefined for PL value')
            end
        end
        
        
                %% Compare victim channel to interefer multiple channels to find
        % co-channel ratio and channel number
        %
        % Output:
        % Protection ratio for co-channel
        % Channel that ratio is applied to
                
        % finds out where vic centre freq is compared to channels centre freq
%         function sortCentreChannels(spectrum)
%             array = [];
%             for channelNum = 1:spectrum.numOfChannels;
%                 array = [array,spectrum.channels(channelNum).CENTRE_FREQ];
%             end
%             array(1+end) = spectrum.victimChannel.CENTRE_FREQ;
%             spectrum.centreFreqArray = sort(array); 
%         end
       
%         
%         function offset = offsetToRight(spectrum)
%             [~,value] = closestChannel(spectrum);
%            offset =  abs(spectrum.victimChannel.CENTRE_FREQ - value);
%            
%         end
  
%         function applyAdjRule(spectrum) 
%             offset = offsetToRight(spectrum);
%             
%             for  index = 1:size(spectrum.rules,1)-1 % # adj rules
%                if (spectrum.rules.frequencyOffset(index) < offset && offset <= spectrum.rules.frequencyOffset(index+1))
%                
%                end
%             end
%         end
%         
%         function [chanNum,value] = closestChannel(spectrum)
%             tmp = abs(spectrum.centreFreqArray - spectrum.victimChannel.CENTRE_FREQ);
%             [~, chanNum] = min(tmp); %  of closest value
%             value = spectrum.centreFreqArray(chanNum); %closest value
%         end
        
%         function selectChannel(spectrum)
%            array = spectrum.centreFreqArray;
%            where = find(array == spectrum.victimChannel.CENTRE_FREQ);    
%            % Co Channel: two centre freq the same, one from victim and the
%            % other from interfer
%            if length(where) == 2
%              %   applyCo(unique(where));  
%              where(end)
%              
%            elseif length(where) == 1 % Adjacent Channel
%                if array(where) == array(1) % Only has one adj channel to right               
%                 2              
%                elseif array(where) == array(end) % Only has one adj channel to left   
%                 where-1
%                else % All other channels have 2 adj channels
%                 where-1
%                 where+1
%                end
%            else disp('Finding to many centre frequencies in selectRule function, Spectrum.m')
%            end
%         end
%       
    end
end

