classdef saveSpectrum < handle
    %SAVE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        filename;
        spectTable;
    end
    
    methods
        function save = saveSpectrum(filename,table)            
            save.filename = [filename,'-',datestr(date),'.csv'];
            save.spectTable = table;           
        end
        
        %% Filename of folder
        function addFileName(save,theFilename)
            if ~exist('Jobs','dir')
                mkdir('Jobs');
            end
            save.filename=[theFilename,'-',datestr(date),'.csv'];
        end            
        
        %% Write table to csv
        function [] = writeTable(save)
            writetable(save.spectTable,strcat('Jobs/',save.filename));
            %This warning is displayed when your requested worksheet does
            %not exist and is created.
            warning('off','MATLAB:xlswrite:AddSheet');
        end
        
        %% Open saved file in Excel
        function openFolder(save)
            winopen(strcat('Jobs/',save.filename));
        end        
    end
    
end

